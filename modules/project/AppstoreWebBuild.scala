import sbt._
import Keys._
import repositories._

import com.github.siasia.PluginKeys._
import com.github.siasia.WebPlugin._

import SbtProfilePlugin._
import SbtProfilePlugin.SbtProfileKeys._

import net.virtualvoid.sbt.graph.Plugin._

object AppstoreWebBuild extends Build {

  val Organization = "hms.appstore.web"
  val ProjectVersion = "vdf-2.0.60-SNAPSHOT"
  val ProjectProfile = "vodafone-da"
  val ProjectProfiles = Seq("default", "dialog", "vodafone-da", "vodafone-live", "robi-bd", "robi-bd-live")

  val DiscoveryApiVersion = "vdf-1.0.62"
  val ScalaVersion = "2.10.0"
  val SalatVersion = "1.9.2"
  val JettyVersion = "8.1.0.RC1"
  val SpringVersion = "3.2.3.RELEASE"
  val SpringSecurityVersion = "3.0.5.HMS-2-RELEASE"

  val TypesafeConfig = "com.typesafe" % "config" % "1.0.0"
  val TypesafeLogging = "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"

  val ServletApi = "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided->default"
  val Slf4jLog4j = "org.slf4j" % "slf4j-log4j12" % "1.7.2"
  val JclOverSlf4j = "org.slf4j" % "jcl-over-slf4j" % "1.7.2"

  val DiscoveryClient = "hms.appstore.api" %% "discovery-client" % DiscoveryApiVersion

  val Salat = "com.novus" %% "salat-core" % SalatVersion
  val SalatUtil = "com.novus" %% "salat-util" % SalatVersion

  val Casbah = "org.mongodb" %% "casbah" % "2.5.1" pomOnly()

  val JodaTime  = "joda-time" % "joda-time" % "2.1"
  val JodaConvert = "org.joda" % "joda-convert" % "1.2"
  val JodaTimeTag = "joda-time" % "joda-time-jsptags" % "1.1.1"

  val JasigCas = "org.jasig.cas.client" % "cas-client-core" % "3.2.1" excludeAll(ExclusionRule(organization = "javax.servlet"), ExclusionRule(organization = "commons-logging"))

  val CommonRegApi = "hms" % "user-service" % "1.0.2"
  val HmsCommonUtil = "hms.common" % "hms-common-util" % "1.0.6"
  val HmsRestCommon = "hms.common" % "rest-util" % "1.0.6"
  val HMSRegistrationApi = "hms" % "registration-api" % "1.1.3" excludeAll(ExclusionRule(organization = "javax.servlet"), ExclusionRule(organization = "org.apache.geronimo.specs"))
  val HttpServletComponents= "hms.scala.http" %% "http-servlet-components" % "1.0.0"

  val SpringOxm = "org.springframework" % "spring-oxm" % SpringVersion
  val SpringAop = "org.springframework" % "spring-aop" % SpringVersion
  val SpringWeb = "org.springframework" % "spring-web" % SpringVersion
  val SpringMvc = "org.springframework" % "spring-webmvc" % SpringVersion
  val SpringCore = "org.springframework" % "spring-core" % SpringVersion
  val SpringTest = "org.springframework" % "spring-test" % SpringVersion
  val SpringBeans = "org.springframework" % "spring-beans" % SpringVersion
  val SpringContext = "org.springframework" % "spring-context" % SpringVersion
  val SpringExpression = "org.springframework" % "spring-expression" % SpringVersion
  val SpringContextSupport = "org.springframework" % "spring-context-support" % SpringVersion

  val SpringSecurityCore = "org.springframework.security" % "spring-security-core" % SpringSecurityVersion
  val SpringSecurityWeb = "org.springframework.security" % "spring-security-web" % SpringSecurityVersion
  val SpringSecurityConfig = "org.springframework.security" % "spring-security-config" % SpringSecurityVersion
  val SpringCasClient = "org.springframework.security" % "spring-security-cas-client" % SpringSecurityVersion
  val springSecurityTaglib = "org.springframework.security" % "spring-security-taglibs" % SpringSecurityVersion

  val Jstl = "jstl" % "jstl" % "1.2"
  val SiteMesh = "org.sitemesh" % "sitemesh" % "3.0-alpha-2"
  val tagLib = "taglibs" % "standard" % "1.1.2"

  val JettyWebApp = "org.eclipse.jetty" % "jetty-webapp" % JettyVersion % "compile"
  val jettyContainer = "org.eclipse.jetty" % "jetty-webapp" % "8.1.0.RC1"
  val JettyJspContainer = "org.mortbay.jetty" % "jsp-2.0" % "6.1.22" % "container" excludeAll (ExclusionRule(organization = "org.slf4j"))
  val JettyWebAppContainer = "org.eclipse.jetty" % "jetty-webapp" % JettyVersion % "container"

  val ApacheFileUpload = "commons-fileupload" % "commons-fileupload" % "1.3.3"
  val IapClient = "hms.iap.service" % "iap-client" % "1.0.5"

  val jettyConf = config("container")

  val jettyPluginSettings = Seq(
    libraryDependencies ++= Seq(
      JettyWebAppContainer,
      JettyJspContainer
    ),
    port in jettyConf := 8080
  )

  val appstoreIndexingDependencies = Seq(
    DiscoveryClient,
    ServletApi,
    TypesafeConfig,
    TypesafeLogging
  )

  val appstoreDependencies = Seq(
    SpringCore,
    SpringExpression,
    SpringBeans,
    SpringAop,
    SpringContext,
    SpringContextSupport,
    SpringOxm,
    SpringWeb,
    SpringMvc,
    SpringTest,
    SpringSecurityCore,
    SpringSecurityWeb,
    SpringSecurityConfig,
    SiteMesh,
    Jstl,
    tagLib,
    JodaTime,
    JodaTimeTag,
    JodaConvert,
    ServletApi,
    Casbah,
    Salat,
    SalatUtil,
    Slf4jLog4j,
    TypesafeLogging,
    TypesafeLogging,
    JclOverSlf4j,
    JasigCas,
    DiscoveryClient,
    HMSRegistrationApi,
    SpringCasClient,
    springSecurityTaglib,
    HmsRestCommon,
    CommonRegApi,
    HmsCommonUtil,
    HttpServletComponents,
    ApacheFileUpload,
    IapClient
  )

  val testDependencies = Seq(
    "org.scalamock" %% "scalamock-specs2-support" % "3.0.1" % "test",
    "hms.specs" %% "specs-matchers" % "0.1.0" % "test"
  )

  val dependencyGraphSettings = net.virtualvoid.sbt.graph.Plugin.graphSettings

  val IvyCredentialFile = if (ProjectVersion.endsWith("SNAPSHOT")) {
    Path.userHome / ".ivy2" / ".credentials-snapshot"
  } else {
    Path.userHome / ".ivy2" / ".credentials-release"
  }

  val moduleLookupConfigurations = DefaultResolver.moduleConfig

  val excludedFilesInJar: NameFilter = (s: String) => """(.*?)\.(properties|props|conf|dsl|txt|xml)$""".r.pattern.matcher(s).matches

  lazy val baseSettings = {
    Defaults.defaultSettings ++ profileSettings ++ Seq(
      version := ProjectVersion,
      organization := Organization,
      scalaVersion := ScalaVersion,
      scalacOptions += "-deprecation",
      scalacOptions += "-unchecked",
      moduleConfigurations ++= moduleLookupConfigurations,
      publishTo <<= (version) { version: String =>
        val repo = "http://archiva.hsenidmobile.com/repository/"
        if (version.trim.endsWith("SNAPSHOT"))
          Some("Archiva Managed snapshots Repository" at repo + "snapshots/")
        else
          Some("Archiva Managed internal Repository" at repo + "internal/")
      },
      credentials += Credentials(IvyCredentialFile),
      logBuffered := false,
      parallelExecution in Test := false,
      offline := true,
      buildProfile := ProjectProfile,
      buildProfiles := ProjectProfiles,
      classpathTypes += "orbit"
    )
  }

  lazy val modules = Project(id = "appstore-web", base = file("."),
    settings = baseSettings ++ Seq(
      name := "appstore-web"
    )
  ) aggregate(appstoreIndexing, appstore)


  lazy val appstoreIndexing = Project(id = "appstore-indexing", base = file("appstore-indexing"),
    settings = baseSettings ++ Seq(
      name := "appstore-indexing",
      libraryDependencies ++= appstoreIndexingDependencies,
      libraryDependencies ++= testDependencies
    )
  )

  lazy val appstore = Project(id = "appstore", base = file("appstore"),
    settings = baseSettings ++ webSettings ++ jettyPluginSettings ++ Seq(
      name := "appstore",
      port in container.Configuration := 8081,
      artifactName := {
        (config: ScalaVersion, module: ModuleID, artifact: Artifact) => "appstore" + "." + "war"
      },
      libraryDependencies ++= appstoreDependencies,
      libraryDependencies ++= testDependencies,
      webappResources in Compile <<= (webappResources in Compile, baseDirectory, buildProfile) {
        (wrs, bd, p) => {
          val resourceDir = bd / "src" / "main" / "profile" / p / "webapp"
          if (resourceDir.exists) (wrs ++ Seq(resourceDir)).reverse else wrs
        }
      }
    )
  ) dependsOn (appstoreIndexing)
}
