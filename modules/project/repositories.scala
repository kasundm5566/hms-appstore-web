import sbt._

package object repositories {
  val HMSReleases = "hms archiva internal" at "http://archiva.hsenidmobile.com/repository/internal"
  val HMSSnapshots = "hms archiva snapshots" at "http://archiva.hsenidmobile.com/repository/snapshots"

  val SprayReleases = "spray repo" at "http://repo.spray.io/"

  val SonatypeReleases = "sonatype releases" at "https://oss.sonatype.org/content/groups/public"
  val SonatypeSnapshots = "sonatype snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

  sealed trait ModuleResolver extends {
    def moduleConfig: Seq[ModuleConfiguration] = Seq.empty
  }

  trait SprayResolver extends ModuleResolver {
    override def moduleConfig = super.moduleConfig ++
      Seq(ModuleConfiguration("io.spray", SprayReleases))
  }

  trait SonatypeResolver extends ModuleResolver {
    override def moduleConfig = super.moduleConfig ++ Seq(
      ModuleConfiguration("com.novus", SonatypeReleases),
      ModuleConfiguration("com.escalatesoft.subcut", SonatypeReleases)
    )
  }


  trait HmsArchivaResolver extends ModuleResolver {
    override def moduleConfig = super.moduleConfig ++ Seq(
      ModuleConfiguration("hms", HMSReleases),
      ModuleConfiguration("hms.specs", HMSReleases),
      ModuleConfiguration("hms.common", HMSReleases),
      ModuleConfiguration("hms.scala.http", HMSReleases),
      ModuleConfiguration("hms.appstore.api", HMSReleases),
      ModuleConfiguration("hms.iap.service", HMSReleases),
      ModuleConfiguration("hms.scala.util", HMSReleases),
      ModuleConfiguration("org.springframework.security", HMSReleases)
    )
  }

  object DefaultResolver extends SprayResolver with SonatypeResolver with HmsArchivaResolver

  object HmsWithMavenCentralOnly extends SprayResolver with SonatypeResolver with HmsArchivaResolver

  object WorkFromHome extends ModuleResolver

}
