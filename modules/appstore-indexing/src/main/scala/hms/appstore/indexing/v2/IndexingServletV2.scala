/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing.v2

import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging

import hms.appstore.api.client.DiscoveryService

import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}

import scala.concurrent._
import scala.xml.{XML, Elem}
import scala.concurrent.duration._
import hms.appstore.indexing.IndexingModule
import hms.appstore.indexing.page.SiteMapPage

class IndexingServletV2 extends HttpServlet with Injectable with  Logging {
  val bindingModule = IndexingModule
  private val apiService = inject[DiscoveryService]
  private val sitMap = inject[SiteMapPage]
  private val futureTimeOut = inject[Long]("future.timeOut")

  override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    val requestURI = req.getRequestURI
    requestURI match {
      case "/appstore/indexing/site-map.xml" => writeSiteMap(resp)
      case _ => resp.setStatus(HttpServletResponse.SC_NOT_FOUND)
    }
  }

  private def writeSiteMap(resp: HttpServletResponse) {
    val queryResults = Await.result(apiService.allAppIds(0, Int.MaxValue), futureTimeOut seconds)
    val page = sitMap.render(queryResults.getResults)
    writeOut("text/xml", page, resp)
  }

  private def writeOut(contentType: String, page: Elem, resp: HttpServletResponse) {
    resp.setStatus(HttpServletResponse.SC_OK)
    resp.setContentType(contentType)

    contentType match {
      case "text/xml" =>  XML.write(resp.getWriter, page, "UTF-8", xmlDecl = true, doctype = null)
      case "text/html" => XML.write(resp.getWriter, page, "UTF-8", xmlDecl = false, doctype =
                              xml.dtd.DocType("html", xml.dtd.SystemID("about:legacy-compat"), Nil))
    }
  }
}