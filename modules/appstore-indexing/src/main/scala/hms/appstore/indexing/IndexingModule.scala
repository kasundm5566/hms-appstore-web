/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing


import com.escalatesoft.subcut.inject.NewBindingModule
import com.typesafe.config.ConfigFactory

import hms.appstore.indexing.page._
import hms.appstore.api.client.ClientModule
import hms.scala.http.util.ConfigUtils

object IndexingModule  extends NewBindingModule({
  implicit module =>

    module <~ ClientModule

    val conf = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore")

    module.bind[String] idBy "appstore.web.indexing.baseUrl" toSingle  conf.getString("web.indexing.baseUrl")
    module.bind[String] idBy "appstore.web.indexing.imageUrlPath" toSingle  conf.getString("web.indexing.imageUrlPath")
    module.bind[Long] idBy  "future.timeOut" toSingle conf.getLong("web.indexing.futureTimeOut")
    module.bind[AppDetailsPage] toSingle new BasicAppDetailsPage
    module.bind[SiteMapPage] toSingle new DefaultSiteMapPage

})