/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing.page

import scala.xml.Elem
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.indexing.IndexingModule
import hms.appstore.api.json.Application

class BasicAppDetailsPage extends AppDetailsPage with Injectable {
  val bindingModule = IndexingModule
  private val baseUrl = inject[String]("appstore.web.indexing.baseUrl")
  private val imageUrlPath = inject[String]("appstore.web.indexing.imageUrlPath")

  def render(app: Application): Elem = {
      <html>
        <head>
          <meta id="fbOgTitle" property="og:title" content={app.displayName}/>
          <meta id="fbMetaInfo" property="og:description" content={app.description}/>
          <meta name="keywords" content={s"${app.displayName} , ${app.description}"}/>
          <title>{app.displayName}</title>
        </head>
        <body>
          <img src={s"${baseUrl}${imageUrlPath}${app.appIcon}"}/>
          <p>Popularity: <span class="rating">{app.rating}</span></p>
          <p>{app.chargingLabel} {app.chargingDetails}</p>
          <p>{app.description}</p>
        </body>
      </html>
  }

}
