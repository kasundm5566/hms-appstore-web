/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing.page

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}
import scala.xml.Elem

class DefaultSiteMapPage(implicit val bindingModule: BindingModule) extends SiteMapPage with Injectable {
  val appstoreUrl = inject[String]("appstore.web.indexing.baseUrl")

  def render(appIds: List[String]): Elem = {
      <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        {appIds.map(id =>
          <url>
            <loc>{appstoreUrl}view?appId={id}</loc>
          </url>
        )}
      </urlset>
  }
}
