/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing

import javax.servlet.http.{HttpServletResponse, HttpServletRequest, HttpServlet}
import com.typesafe.scalalogging.slf4j.Logging
import javax.servlet.ServletConfig

class SiteOwnershipServlet extends HttpServlet with Logging{

  private var ownershipContent = ""

  override def init(config: ServletConfig) {
    ownershipContent = config.getInitParameter("ownership.content")
    logger.debug("Ownership content found [{}]",ownershipContent)
  }

  override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
    resp.setContentType("text/plain")
    val writer = resp.getWriter
    writer.print(ownershipContent)
    resp.setStatus(HttpServletResponse.SC_OK)
  }
}
