/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing.v2

import com.typesafe.scalalogging.slf4j.Logging

import javax.servlet._
import javax.servlet.http.HttpServletRequest

class IndexingFilterV2 extends Filter with Logging{
  private var supportedUserAgents = Set("Googlebot")

  def init(fg: FilterConfig) {
    supportedUserAgents = supportedUserAgents ++ parseUserAgentHeader(fg.getInitParameter("supported.useragents"))
    logger.debug("supported bot user agents [{}]", supportedUserAgents)
  }

  private def parseUserAgentHeader(header: String): Set[String] = {
    Option(header).map(_.split(";").toSet).getOrElse(Set.empty)
  }

  def doFilter(servletRequest: ServletRequest, servletResponse: ServletResponse, filterChain: FilterChain) {
    val request: HttpServletRequest = servletRequest.asInstanceOf[HttpServletRequest]

    val uas = parseUserAgentHeader(request.getHeader("User-Agent"))

    logger.debug("dispatching user agent [{}]", uas.mkString(";"))

    if (uas.exists(ua => supportedUserAgents.contains(ua))) {
      logger.debug("detected bot user agent [{}]", uas)
      request.getRequestURI match {
        case path if path.endsWith("site-map.xml") => {
          logger.debug("dispatching bot [{}] to /appstore/indexing/site-map.xml", uas)
          request.getRequestDispatcher("/indexing/site-map.xml").forward(servletRequest, servletResponse)
        }
        case _@path => {
          logger.debug("bot [{}] is requesting non seo page [{}]", uas, path)
          filterChain.doFilter(servletRequest, servletResponse)
        }
      }
    } else {
      logger.debug("dispatching non bot user agent [{}]", uas)
      filterChain.doFilter(servletRequest, servletResponse)
    }

  }

  def destroy() {}
}
