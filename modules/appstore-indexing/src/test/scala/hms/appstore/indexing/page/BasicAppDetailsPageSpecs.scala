/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing.page

import org.specs2.mutable.Specification
import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.api.json.Application
import hms.appstore.indexing.IndexingModule
import org.scalamock.specs2.MockFactory

class BasicAppDetailsPageSpecs extends Specification with MockFactory with Injectable {
  val bindingModule = IndexingModule

  val appDetailsPage = inject[AppDetailsPage]

  "app page" should {

    "render app details" in {
      val app = stub[Application]

      (app.displayName _).when().returns("Angry Bird")
      (app.category _).when().returns("Games")
      (app.description _).when().returns("Get more fun with Angry birds")
      (app.appIcon _).when().returns("/image/angry-bird/large.jpg")

      val html = appDetailsPage.render(app)

      html.text.contains("Angry Bird") must beTrue
    }

  }

}
