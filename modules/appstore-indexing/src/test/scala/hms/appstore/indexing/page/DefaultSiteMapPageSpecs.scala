/*
 * (C) Copyright 2010-2013 hSenid Software International (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Software International (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Software International (Pvt) Limited.
 *
 * hSenid Software International (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.indexing.page

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.indexing.IndexingModule
import org.specs2.mutable.Specification


class DefaultSiteMapPageSpecs extends Specification with Injectable {

  val bindingModule = IndexingModule

  val siteMap = inject[SiteMapPage]
  val appStoreUrl = inject[String]("appstore.web.indexing.baseUrl")

  "Site map generator" should {

    "render site map xml" in {
      val xml = siteMap.render(List("APP_0001", "APP_0002", "APP_0003"))

      xml.text.contains(s"$appStoreUrl#!app_APP_0001") must beTrue
    }

  }

}
