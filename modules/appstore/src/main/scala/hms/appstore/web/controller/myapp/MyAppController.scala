/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller.myapp

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

import java.lang.String
import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.web.domain.{BannerLocation, UserDetails}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.{BannerView, ApplicationView}
import hms.appstore.web.service.{ServiceModule, WebConfig}

import org.springframework.ui._
import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.util.WebUtil._
import hms.appstore.api.json.{UpdateCountResp, QueryResult}
import scala.collection.mutable._

@Controller
class MyAppController extends Injectable with WebConfig with Logging {


  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/myApps"), method = Array(RequestMethod.GET))
  def getUserPage(model: Model, req: HttpServletRequest): String = {

    val userDetails = UserDetails(req)
    val userName = userDetails.username.get
    val sessionid = userDetails.sessionId.get
    val authenticated = userDetails.isAuthenticated

    logger.debug("exploring my applications userId [{}] sessionId [{}] authenticated [{}]",userName,sessionid,authenticated.toString)

    val categoriesFuture = discoveryService.categories()
    val mostlyUsedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val myDownloadsFuture = discoveryService.myDownloadsWithBinaries(sessionid)
    val mySubscriptionsFuture = discoveryService.mySubscriptions(sessionid)
    val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
                                               case _ => Future.apply(QueryResult(UpdateCountResp(0))) }
    val appList = Await.result(myDownloadsFuture.map{ x => x.getResults}, discoveryApiTimeOut)
    val updateList = scala.collection.mutable.ArrayBuffer.empty[hms.appstore.api.json.Application]

    appList.foreach(x => {
      val contentId = x.downloadStatus.map {
        u => u.contentId.toLong
      }
      x.downloadableBinaries.map {
        case (mk, mv) => mv.map {
          n => n.map {
            case (k, v) => v.foreach {
              l => {
                if (contentId.getOrElse(0.toLong) < l("content-id").toLong) {
                  updateList += x
                }
              }
            }
          }
        }
      }
    })

    val allResult = for {
      mySub <- mySubscriptionsFuture
      myDwn <- myDownloadsFuture
      mostUsed <- mostlyUsedFuture
      categories <- categoriesFuture
      updateCount <- appUpdateCount
    } yield (mySub.getResults.map(ApplicationView(_)),
        myDwn.getResults.map(ApplicationView(_)),
        mostUsed.getResults.map(ApplicationView(_)),
        categories.getResults.map(a => a.id),
        updateCount.getResult
        )

    val userId = userDetails.userName
    val (mySubscriptions, myDownloads, mostUsedApps, categoryList, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.MY_APPS.toString)
    val appBanners = Await.result(bannersFuture, discoveryApiTimeOut).getResults.map( BannerView(_) )

    val popularAppsFuture = discoveryService.popularApps(start = 0, limit = numOfAppsForRightPanel)
    val popularApps = Await.result(popularAppsFuture, discoveryApiTimeOut).getResults.map(ApplicationView(_))
    val baseUrl = appstoreBaseUrl

    model.addAllAttributes(
      Map(
        "appBanners" -> appBanners.asJava,
        "popularApps" -> popularApps.asJava,
        "userName" -> userName,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(req),
        "mostUsedApps" -> mostUsedApps.asJava,
        "subAppList" -> mySubscriptions.asJava,
        "dwnAppList" -> myDownloads.asJava,
        "updateList" -> updateList.distinct.map(ApplicationView(_)).asJava,
        "appCategories" -> categoryList.asJava,
        "userRole" -> isAdminRole(req),
        "userId" -> userId,
        "active_page" -> "user",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> appstoreBaseUrl
      ).asJava
    )
    addLeftPanelBanners(model, discoveryService)
    "my-apps"
  }
}
