package hms.appstore.web.service

import javax.servlet._
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import scala.collection.JavaConversions._
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.domain.UserDetails
import hms.appstore.web.security.IpValidationService
import org.apache.commons.fileupload.servlet.ServletFileUpload
import hms.appstore.web.util.InAppKeyBox
import hms.appstore.web.util.WebUtil._

class AutoLoginFilter extends Filter with Logging with WebConfig {
  def destroy() {}



  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {

    val httpServletRequest = request.asInstanceOf[HttpServletRequest]
    val httpServletResponse = response.asInstanceOf[HttpServletResponse]
    val requestURI = httpServletRequest.getRequestURI

    //save the in-app request data in session
    preserveInAppRequest(httpServletRequest)

    val session = httpServletRequest.getSession(false)

    if (requestURI.contains("resources") || requestURI.contains("images")) {
      chain.doFilter(request, response)
      return
    }


    val hostIpHeader = httpServletRequest.getHeader("X-Forwarded-For")
    val hostIp = if (hostIpHeader.contains(",")) {
      hostIpHeader.split(",").last.trim
    } else {
      hostIpHeader.trim
    }
    val isValidHost = IpValidationService.getInstance().isValidHost(hostIp)

    val autologinNegotiated = if (session != null) {
      session.getAttribute(UserDetails.AutoLoginNegotiated) != null
    } else {
      false
    }

    if (session != null && !autologinNegotiated && isValidHost) {
      session.setAttribute(UserDetails.AutoLoginNegotiated, true)
      logger.debug("found a valid user  host-ip [{}] >> redirecting to [{}] ", hostIp, autologinRedirectPath)
      httpServletResponse.sendRedirect(autologinRedirectPath)
      return
    }
    chain.doFilter(request, response)
  }

  def init(filterConfig: FilterConfig) {}

  private def preserveInAppRequest(request: HttpServletRequest) {
    val session = request.getSession
    val requestURI = request.getRequestURI
    var iapRequest: String = null

    val alreadySavedIapReq = session.getAttribute(InAppKeyBox.IAP_REQUEST_RAW) != null

    if (requestURI.contains("in-app-purchase") && !alreadySavedIapReq) {
      val iterator = (new ServletFileUpload).getItemIterator(request)
      if (iterator.hasNext) {
        val item = iterator.next
        val stream = item.openStream
        if (item.isFormField) {
          if (item.getFieldName == "iap-request") {
            val data = new Array[Byte](stream.available)
            stream.read(data)
            iapRequest = new String(data)
          }
        }
      }
      session.setAttribute(InAppKeyBox.DO_AUTO_LOGIN_REDIRECT, true)
      session.setAttribute(InAppKeyBox.IAP_REQUEST_RAW, iapRequest)
      logger.debug("in app request data [{}] ", iapRequest)
    }
  }
}
