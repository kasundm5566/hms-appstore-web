/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.domain

import scala.beans.BeanProperty

class DownloadResponseMsg {

  @BeanProperty var appId: String = ""
  @BeanProperty var contentId: String = ""
  @BeanProperty var statusCode: String = ""
  @BeanProperty var downloadRequestId: String = ""
  @BeanProperty var downloadStateDesc: String = ""
}

object DownloadResponseMsg {

  def apply(statusCode: String,
            appId: String,
            contentId: String,
            downloadRequestId: String,
            downloadStateDesc: String): DownloadResponseMsg = {
    val responseMsg: DownloadResponseMsg = new DownloadResponseMsg

    responseMsg.appId = appId
    responseMsg.contentId = contentId
    responseMsg.statusCode = statusCode
    responseMsg.downloadRequestId = downloadRequestId
    responseMsg.downloadStateDesc = downloadStateDesc

    responseMsg
  }
}