/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.ui._
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

import java.lang.String
import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable

import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}

import hms.appstore.web.util.WebUtil
import hms.appstore.web.domain.{BannerLocation, UserDetails}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.{BannerView, ApplicationView}
import hms.appstore.web.service.{ServiceModule, WebConfig}
import hms.appstore.web.util.WebUtil._
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.json.{UpdateCountResp, QueryResult}
import java.net.URLEncoder

@Controller
class CategoryViewController extends Injectable with WebConfig with Logging {

  val bindingModule = ServiceModule
  implicit val executionContext = inject[ExecutionContext]
  private val discoveryService = inject[DiscoveryService]

  @RequestMapping(value = Array("/apps"), method = Array(RequestMethod.GET))
  def viewAllByCategory(model: Model, request: HttpServletRequest): String = {

    val category = request.getParameter("category")

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val authenticated = userDetails.isAuthenticated
    val userId =   userDetails.userName

    val categoriesFuture = discoveryService.categories()
    val appsByCategoryFuture = discoveryService.appsByCategoryWithoutSorting(category = category, start = start, limit = limit)
    val mostUsedbyCategyFuture = discoveryService.mostlyUsedAppsByCategory(start = 0, limit = numOfAppsForRightPanel, category = category)
    val appsByCategoryLenFuture = discoveryService.countByCategory(category)
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.CATEGORIES.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- appsByCategoryFuture
      category <- categoriesFuture
      appCount <- appsByCategoryLenFuture
      mostUsed <- mostUsedbyCategyFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostUsed.getResults.map(ApplicationView(_)),
        category.getResults.map(a => a.id),
        appCount.getResult.count,
        updateCount.getResult)

    val (banners, apps, mostUsedApps, appCategories, allCount, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount.asInstanceOf[Int], baseUrl = s"apps?category=${category}").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "mostUsedApps" -> mostUsedApps.asJava,
        "panelTitle" -> s"$category",
        "pagination" -> pagination.asJava,
        "appCategories" -> appCategories.asJava,
        "authenticated" -> authenticated,
        "active" -> (start / viewAllResultPerPage),
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "userId" -> userId,
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )
    "category-view-new"
  }

  @RequestMapping(value = Array("/top-rated"), method = Array(RequestMethod.GET))
  def viewAllTopRated(model: Model, request: HttpServletRequest): String = {

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val authenticated = userDetails.isAuthenticated
    val userId = userDetails.userName

    val topRatedFuture = discoveryService.topRatedApps(start = start, limit = limit)
    val topRatedLenFuture = discoveryService.topRatedApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUsedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.TOP_RATED_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- topRatedFuture
      category <- categoriesFuture
      mostList <- mostlyUsedFuture
      appLength <- topRatedLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostlyUsed, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "top-rated?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "mostUsedApps" -> mostlyUsed.asJava,
        "panelTitle" -> "Top Rated Apps",
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "active" -> (start / viewAllResultPerPage),
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "userId" -> userId,
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }

  @RequestMapping(value = Array("/newly-added"), method = Array(RequestMethod.GET))
  def viewAllNewlyAdded(model: Model, request: HttpServletRequest): String = {

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val newAddedLenFuture = discoveryService.newlyAddedApps(start = 0, limit = maximumAppsToLoad)
    val newAddedFuture = discoveryService.newlyAddedApps(start = start, limit = limit)
    val mostlyUsedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.NEWLY_ADDED_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- newAddedFuture
      mostList <- mostlyUsedFuture
      category <- categoriesFuture
      appLength <- newAddedLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostUsedApps, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "newly-added?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "mostUsedApps" -> mostUsedApps.asJava,
        "panelTitle" -> "Newly Added Apps",
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "active" -> (start / viewAllResultPerPage),
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "userId" -> userId,
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )
    "category-view-new"
  }

  @RequestMapping(value = Array("/mostly-used"), method = Array(RequestMethod.GET))
  def viewAllMostUsed(model: Model, request: HttpServletRequest): String = {

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val mostUsedLenFuture = discoveryService.mostlyUsedApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUsedFuture = discoveryService.mostlyUsedApps(start = start, limit = limit)
    val newlyAddedFuture = discoveryService.newlyAddedApps(start = 0, limit = numOfAppsForRightPanel)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.MOST_USED_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- mostlyUsedFuture
      newList <- newlyAddedFuture
      category <- categoriesFuture
      appLength <- mostUsedLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        newList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, newAppList, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "mostly-used?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "panelTitle" -> "Most Used Apps",
        "mostUsedApps" -> newAppList.asJava,
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "active" -> (start / viewAllResultPerPage),
        "userId" -> userId,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }

  @RequestMapping(value = Array("/free-apps"), method = Array(RequestMethod.GET))
  def viewAllFreeApps(model :Model,request:HttpServletRequest):String={

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val freeLenFuture = discoveryService.freeApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUdedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val freeAppsFuture = discoveryService.freeApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.FREE_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- freeAppsFuture
      mostList <- mostlyUdedFuture
      category <- categoriesFuture
      appLength <- freeLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostUsedApps, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "free-apps?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "panelTitle" -> "Free Apps",
        "mostUsedApps" -> mostUsedApps.asJava,
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "active" -> (start / viewAllResultPerPage),
        "userId" -> userId,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }

  @RequestMapping(value = Array("/featured-apps"), method = Array(RequestMethod.GET))
  def viewAllFeaturedApps(model :Model,request:HttpServletRequest):String={

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val featuredLenFuture = discoveryService.featuredApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUdedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val featuredAppsFuture = discoveryService.featuredApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.FEATURED_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- featuredAppsFuture
      mostList <- mostlyUdedFuture
      category <- categoriesFuture
      appLength <- featuredLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostUsedApps, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "featured-apps?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "panelTitle" -> "Featured Apps",
        "mostUsedApps" -> mostUsedApps.asJava,
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "active" -> (start / viewAllResultPerPage),
        "userId" -> userId,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }


  @RequestMapping(value = Array("/subscription-apps"), method = Array(RequestMethod.GET))
  def viewAllSubscriptionApps(model :Model,request:HttpServletRequest):String={

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val subscriptionLenFuture = discoveryService.subscriptionApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUdedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val subscriptionAppsFuture = discoveryService.subscriptionApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.SUBSCRIPTION_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- subscriptionAppsFuture
      mostList <- mostlyUdedFuture
      category <- categoriesFuture
      appLength <- subscriptionLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostUsedApps, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "subscription-apps?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "panelTitle" -> "Subscription Apps",
        "mostUsedApps" -> mostUsedApps.asJava,
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "active" -> (start / viewAllResultPerPage),
        "userId" -> userId,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }


  @RequestMapping(value = Array("/downloadable-apps"), method = Array(RequestMethod.GET))
  def viewAllDownloadableApps(model :Model,request:HttpServletRequest):String={

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val downloadableLenFuture = discoveryService.downloadableApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUdedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val downloadableAppsFuture = discoveryService.downloadableApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.DOWNLOADABLE_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- downloadableAppsFuture
      mostList <- mostlyUdedFuture
      category <- categoriesFuture
      appLength <- downloadableLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostUsedApps, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "downloadable-apps?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "banners" -> banners.asJava,
        "appList" -> apps.asJava,
        "panelTitle" -> "Downloadable Apps",
        "mostUsedApps" -> mostUsedApps.asJava,
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "active" -> (start / viewAllResultPerPage),
        "userId" -> userId,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }


  @RequestMapping(value = Array("/popular-apps"), method = Array(RequestMethod.GET))
  def viewAllPopularApps(model :Model,request:HttpServletRequest):String={

    val start = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val popularLenFuture = discoveryService.popularApps(start = 0, limit = maximumAppsToLoad)
    val mostlyUdedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val popularAppsFuture = discoveryService.popularApps(start = start, limit = limit)
    val categoriesFuture = discoveryService.categories()
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.POPULAR_APPS.toString)
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      bannerList <- bannersFuture
      appsList <- popularAppsFuture
      mostList <- mostlyUdedFuture
      category <- categoriesFuture
      appLength <- popularLenFuture
      updateCount <- retrieveAppUpdateCount(request, authenticated)
    } yield (
        bannerList.getResults.map( BannerView(_) ),
        appsList.getResults.map(ApplicationView(_)),
        mostList.getResults.map(ApplicationView(_)),
        appLength.getResults.length,
        category.getResults.map(a => a.id),
        updateCount.getResult)

    val (banners, apps, mostUsedApps, allCount, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = "popular-apps?").toList
    } else List.empty

    addPaginationParameters(model)
    addLeftPanelBanners(model, discoveryService)
    model.addAllAttributes(
      Map(
        "appList" -> apps.asJava,
        "banners" -> banners.asJava,
        "panelTitle" -> "Popular Apps",
        "mostUsedApps" -> mostUsedApps.asJava,
        "appCategories" -> categories.asJava,
        "pagination" -> pagination.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "active" -> (start / viewAllResultPerPage),
        "userId" -> userId,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )

    "category-view-new"
  }


  private def retrieveAppUpdateCount(request: HttpServletRequest, isAuthenticated: Boolean):
  Future[QueryResult[UpdateCountResp]] = {
    isAuthenticated match { case true => discoveryService.updateCount(UserDetails(request).sessionId.get)
                            case _ => Future.apply(QueryResult(UpdateCountResp(0))) }
  }
}
