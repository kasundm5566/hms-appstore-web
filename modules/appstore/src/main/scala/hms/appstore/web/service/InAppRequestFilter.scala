package hms.appstore.web.service

import javax.servlet._
import javax.servlet.http.HttpServletRequest
import com.typesafe.scalalogging.slf4j.Logging
import org.apache.commons.fileupload.servlet.ServletFileUpload
import hms.appstore.web.util.InAppKeyBox

class InAppRequestFilter extends Filter with Logging with WebConfig {
   def destroy() {}

   def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {

     val httpServletRequest = request.asInstanceOf[HttpServletRequest]

     logger.debug("InApp Request Filter for [dialog]")
     //save the in-app request data in session
     preserveInAppRequest(httpServletRequest)
     chain.doFilter(request, response)
   }

   def init(filterConfig: FilterConfig) {}

   private def preserveInAppRequest(request: HttpServletRequest) {
     val session = request.getSession
     val requestURI = request.getRequestURI
     var iapRequest: String = null

     val alreadySavedIapReq = session.getAttribute(InAppKeyBox.IAP_REQUEST_RAW) != null

     if (requestURI.contains("in-app-purchase") && !alreadySavedIapReq) {
       val iterator = (new ServletFileUpload).getItemIterator(request)
       if (iterator.hasNext) {
         val item = iterator.next
         val stream = item.openStream
         if (item.isFormField) {
           if (item.getFieldName == "iap-request") {
             val data = new Array[Byte](stream.available)
             stream.read(data)
             iapRequest = new String(data)
           }
         }
       }
       session.setAttribute(InAppKeyBox.DO_AUTO_LOGIN_REDIRECT, true)
       session.setAttribute(InAppKeyBox.IAP_REQUEST_RAW, iapRequest)
       logger.debug("in app request data [{}] ", iapRequest)
     }
   }
 }
