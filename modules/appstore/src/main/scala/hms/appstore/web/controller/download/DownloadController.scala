/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller.download

import hms.appstore.web.domain.{DownloadResponseMsg, UserDetails}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}

import org.springframework.ui.Model
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, ResponseBody, RequestMapping}

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Await}

import javax.ws.rs.Produces
import javax.servlet.http.HttpServletRequest
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class DownloadController extends Injectable with WebConfig with Logging {


  val bindingModule: BindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]


  @ResponseBody
  @Produces(value = Array("application/json"))
  @RequestMapping(value = Array("/downloadNow"), method = Array(RequestMethod.POST))
  def downloadApp(req: HttpServletRequest, model: Model) = {

    val appId = req.getParameter("appId")
    val version = req.getParameter("version")
    val sessionId = UserDetails(req).sessionId.get

    val appDetailsFuture = discoveryService.appDetails(sessionId, appId)
    val appDetails = Await.result(appDetailsFuture, discoveryApiTimeOut).getResult

    val contentId = {
      ApplicationView(appDetails).dnPlatformsAndContentIds.get(version).
        asScala.toList.sortWith(_ > _).head
    }
    val downLoadFuture = discoveryService.download(sessionId, appId, contentId)
    val downResp = Await.result(downLoadFuture, discoveryApiTimeOut)

    val downloadStatus = if (downResp.isSuccess) {
      if (downResp.statusCode.equalsIgnoreCase("S1000")) {
        DownloadResponseMsg(
          downResp.statusCode,
          appId,
          contentId,
          downResp.downloadRequestId.getOrElse(""),
          downResp.statusDescription.getOrElse(""))

      } else if (downResp.statusCode.equalsIgnoreCase("S1005")) {
        DownloadResponseMsg(
          downResp.statusCode,
          appId,
          contentId,
          downResp.downloadRequestId.getOrElse(""),
          downResp.statusDescription.getOrElse(""))
      }
    } else {
      DownloadResponseMsg(
        downResp.statusCode,
        appId,
        contentId,
        downResp.downloadRequestId.getOrElse(""),
        downResp.statusDescription.getOrElse(""))
    }

    logger.debug("Downloading application | appId : [{}] | version : [{}]" +
      " | sessionId : [{}] | contentId :[{}] | downReqId : [{}]",
      appId, version, sessionId, contentId, downResp.downloadRequestId.getOrElse(""))

    downloadStatus
  }

  @ResponseBody
  @RequestMapping(value = Array("/downloadCharged"))
  def downloaderPayment(req: HttpServletRequest, model: Model): String = {

    val downReqId = req.getParameter("downReqId")
    val sessionId = UserDetails(req).sessionId
    val mpaisaPin = Option(req.getParameter("pin"))
    val pInstrument = req.getParameter("pInstrument")

    logger.debug("Downloading charged app | paymentInstrument : [{}] " +
      "| downReqId : [{}] | sessionId : [{}]" +
      "| ping : [{}]",
      pInstrument, downReqId, sessionId, mpaisaPin)

    val downloadRespFuture = if (mpaisaPin == Some("") || mpaisaPin == None) {
      logger.debug("Downloading with mobile account")
      discoveryService.downloadWithMobileAccount(sessionId.get, downReqId, pInstrument)
    } else {
      logger.debug("Downloading with mPaisa account")
      discoveryService.downloadWithMpaisa(sessionId.get, downReqId, pInstrument, mpaisaPin.get)
    }
    val downloadResp = Await.result(downloadRespFuture, discoveryApiTimeOut)
    
    logger.debug("status : [{}] | description : [{}]",
      downloadResp.statusCode, downloadResp.longDescription.getOrElse(""))
    
    downloadResp.statusDescription.getOrElse("")
  }
}
