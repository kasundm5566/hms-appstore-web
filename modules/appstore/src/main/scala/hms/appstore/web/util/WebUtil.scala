/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.util

import hms.appstore.web.service.WebConfig
import scala.collection.mutable.ListBuffer
import javax.servlet.http.HttpServletRequest
import hms.appstore.web.domain.UrlValue
import org.springframework.security.web.savedrequest.DefaultSavedRequest
import org.springframework.security.web.PortResolver
import javax.servlet.ServletRequest
import org.springframework.ui.Model


object WebUtil extends WebConfig {

  def genPagination(searchKey: String, count: Int, baseUrl: String): List[UrlValue] = {

    val retVal = new ListBuffer[UrlValue]
    var skip = 0
    val numOfUrls = Math.ceil(count.toDouble / searchResultPerPage)

    for (i <- 1 to numOfUrls.toInt) {
      retVal.append(new UrlValue(
        _path = s"$baseUrl?searchKey=$searchKey&skip=$skip&limit=$searchResultPerPage",
        _index = i
      ))
      skip += searchResultPerPage
    }
    retVal.toList
  }

  def paginate(baseUrl: String, count: Int): List[UrlValue] = {
    val retVal = new ListBuffer[UrlValue]
    var skip = 0
    val numOfUrls = Math.ceil(count.toDouble / viewAllResultPerPage)

    for (i <- 1 to numOfUrls.toInt) {
      retVal.append(
        new UrlValue(
          _path = s"$baseUrl&skip=$skip&limit=$viewAllResultPerPage",
          _index = i
        )
      )
      skip += viewAllResultPerPage
    }
    retVal.toList
  }


  def allowViewRegistration(req: HttpServletRequest): Boolean = {
    req.isUserInRole("ROLE_APP_VIEW_REGISTRATION")
  }

  def isAdminRole(req: HttpServletRequest) = {
    req.isUserInRole("ROLE_APP_ADMIN")
  }

  def rememberToRedirect(request: HttpServletRequest, userReferer:Boolean=true) {

    val referer = request.getHeader("Referer")
    val session = request.getSession(false)
    if (referer != null) {
      val savedRequest = new DefaultSavedRequest(request, new PortResolver {
        def getServerPort(request: ServletRequest): Int = request.getLocalPort
      }, userReferer)
      session.setAttribute("SPRING_SECURITY_SAVED_REQUEST_KEY", savedRequest)
    }
  }

  def addPaginationParameters(model: Model) {

    model.addAttribute("appsPerPage", viewAllResultPerPage)
    model.addAttribute("numberOfPages", paginationNodes)
  }
}
