/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller.purchase

import org.springframework.stereotype.Controller
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.web.service.{ServiceModule, WebConfig}
import com.typesafe.scalalogging.slf4j.Logging
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import scala.Array
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.apache.commons.fileupload.servlet.ServletFileUpload
import org.apache.commons.fileupload.FileUploadException
import hms.iap.proto.{ProtoJsonMessageDecoder, Messages}
import com.google.protobuf.InvalidProtocolBufferException
import com.googlecode.protobuf.format.JsonFormat.ParseException
import hms.iap.client.IapService
import scala.concurrent.Await
import hms.appstore.web.domain.{UserDetails, IapPurchaseInfo}
import hms.appstore.web.util.InAppKeyBox
import hms.iap.proto.Messages.IapResponse
import org.springframework.ui.Model


/**
 * Receives in-app purchase request from client application by post method and start processing by redirecting client to
 * in-app confirmation page.
 */
@Controller
class ConnectionController extends Injectable with WebConfig with Logging {
  val bindingModule = ServiceModule
  private val iapService = inject[IapService]

  /**
   * Accepts post from in-app client and call iap-server to verify
   * @param request http request
   * @param response http response
   * @return iap-confirmation / iap-invalid-request
   */
  @RequestMapping(value = Array("/in-app-purchase"))
  def viewAppDetails(model: Model, request: HttpServletRequest, response: HttpServletResponse): String = {

    val userDetails = UserDetails(request)
    val authenticated = userDetails.isAuthenticated

    val httpSession = request.getSession

    if(request.getMethod==RequestMethod.GET){
      "redirect:confirmation"
    }

    if (authenticated) {
      model.addAttribute("authenticated", authenticated)
      model.addAttribute("userId", userDetails.userName)
    }

    val iapRequest: String = httpSession
      .getAttribute(InAppKeyBox.IAP_REQUEST_RAW)
      .asInstanceOf[String]

    if (iapService == null) {
      model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "The in-app purchase request is invalid")
      "iap-error"
    }
    //    try {
    //      val iterator = (new ServletFileUpload).getItemIterator(request)
    //      if (iterator.hasNext) {
    //        val item = iterator.next
    //        val stream = item.openStream
    //        if (item.isFormField) {
    //          if (item.getFieldName == "iap-request") {
    //            val data = new Array[Byte](stream.available)
    //            stream.read(data)
    //            iapRequest = new String(data)
    //          }
    //        }
    //      }
    //    } catch {
    //      case e: FileUploadException =>
    //        logger.error("Error in-app request data format" + e)
    //        model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "The in-app purchase request is invalid")
    //        "iap-error"
    //      case e: Exception =>
    //        logger.error("Error in in-app purchase request dispatching" + e)
    //        model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "The in-app purchase request is invalid")
    //        "iap-error"
    //    }

    var decodedRequest: Messages.IapRequest = null
    try {
      decodedRequest = ProtoJsonMessageDecoder.decodeRequest(iapRequest)
      logger.debug("Request Decoded: " + decodedRequest.toString)
      //Keep purchaseInfo inside session
      httpSession.setAttribute(InAppKeyBox.IAP_REQUEST_KEY, decodedRequest)
    } catch {
      case e: InvalidProtocolBufferException =>
        logger.error("Error in-app request Invalid protocol buffer" + e)
        model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "The in-app purchase request is invalid")
        "iap-error"
      case e: ParseException =>
        logger.error("Error in-app request parse exception" + e)
        model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "The in-app purchase request is invalid")
        "iap-error"
    }

    logger.debug("Calling iap-server to verify request before processing")
    val iapVerifyFuture = iapService.verify(decodedRequest)
    val iapVerify: IapResponse = Await.result(iapVerifyFuture, iapApiTimeOut)
    logger.debug("Request verification Result: " + iapVerify.getStatusCode + iapVerify.getStatusDetail)
    if (iapVerify.getStatusCode.startsWith("S")) {
      httpSession.setAttribute(InAppKeyBox.PURCHASE_INFO_KEY, IapPurchaseInfo(decodedRequest))
      logger.debug("Application verified, redirecting to confirmation page")
      "iap-confirmation"
    } else {
      logger.info("****Application not verified: This could be a potentially malicious application!****" + iapVerify.getStatusCode + iapVerify.getStatusDetail)
      model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "Application not verified, This could be a potentially malicious application")
      "iap-error"
    }
  }
}
