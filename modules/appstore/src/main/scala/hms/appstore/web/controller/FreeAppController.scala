/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.ui.Model
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}

import javax.servlet.http.HttpServletRequest
import scala.concurrent.{Future, Await, ExecutionContext}
import hms.appstore.web.domain.UserDetails

import scala.collection.JavaConverters._
import hms.appstore.web.util.WebUtil
import hms.appstore.web.util.WebUtil._
import hms.appstore.api.json.{UpdateCountResp, QueryResult}

@Controller
class FreeAppController extends Injectable with WebConfig {

  val bindingModule = ServiceModule
  implicit val executionContext = inject[ExecutionContext]
  private val discoveryService = inject[DiscoveryService]

  @RequestMapping(value = Array("/freeApps"), method = Array(RequestMethod.GET))
  def displayFreeApplications(model: Model, req: HttpServletRequest): String = {

    val start = Integer.valueOf(Option(req.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(req.getParameter("limit")).getOrElse(viewAllResultPerPage).toString)
    val userDetails = UserDetails(req)
    val userId = UserDetails(req).userName
    val authenticated = userDetails.isAuthenticated

    val categoriesFuture = discoveryService.categories()
    val freeAppsFuture = discoveryService.freeApps(start = start, limit = limit)
    val mostUsedFuture = discoveryService.mostlyUsedApps(start = start, limit = numOfAppsForRightPanel)
    val allFreeAppCountFuture = discoveryService.freeApps(start = 0, limit = Int.MaxValue)
    val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
                                               case _ => Future.apply(QueryResult(UpdateCountResp(0))) }
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      fApps <- freeAppsFuture
      mApps <- mostUsedFuture
      cat <- categoriesFuture
      count <- allFreeAppCountFuture
      updateCount <- appUpdateCount
    } yield (fApps.getResults.map(ApplicationView(_)),
        mApps.getResults.map(ApplicationView(_)),
        cat.getResults.map(a => a.id),
        count.getResults.length,
        updateCount.getResult)

    val (freeApps, mostUsedApps, appCategories, allCount, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (allCount > viewAllResultPerPage) {
      WebUtil.paginate(count = allCount, baseUrl = s"freeApps?").toList
    } else List.empty

    addPaginationParameters(model)
    model.addAllAttributes(
      Map(
        "mostUsedApps" -> mostUsedApps.asJava,
        "appCategories" -> appCategories.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(req),
        "panelTitle" -> "Free Apps",
        "pagination" -> pagination.asJava,
        "appList" -> freeApps.asJava,
        "active" -> (start / viewAllResultPerPage),
        "userRole" -> isAdminRole(req),
        "userId" -> userId,
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )
    "category-view"
  }

}
