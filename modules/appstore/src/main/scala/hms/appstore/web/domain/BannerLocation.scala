package hms.appstore.web.domain

/*
 * (C) Copyright 1997 - 2018 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
object BannerLocation extends Enumeration {
  type BannerLocation = Value

  val HOME = Value("Home")
  val POPULAR_APPS = Value("Popular-apps")
  val SUBSCRIPTION_APPS = Value("Subscription-apps")
  val FEATURED_APPS = Value("Featured-apps")
  val DOWNLOADABLE_APPS = Value("Downloadable-apps")
  val CATEGORIES = Value("Categories")
  val DEV = Value("Dev")
  val MY_APPS = Value("My-apps")
  val LEFT_PANEL = Value("Left-panel")
  val DEVELOPER_PROFILE = Value("Dev")
  val TOP_RATED_APPS = Value("Top-rated-apps")
  val NEWLY_ADDED_APPS = Value("Newly-added-apps")
  val MOST_USED_APPS = Value("Most-used-apps")
  val FREE_APPS = Value("Free-apps")

}
