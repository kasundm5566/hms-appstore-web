package hms.appstore.web.service

import javax.servlet._
import javax.servlet.http.{HttpServletRequest, HttpServletResponse}

class CharsetFilter extends Filter {
  def destroy() {}

  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
    val httpResponse = response.asInstanceOf[HttpServletResponse]
    httpResponse.setCharacterEncoding("UTF-8")
    httpResponse.setContentType("text/html; charset=UTF-8")
    chain.doFilter(request, response)
  }

  def init(filterConfig: FilterConfig) {}
}
