/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller.publish

import org.springframework.ui.Model
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import com.escalatesoft.subcut.inject.{BindingModule, Injectable}

import hms.appstore.api.json.{UpdateCountResp, QueryResult, UpdateReq}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{WebConfig, ServiceModule}
import hms.appstore.api.client.{DiscoveryInternalService, DiscoveryService}

import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}

import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import hms.appstore.web.domain.UserDetails
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class AppPublishingController extends Injectable with WebConfig with Logging {


  val bindingModule: BindingModule = ServiceModule

  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]
  private val discoveryInternalService = inject[DiscoveryInternalService]


  @RequestMapping(value = Array("/publish-app"), method = Array(RequestMethod.GET))
  def processPublish(model: Model, req: HttpServletRequest, res: HttpServletResponse) = {

    val appId = req.getParameter("appId")

    val categoriesFuture = discoveryService.categories()
    val mostUsedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val statusFuture = discoveryInternalService.requestApp(appId, "requesting")

    val statusCode = Await.result(statusFuture, discoveryApiTimeOut)
    val mostUsedList = Await.result(mostUsedFuture, discoveryApiTimeOut).getResults.map(ApplicationView(_))
    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
    case _ => Future.apply(QueryResult(UpdateCountResp(0))) }
    val baseUrl = appstoreBaseUrl

    if (statusCode.isSuccess) {
      val pendingFuture = discoveryInternalService.findAppOrPendingApp(appId)
      val allResult = for {
        category <- categoriesFuture
        pendingApp <- pendingFuture
        updateCount <- appUpdateCount
      } yield (category.getResults.map(a => a.id), pendingApp.getResult, updateCount.getResult)

      val (categories, application, updateCount) = Await.result(allResult, discoveryApiTimeOut)

      model.addAllAttributes(Map(
          "appCategories" -> categories.asJava,
          "status" -> statusCode.status,
          "app" -> ApplicationView(application),
          "appUpdateCount" -> updateCount.count,
          "baseUrl" -> baseUrl
        ).asJava)
    }

    val allResult = for {
      categories <- categoriesFuture
    } yield categories.getResults.map(a => a.id)

    val categories = Await.result(allResult, discoveryApiTimeOut)

    logger.debug("receive app publish request from user [{}] appId [{}]",userId,appId)

    model.addAttribute("appCategories", categories.asJava)
    model.addAttribute("mostUsedApps", mostUsedList.asJava)
    model.addAttribute("authenticated", authenticated)
    model.addAttribute("status", statusCode.status)
    model.addAttribute("userId", userId)

    "app-publish"
  }


  @RequestMapping(value = Array("/update-app"), method = Array(RequestMethod.POST))
  def saveApplication(model: Model, req: HttpServletRequest) = {

    val appId = req.getParameter("appId")
    val pubName = Option(req.getParameter("pubName"))
    val category = Option(req.getParameter("category"))
    val shortDesc = Option(req.getParameter("shortDesc"))
    val description = Option(req.getParameter("description"))
    val instructions = Option(req.getParameter("instructions").trim)

    val appIcon = Option(req.getParameter("appIcon"))
    val label_1 = Option(req.getParameter("label_1")).getOrElse("")
    val label_2 = Option(req.getParameter("label_2")).getOrElse("")
    val label_3 = Option(req.getParameter("label_3")).getOrElse("")

    val screenShot = Option(req.getParameter("screenShot"))
    val screenShot_1 = Option(req.getParameter("screenShot_1"))
    val screenShot_2 = Option(req.getParameter("screenShot_2"))

    val mobileScreenShot_1 = Option(req.getParameter("mobileScreenShot_1"))
    val mobileScreenShot_2 = Option(req.getParameter("mobileScreenShot_2"))
    val mobileScreenShot_3 = Option(req.getParameter("mobileScreenShot_3"))

    //TODO fix naming convention of appstore screenshots
    val screenShots = Map(
      "screenshot_1" -> screenShot,
      "screenshot_2" -> screenShot_1,
      "screenshot_3" -> screenShot_2,
      "mobile_screenshot_1" -> mobileScreenShot_1,
      "mobile_screenshot_2" -> mobileScreenShot_2,
      "mobile_screenshot_3" -> mobileScreenShot_3
    ).collect {
      case (k, v) if v.isDefined && !v.get.trim.isEmpty => (k, v.get.trim)
    }

    val labels = label_1 :: label_2 :: label_3 :: Nil

    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val categoriesFuture = discoveryService.categories()
    val mostUsedFuture = discoveryService.mostlyUsedApps()
    val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
    case _ => Future.apply(QueryResult(UpdateCountResp(0))) }

    val statusFuture = discoveryInternalService.updateApp(
      UpdateReq(
        id = appId,
        category = category,
        appIcon = appIcon,
        description = description,
        shortDesc = shortDesc,
        labels = labels,
        screenShots = screenShots,
        publishName = pubName,
        instructions = Map(operator -> instructions.getOrElse(""))
      )
    )

    val allResult = for {
      categories <- categoriesFuture
      mostlyUsed <- mostUsedFuture
      updateCount <- appUpdateCount
    } yield (
        categories.getResults.map(a => a.id),
        mostlyUsed.getResults.map(ApplicationView(_)),
        updateCount.getResult)

    val status = Await.result(statusFuture, discoveryApiTimeOut)
    val (categories, mostlyUsed, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    logger.debug(s"publishing applicaton  status [{}] discovery-api response [{}] | user [{}] ",status.status,status.description,userId)
    val baseUrl = appstoreBaseUrl
    model.addAttribute("appCategories", categories.asJava)
    model.addAttribute("mostUsedApps", mostlyUsed.asJava)
    model.addAttribute("authenticated", authenticated)
    model.addAttribute("status", status.status)
    model.addAttribute("userId", userId)
    model.addAttribute("appUpdateCount", updateCount.count)
    model.addAttribute("baseUrl", baseUrl)

    "app-publish"
  }

}
