/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import scala.Array

import hms.appstore.web.domain.UserDetails
import javax.servlet.http.HttpServletRequest

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{ResponseBody, RequestMethod, RequestMapping}
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.util.WebUtil

@Controller
class LoginController extends Logging {

  @ResponseBody
  @RequestMapping(value = Array("/checkUserSession"), method = Array(RequestMethod.GET))
  def getRelevantModal(request: HttpServletRequest): String = {

    val result = UserDetails(request).isAuthenticated match {
      case true => "sessionOk"
      case _ => "sessionNotOk"
    }
    logger.debug("LoginController result [{}]", result)
    result
  }

  @ResponseBody
  @RequestMapping(value = Array("/isAdminRole"), method = Array(RequestMethod.GET))
  def checkUserRole(req: HttpServletRequest) = {
    WebUtil.isAdminRole(req) match {
      case true => "isAdmin"
      case false => "notAdmin"
    }
  }
}
