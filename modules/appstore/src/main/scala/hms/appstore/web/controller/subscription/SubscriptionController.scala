/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller.subscription

import com.escalatesoft.subcut.inject.Injectable
import scala.concurrent.{ExecutionContext, Await}

import javax.ws.rs.Produces
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

import hms.appstore.web.domain.UserDetails
import hms.appstore.api.client.DiscoveryService
import org.springframework.stereotype.Controller
import hms.appstore.web.service.{ServiceModule, WebConfig}
import org.springframework.web.bind.annotation.{ResponseBody, RequestMethod, RequestMapping}
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class SubscriptionController extends Injectable with WebConfig with Logging {


  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  @ResponseBody
  @Produces(value = Array("Application/json"))
  @RequestMapping(value = Array("/subscribeNow"), method = Array(RequestMethod.POST))
  def subscribeApp(req: HttpServletRequest, res: HttpServletResponse): String = {

    val appId = req.getParameter("appId")
    val userDetails = UserDetails(req)
    val sessionId = userDetails.sessionId.get
    val paymentMethod = Option(req.getParameter("payment_method"))

    val userId = userDetails.userName
    val subscribeRespFuture = discoveryService.subscribe(sessionId, appId)
    val subscribeResp = Await.result(subscribeRespFuture, discoveryApiTimeOut)

    logger.debug("[{}] just subscribed to app [{}]  discovery-response [{}]",userId,appId,subscribeResp.description)

    s"${subscribeResp.status}|${subscribeResp.statusDescription.getOrElse("")}"
  }

  @ResponseBody
  @Produces(value = Array("Application/json"))
  @RequestMapping(value = Array("/unSubscribeNow"), method = Array(RequestMethod.POST))
  def unSubApp(req: HttpServletRequest, res: HttpServletResponse): String = {

    val appId = req.getParameter("appId")
    val userDetails = UserDetails(req)
    val sessionId = userDetails.sessionId.get

    val userId = userDetails.userName
    val unSubRespFuture = discoveryService.unSubscribe(sessionId, appId)
    val unSubResp = Await.result(unSubRespFuture, discoveryApiTimeOut)

    logger.debug("[{}] unsubscrib from  app [{}] | discovery-response [{}]",userId,appId,unSubResp.description)

    s"${unSubResp.status}|${unSubResp.statusDescription.getOrElse("")}"
  }
}
