/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.appstore.web.util

import scala.Predef.String

/**
 * In-app purchase related keys.
 */
object InAppKeyBox {
  val IAP_REQUEST_KEY = "iap_request"
  val CALLBACK_URL_KEY = "callback_url"
  val IAP_REQUEST_RAW = "iap_raw_request"
  val PURCHASE_INFO_KEY = "purchase_info"
  val USER_AUTH_STATUS_KEY = "user_auth_status"
  val IAP_ERROR_MESSAGE_KEY = "iap_error_message"
  val PURCHASE_INFO_DONE_KEY = "purchase_info_done"
  val IAP_RESPONSE_BASE64_KEY = "iap_response_base64"
  val DO_AUTO_LOGIN_REDIRECT = "do_auto_login_redirect"
  val IAP_RESPONSE_DISPLAYABLE_TEXT_KEY = "iap_response_displayable_text"
}
