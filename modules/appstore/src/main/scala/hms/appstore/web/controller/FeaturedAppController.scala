/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import javax.servlet.http.HttpServletRequest
import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}

import scala.collection.JavaConverters._
import scala.concurrent.{ExecutionContext, Await}

import org.springframework.ui.Model
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import hms.appstore.web.domain.UserDetails
import hms.appstore.web.util.WebUtil._
import hms.appstore.web.util.WebUtil

@Controller
class FeaturedAppController extends Injectable with WebConfig {


  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/featured"), method = Array(RequestMethod.GET))
  def showFeatured(request: HttpServletRequest, model: Model): String = {

    val categoriesFuture = discoveryService.categories()
    val featuredAppFuture = discoveryService.featuredApps(start = 0)

    val allResult = for {
      featuredList <- featuredAppFuture
      categoryList <- categoriesFuture
    } yield (featuredList.getResults.map(ApplicationView(_)), categoryList.getResults.map(a => a.id))

    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val (featuredApps, categories) = Await.result(allResult, discoveryApiTimeOut)
    val baseUrl = appstoreBaseUrl

    model.addAllAttributes(
      Map(
        "appList" -> featuredApps.asJava,
        "appCategories" -> categories.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "appsTitle" -> "Featured Apps",
        "userId" -> userId,
        "baseUrl" -> baseUrl
      ).asJava
    )
    "featured-apps"
  }
}
