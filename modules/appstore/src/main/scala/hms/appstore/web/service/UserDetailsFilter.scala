/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.service

import hms.appstore.web.domain.UserDetails
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}

import scala.concurrent.Await
import com.escalatesoft.subcut.inject.Injectable

import scala.Some
import javax.servlet._
import java.security.Principal
import javax.servlet.http.HttpServletRequest
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.security.{AutologinAuthenticationToken, AnonymousAuthenticationToken}
import spray.http.HttpHeader
import spray.http.HttpHeaders.RawHeader

class UserDetailsFilter extends Injectable with Filter with WebConfig with Logging {

  val bindingModule = ServiceModule

  private val discoveryInternalService = inject[DiscoveryInternalService]
  private val discoveryService = inject[DiscoveryService]

  def init(filterConfig: FilterConfig) {}

  def doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {

    val httpRequest = request.asInstanceOf[HttpServletRequest]
    val userPrincipal: Principal = httpRequest.getUserPrincipal
    val session = httpRequest.getSession
    val requestURI = httpRequest.getRequestURI

    if (requestURI.contains("resources") || requestURI.contains("images")) {
      chain.doFilter(request, response)
      return
    } else {

      Option(userPrincipal) match {
        case Some(principal) if (!principal.isInstanceOf[AnonymousAuthenticationToken]
          && !principal.isInstanceOf[AutologinAuthenticationToken]) => {
          logger.debug("User principal found for a logged in user principal [{}]", principal)
          setUserDetails(principal)
        }
        case Some(principal) if (!principal.isInstanceOf[AnonymousAuthenticationToken]
          && principal.isInstanceOf[AutologinAuthenticationToken]) => {
          logger.debug("User principal found for a autoLogged in user principal [{}]", principal)
          setUserDetails(principal, isAutoLogin = true)
        }
        case x => {
          logger.debug("User principal not found or Anonymous login, principal [{}]", x)
          session.setAttribute(UserDetails.UserDetailsKey, UserDetails())
        }
      }
    }

    def setUserDetails(principal: Principal, isAutoLogin: Boolean = false) {
      if (!isAutoLogin) {
        val authenticateResp = Await.result(
          discoveryInternalService.authenticateByUsername(principal.getName),
          discoveryApiTimeOut
        )
        logger.debug("authResponse [{}] ", authenticateResp.isSuccess.toString)

        if (authenticateResp.isSuccess) {
          val sessionId = authenticateResp.sessionId
          val alias = userPrincipal.getName
          session.setAttribute(UserDetails.UserDetailsKey,
            UserDetails(Some(alias), sessionId, Some(true)))
        } else {
          throw new Exception("Authentication failure from discovery->authenticateByUsername " + principal.getName)
        }
      } else {
        val autoLoginTempToken = session.getAttribute(UserDetails.AutologinKey).asInstanceOf[UserDetails]
        if (autoLoginTempToken != null) {
          session.setAttribute(UserDetails.UserDetailsKey, autoLoginTempToken)
        } else {
          session.setAttribute(UserDetails.UserDetailsKey, UserDetails())
        }
      }
    }


    chain.doFilter(request, response)
  }


  def destroy() {}

}
