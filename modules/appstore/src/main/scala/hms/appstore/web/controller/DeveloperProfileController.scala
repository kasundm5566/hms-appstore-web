/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.appstore.web.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import java.lang.String
import javax.servlet.http.HttpServletRequest
import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.web.domain.{BannerLocation, UserDetails}
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.{BannerView, ApplicationView}
import hms.appstore.web.service.{ServiceModule, WebConfig}
import org.springframework.ui._
import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.util.WebUtil._
import hms.appstore.api.json.{UserDetailsByUsernameReq, UpdateCountResp, QueryResult}
import scala.collection.mutable._

@Controller
class DeveloperProfileController extends Injectable with WebConfig with Logging {

  val bindingModule = ServiceModule

  private val discoveryService = inject[DiscoveryService]

  @RequestMapping(value = Array("/developer-profile"), method = Array(RequestMethod.GET))
  def developerProfile(model: Model, request: HttpServletRequest): String = {

    val developerName = request.getParameter("developerName")
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val appsByDeveloperFuture = discoveryService.appsByDeveloper(start = 0, limit = maximumAppsToLoad, developerName)
    val appsByDeveloper = Await.result(appsByDeveloperFuture, discoveryApiTimeOut).getResults.map(ApplicationView(_))

    val categoriesFuture = discoveryService.categories()
    val categories = Await.result(categoriesFuture, discoveryApiTimeOut).getResults.map(c => c.id)

    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.DEVELOPER_PROFILE.toString)
    val banners = Await.result(bannersFuture, discoveryApiTimeOut).getResults.map(BannerView(_))

    val ud: UserDetails = request.getSession().getAttribute("user.details.key").asInstanceOf[UserDetails]
    val profileFuture = discoveryService.developerProfileDetails(UserDetailsByUsernameReq(developerName))
    val profile = Await.result(profileFuture, discoveryApiTimeOut)
    val baseUrl = appstoreBaseUrl

    model.addAllAttributes(
      Map(
        "developerName" -> developerName,
        "developerImage" -> profile.image,
        "developerFirstName" -> profile.firstName,
        "developerLastName" -> profile.lastName,
        "developerEmail" -> profile.email,
        "banners" -> banners.asJava,
        "appList" -> appsByDeveloper.asJava,
        "appCategories" -> categories.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "userId" -> userId,
        "active_page" -> "",
        "baseUrl" -> baseUrl
      ).asJava
    )
    addLeftPanelBanners(model, discoveryService)
    "developer-profile"

  }

}
