/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.ui._
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

import hms.appstore.web.util.WebUtil._
import hms.appstore.web.domain.{BannerLocation, UserDetails}
import hms.appstore.api.client.{DiscoveryInternalService, DiscoveryService}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}

import com.typesafe.scalalogging.slf4j.Logging
import com.escalatesoft.subcut.inject.Injectable

import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}
import hms.appstore.web.util.{InAppKeyBox, WebUtil}
import org.springframework.security.web.savedrequest.{SavedRequest, DefaultSavedRequest}
import org.springframework.security.web.PortResolver
import javax.servlet.ServletRequest
import hms.appstore.api.json.{UpdateCountResp, QueryResult, ParentCategory}

@Controller
class HomeController extends Injectable with WebConfig with Logging {


  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  private def applyPanelSettings(panelConfigs: List[ParentCategory]
                                 , model: Model
                                 , mostUsedApps: List[ApplicationView]
                                 , newAddedApps: List[ApplicationView]
                                 , topRatedApps: List[ApplicationView]
                                 , freeApps: List[ApplicationView]) {
    panelConfigs.foreach(config => {
      config.id.trim match {
        case "panelTop" => {
          logger.debug("selecting apps for panelTop")
          val panelCategory = resolvePanelCategory(config)
          model.addAttribute("viewAllTop",panelCategory._3)
          model.addAttribute("panelTopTitle",panelCategory._2)
          model.addAttribute("panelTopApps", panelCategory._1.asJava)
        }
        case "panelBottom" => {
          logger.debug("selecting apps for panelBottom")
          val panelCategory = resolvePanelCategory(config)
          model.addAttribute("viewAllBottom",panelCategory._3)
          model.addAttribute("panelBottomTitle",panelCategory._2)
          model.addAttribute("panelBottomApps", panelCategory._1.asJava)
        }
        case "panelRight" | "panelMiddle" => {
          logger.debug("selecting apps for panelRight | panelMiddle")
          val panelCategory = resolvePanelCategory(config)
          model.addAttribute("viewAllMiddle",panelCategory._3)
          model.addAttribute("panelMiddleTitle",panelCategory._2)
          model.addAttribute("panelMiddleApps", panelCategory._1.asJava)
        }
      }
    })
    def resolvePanelCategory(cfg: ParentCategory) = {
      try {
        cfg.category match {
          case "mostlyUsedApps" => (mostUsedApps,mostlyUsedAppTitle,"mostly-used")
          case "freeApps" => (freeApps,freeAppTitle,"free-apps")
          case "topRatedApps" => (topRatedApps,topRatedAppTitle,"top-rated")
          case "newlyAddedApps" => (newAddedApps,newlyAddedAppTitle,"newly-added")
        }
      } catch {
        case e: Exception => logger.debug(e.getMessage); (mostUsedApps,mostlyUsedAppTitle,"mostly-used")
      }
    }
    logger.debug(model.asMap().toString)
  }

  @RequestMapping(value = Array("/"), method = Array(RequestMethod.GET))
  def getIndexPage(req: HttpServletRequest, model: Model): String = {

    val session = req.getSession
    if (session.getAttribute(InAppKeyBox.DO_AUTO_LOGIN_REDIRECT) != null) {
      session.removeAttribute(InAppKeyBox.DO_AUTO_LOGIN_REDIRECT)
      logger.debug("found a in-app-redirect")
      "redirect:in-app-purchase"
    } else {
      val topRatedFuture = discoveryService.topRatedApps(start = 0, limit = numOfTopRatedApps)
      val mostUsedFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
      val newAddedFuture = discoveryService.newlyAddedApps(start = 0, limit = numOfNewlyAddedApps)
      val featuredFuture = discoveryService.featuredApps(start = 0, limit = numOfFeaturedAppsFroSlider)
      val freeFuture = discoveryService.freeApps(start = 0, limit = numOfNewlyAddedApps)
      val categoriesFuture = discoveryService.categories()
      val panelConfigFuture = discoveryService.parentCategories()
      val userDetails = UserDetails(req)
      val authenticated = userDetails.isAuthenticated
      val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
                                                 case _ => Future.apply(QueryResult(UpdateCountResp(0))) }

      val bannersFuture = discoveryService.bannersByLocation(BannerLocation.HOME.toString)
      val banners = Await.result(bannersFuture, discoveryApiTimeOut).getResults
      val bannerUrls = banners.map(b => b.image_url)
      val bannerLinks = banners.map(b => b.link)

      val allResult = for {
        mostUsed <- mostUsedFuture
        newAdded <- newAddedFuture
        topRated <- topRatedFuture
        featured <- featuredFuture
        free <- freeFuture
        category <- categoriesFuture
        panelConfig <- panelConfigFuture
        updateCount <- appUpdateCount
      } yield (mostUsed.getResults.map(ApplicationView(_)),
          newAdded.getResults.map(ApplicationView(_)),
          topRated.getResults.map(ApplicationView(_)),
          featured.getResults.map(ApplicationView(_)),
          free.getResults.map(ApplicationView(_)),
          category.getResults.map(a => a.id),
          panelConfig.getResults,
          updateCount.getResult)

      val userId = userDetails.userName
      val (mostUsedApps, newAddedApps, topRatedApps, featuredApps, freeApps, appCategories, panelSettings, updateCount) = Await.result(allResult, discoveryApiTimeOut)

      applyPanelSettings(panelSettings
        , model
        , mostUsedApps
        , newAddedApps
        , topRatedApps
        , freeApps)

      val baseUrl = appstoreBaseUrl
      model.addAllAttributes(
        Map(
          "bannerUrls" -> bannerUrls.asJava,
          "bannerLinks" -> bannerLinks.asJava,
          "featuredApps" -> featuredApps.asJava,
          "appCategories" -> appCategories.asJava,
          "authenticated" -> authenticated,
          "showRegLink" -> allowViewRegistration(req),
          "userRole" -> isAdminRole(req),
          "userId" -> userId,
          "appUpdateCount" -> updateCount.count,
          "baseUrl" -> baseUrl
        ).asJava
      )
      addLeftPanelBanners(model, discoveryService)
      "index"
    }
  }

  @RequestMapping(value = Array("/logout"), method = Array(RequestMethod.GET))
  def logout(req: HttpServletRequest, res: HttpServletResponse) {
    val session = Option(req.getSession(false))
    if (session.isDefined) {
      session.get.removeAttribute(UserDetails.UserDetailsKey)
      session.get.invalidate
    }
    res.sendRedirect(casLogoutUrl)
  }

  @RequestMapping(value = Array("/login_direct"), method = Array(RequestMethod.GET))
  def directLogin(request: HttpServletRequest): String = {

//    rememberToRedirect(request,false)
    val casLoginServiceURL = casLoginUrl + "?service=" + casSecurityCheckUrl.replace("http", "https")
    "redirect:" + casLoginServiceURL
  }
}
