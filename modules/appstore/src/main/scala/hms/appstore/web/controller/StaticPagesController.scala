/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import scala.Array
import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.web.service.{ServiceModule, WebConfig}
import hms.appstore.web.domain.UserDetails
import hms.appstore.api.client.DiscoveryService

import scala.concurrent.{Future, ExecutionContext, Await}
import scala.collection.JavaConverters._

import org.springframework.ui.Model
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import javax.servlet.http.HttpServletRequest
import hms.appstore.web.util.WebUtil._
import hms.appstore.web.util.WebUtil
import hms.appstore.api.json.{UpdateCountResp, QueryResult}

@Controller
class StaticPagesController extends Injectable with WebConfig {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/terms-and-conditions"), method = Array(RequestMethod.GET))
  def getTermsPage(model: Model, req: HttpServletRequest): String = {

    val categories = Await.result(
      discoveryService.categories(), discoveryApiTimeOut
    )

    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val updateCount = retrieveAppUpdateCount(req, authenticated)

    model.addAllAttributes(Map(
      "appCategories" -> categories.getResults.map(a => a.id).asJava,
      "userId" -> userId,
      "authenticated" -> authenticated,
      "userRole" -> isAdminRole(req),
      "showRegLink" -> allowViewRegistration(req),
      "appUpdateCount" -> updateCount
    ).asJava)

    "appstore-terms"
  }

  @RequestMapping(value = Array("/privacy-policy"), method = Array(RequestMethod.GET))
  def getPrivacyPage(model: Model, req: HttpServletRequest): String = {

    val categories = Await.result(
      discoveryService.categories(), discoveryApiTimeOut
    )

    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val updateCount = retrieveAppUpdateCount(req, authenticated)

    model.addAllAttributes(Map(
        "appCategories" -> categories.getResults.map(a => a.id).asJava,
        "userId" -> userId,
        "showRegLink" -> allowViewRegistration(req),
        "userRole" -> isAdminRole(req),
        "authenticated" -> authenticated,
        "appUpdateCount" -> updateCount
      ).asJava)

    "appstore-privacy"
  }

  @RequestMapping(value = Array("/contact"), method = Array(RequestMethod.GET))
  def getContactPage(model: Model, req: HttpServletRequest): String = {

    val categories = Await.result(discoveryService.categories(), discoveryApiTimeOut)

    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val updateCount = retrieveAppUpdateCount(req, authenticated)

    model.addAllAttributes(Map(
        "appCategories" -> categories.getResults.map(a => a.id).asJava,
        "userId" -> userId,
        "authenticated" -> authenticated,
        "userRole" -> isAdminRole(req),
        "showRegLink" -> allowViewRegistration(req),
        "appUpdateCount" -> updateCount
      ).asJava)

    "appstore-contact"
  }

  @RequestMapping(value = Array("/500"), method = Array(RequestMethod.GET))
  def get500Page(model: Model, req: HttpServletRequest): String = {

    val categories = Await.result(
      discoveryService.categories()
      , discoveryApiTimeOut
    ).getResults.map(c => c.id)

    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val updateCount = retrieveAppUpdateCount(req, authenticated)

    model.addAllAttributes(Map(
        "userId" -> userId,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(req),
        "userRole" -> isAdminRole(req),
        "appCategories" -> categories.asJava,
        "appUpdateCount" -> updateCount
      ).asJava)

    "500"
  }

  @RequestMapping(value = Array("/404"), method = Array(RequestMethod.GET))
  def get404Page(model: Model, req: HttpServletRequest): String = {

    val categories = Await.result(
      discoveryService.categories(),
      discoveryApiTimeOut
    ).getResults.map(c => c.id)

    val userDetails = UserDetails(req)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated
    val updateCount = retrieveAppUpdateCount(req, authenticated)

    model.addAllAttributes(Map(
        "userId" -> userId,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(req),
        "userRole" -> isAdminRole(req),
        "appCategories" -> categories.asJava,
        "appUpdateCount" -> updateCount
      ).asJava)

    "404"
  }

  private def retrieveAppUpdateCount(request: HttpServletRequest, isAuthenticated: Boolean): Int = {
    val updateCountFutureResp = isAuthenticated match {
      case true => discoveryService.updateCount(UserDetails(request).sessionId.get)
      case _ => Future.apply(QueryResult(UpdateCountResp(0)))
    }
    Await.result(updateCountFutureResp.map {
      x => x.getResult.count
    }, discoveryApiTimeOut)
  }
}