/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.appstore.web.controller.purchase

import spray.json._
import spray.json.DefaultJsonProtocol._
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import scala.Array
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import hms.appstore.web.service.{ServiceModule, WebConfig}
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import hms.iap.proto.Messages._
import hms.appstore.web.domain.{IapPurchaseInfo, UserDetails}
import scala.concurrent.Await
import hms.iap.client.IapService
import hms.iap.proto.ProtoJsonMessageEncoder
import hms.appstore.web.util.InAppKeyBox
import org.springframework.ui.Model

@Controller
class PurchaseController extends Injectable with WebConfig with Logging {
  val bindingModule = ServiceModule
  private val iapService = inject[IapService]

  @RequestMapping(value = Array("/purchase"), method = Array(RequestMethod.POST))
  def viewAppDetails(model: Model, request: HttpServletRequest, response: HttpServletResponse) = {
    logger.debug("Received in-app purchase purchase request")

    val userDetails=UserDetails(request)
    val authenticated = userDetails.isAuthenticated

    if(authenticated){
      model.addAttribute("authenticated" , authenticated)
      model.addAttribute("userId" , userDetails.userName)
    }

    val pInstrument = request.getParameter("pInstrument")
    val pin = Option(request.getParameter("pin")).map(p => Map("pin" -> p)).getOrElse(Map.empty)

    val session = request.getSession
    val iapRequest = session.getAttribute(InAppKeyBox.IAP_REQUEST_KEY).asInstanceOf[IapRequest]
    val iapChargingRequest = iapRequest.getExtension(IapChargingRequest.extension)

    logger.debug("Injecting payment instrument, pin and session into charging request")
    logger.debug("additional params is now : [{}] ", pin.toString())
    var iapChargingRequestToSend = IapChargingRequest.newBuilder(iapChargingRequest)
      .clearPaymentInstrument()
      .setPaymentInstrument(pInstrument)
      .build()
    val iapRequestToSend = IapRequest.newBuilder(iapRequest)
      .clearSessionId()
      .clearExtension(IapChargingRequest.extension)
      .setSessionId(UserDetails(request).sessionId.get)
      .setAdditionalParams(pin.toJson.toString())
      .setExtension(IapChargingRequest.extension, iapChargingRequestToSend)
      .build()

    logger.debug("Querying iap service to execute charging request")
    val iapExecuteFuture = iapService.execute(iapRequestToSend)
    val iapExecute: IapResponse = Await.result(iapExecuteFuture, iapApiTimeOut)
    logger.debug("Received iap-server response: " + iapExecute.toString)

    val purchaseInfo = session.getAttribute(InAppKeyBox.PURCHASE_INFO_KEY).asInstanceOf[IapPurchaseInfo]
    session.setAttribute(InAppKeyBox.IAP_RESPONSE_BASE64_KEY, ProtoJsonMessageEncoder.encodeResponse(iapExecute))
    session.setAttribute(InAppKeyBox.IAP_RESPONSE_DISPLAYABLE_TEXT_KEY, iapExecute.getDisplayableText)
    session.setAttribute(InAppKeyBox.CALLBACK_URL_KEY,purchaseInfo.getCallbackUrl)
    session.setAttribute(InAppKeyBox.PURCHASE_INFO_DONE_KEY,purchaseInfo)

    session.removeAttribute(InAppKeyBox.IAP_REQUEST_KEY)
    session.removeAttribute(InAppKeyBox.PURCHASE_INFO_KEY)
    "iap-redirection"
  }

  @RequestMapping(value = Array("/purchase"), method = Array(RequestMethod.GET))
  def invalid(model: Model, request: HttpServletRequest, response: HttpServletResponse) = {
    logger.debug("Received in-app purchase purchase request by GET: not allowed")
    model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "Received in-app purchase purchase request by GET: not allowed")
    "iap-error"
  }
}
