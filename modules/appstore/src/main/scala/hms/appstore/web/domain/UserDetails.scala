/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.domain

import javax.servlet.http.HttpServletRequest

case class UserDetails(username: Option[String] = None, sessionId: Option[String] = None, registered: Option[Boolean] = Some(false)) {
  def isAuthenticated = !isAnonymousUser && sessionId.isDefined

  def isAnonymousUser = username.map(_.equals(UserDetails.AnonymousUserName)).getOrElse(true)

  def userName = username.getOrElse("")

  def isRegistered = registered.get
}


object UserDetails {

  val AutologinKey  = "user.autologin.key"
  val UserDetailsKey = "user.details.key"
  val AnonymousUserName = "user.anonymous.key"
  val AutoLoginNegotiated= "user.autologin.negotiated.key"

  def apply(req: HttpServletRequest): UserDetails = {
    req.getSession.getAttribute(UserDetailsKey).asInstanceOf[UserDetails]
  }
}




