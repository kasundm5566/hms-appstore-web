/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}

import javax.servlet.ServletRequest
import javax.servlet.http.HttpServletRequest

import spray.http.HttpHeader
import spray.http.HttpHeaders.RawHeader

import scala.concurrent.Await

import com.typesafe.scalalogging.slf4j.Logging
import com.escalatesoft.subcut.inject.{BindingModule, Injectable}

import hms.appstore.web.domain.UserDetails
import hms.appstore.web.service.{ServiceModule, WebConfig}
import hms.appstore.api.client.{DiscoveryInternalService, DiscoveryService}

@Controller
class AutoLoginController extends Injectable with Logging with WebConfig {

  val bindingModule: BindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  private val discoveryInternalService = inject[DiscoveryInternalService]

  @RequestMapping(value = Array("autologin"), method = Array(RequestMethod.GET))
  def doSomething(request: ServletRequest): String = {

    val httpRequest = request.asInstanceOf[HttpServletRequest]
    val session = httpRequest.getSession
    val msisdn = httpRequest.getHeader("msisdn")

    logger.debug("authenticating with msisdn for [{}]", getHeaders(request).toString())
    val authenticateResp = Await.result(
      discoveryService.authenticateByMsisdn(getHeaders(request)),
      discoveryApiTimeOut
    )

    if (authenticateResp.isSuccess) {

      val sessionId = authenticateResp.sessionId

      val isRegistered = authenticateResp.statusCode.equals("S1000")
      val alias = if (isRegistered) {
        val response = Await.result(
          discoveryInternalService.userDetailsByMsisdn(msisdn),
          discoveryApiTimeOut
        )
        response.getUserName.getOrElse(msisdn)
      } else {
        msisdn
      }

      val userDetails = UserDetails(Some(alias), sessionId, Some(isRegistered))
      session.setAttribute(UserDetails.AutologinKey, userDetails)
      logger.debug("user authenticated via auto-login process", userDetails.userName)
    }

    "redirect:/"
  }

  private def getHeaders(request: ServletRequest): List[HttpHeader] = {
    val httpServletRequest = request.asInstanceOf[HttpServletRequest]
    val headers = (Option(httpServletRequest.getHeader("X-Forwarded-For")), Option(httpServletRequest.getHeader("msisdn")))
    headers match {
      case (Some(h), Some(m)) => List(RawHeader("msisdn", m), RawHeader("X-Forwarded-For", h))
      case _ => List.empty[HttpHeader]
    }
  }

}
