/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
package hms.appstore.web.controller.purchase

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMapping, RequestMethod}
import scala.Array
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import hms.appstore.web.service.{ServiceModule, WebConfig}
import com.escalatesoft.subcut.inject.Injectable
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.domain.{IapPurchaseInfo, UserDetails}
import scala.concurrent.Await
import hms.appstore.api.client.DiscoveryService
import org.springframework.ui.Model
import scala.collection.JavaConverters._
import hms.appstore.web.util.InAppKeyBox
import hms.appstore.web.util.WebUtil._


@Controller
class ConfirmationController extends Injectable with WebConfig with Logging {
  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]

  @RequestMapping(value = Array("/confirmation"), method = Array(RequestMethod.GET))
  def viewAppDetails(model: Model, request: HttpServletRequest, response: HttpServletResponse): String = {

    val userDetails = UserDetails(request)

    logger.debug("Received in-app purchase confirmation request" + userDetails.userName)

    val authenticated = userDetails.isAuthenticated
    if (authenticated) {

      model.addAttribute("authenticated" , authenticated)
      model.addAttribute("userId" , userDetails.userName)

      logger.debug("In-app request authentication check =>User is authenticated," +
        " querying discovery api for payment instruments")

      val purchaseInfo: IapPurchaseInfo = request.getSession.getAttribute(InAppKeyBox.PURCHASE_INFO_KEY).asInstanceOf[IapPurchaseInfo]
      val appId = purchaseInfo.getApplicationId
      val sessionId = userDetails.sessionId.get

      logger.debug("Querying Discovery API for payment instruments with appid: " + appId + "SessionId: " + sessionId)
      val paymentInstrumentsRespFuture = discoveryService.findPaymentInstruments(sessionId, appId)
      val paymentInstrumentsResp = Await.result(paymentInstrumentsRespFuture, discoveryApiTimeOut)
      logger.debug("Payment instrument call isSuccess [{}]", paymentInstrumentsResp.isSuccess.toString)

      if (paymentInstrumentsResp.isSuccess) {
        logger.debug("Payment Instrument results: [{}]", paymentInstrumentsResp.paymentInstrumentList.toString)
        val paymentInstruments = paymentInstrumentsResp.paymentInstrumentList.get.map(entry => entry.get("name").getOrElse(""))
        if (paymentInstruments.isEmpty) {
          model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "You don't have any payment instruments capable to do this payment")
          "iap-error"
        }
        model.addAttribute("pInstruments", paymentInstruments.asJava)
        "iap-pi-selection"
      } else {
        model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "Application not verified, This could be a potentially malicious application")
        "iap-error"
      }
    }
    else {
      logger.debug("redirecting iap-purchase use to login ")
      rememberToRedirect(request)
      val casLoginServiceURL = casLoginUrl + "?service=" + casSecurityCheckUrl.replace("http", "https")
      "redirect:" + casLoginServiceURL
    }
  }

  @RequestMapping(value = Array("/confirmation"), method = Array(RequestMethod.POST))
  def invalid(model: Model, request: HttpServletRequest, response: HttpServletResponse): String = {
    logger.debug("Received in-app purchase confirmation request by POST: not allowed")
    model.addAttribute(InAppKeyBox.IAP_ERROR_MESSAGE_KEY, "Received in-app purchase request by POST: not allowed")
    "iap-error"
  }

}
