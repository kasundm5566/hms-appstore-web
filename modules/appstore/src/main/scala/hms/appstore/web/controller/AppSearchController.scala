/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}

import javax.servlet.http.HttpServletRequest
import com.escalatesoft.subcut.inject.Injectable

import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}

import hms.appstore.web.util._
import hms.appstore.web.util.WebUtil._
import hms.appstore.web.domain.UserDetails
import hms.appstore.api.json.{UpdateCountResp, QueryResult, AppStatus, SearchQuery}
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}
import com.typesafe.scalalogging.slf4j.Logging

@Controller
class AppSearchController extends Injectable with WebConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]
  private val discoveryInternalService = inject[DiscoveryInternalService]

  @RequestMapping(value = Array("/search"), method = Array(RequestMethod.GET))
  def doSearch(request: HttpServletRequest, model: Model): String = {

    val keyWord = Option(request.getParameter("searchKey")).getOrElse("")
    val searchCategory = Option(request.getParameter("searchCategory"))
    val skip = Integer.valueOf(Option(request.getParameter("skip")).getOrElse(0).toString)
    val limit = Integer.valueOf(Option(request.getParameter("limit")).getOrElse(searchResultPerPage).toString)
    val userDetails = UserDetails(request)
    val userId = userDetails.userName
    val authenticated = userDetails.isAuthenticated

    val mostlyUsedAppsFuture = discoveryService.mostlyUsedApps(start = 0, limit = numOfAppsForRightPanel)
    val categoriesFuture = discoveryService.categories()
    val searchResFuture = discoveryService.searchApps(keyWord,skip,limit)
    val searchResFutureCount = discoveryService.searchApps(keyWord,0,Integer.MAX_VALUE)
    val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
                                               case _ => Future.apply(QueryResult(UpdateCountResp(0))) }
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      searchCount <- searchResFutureCount
      mostUsedApps <- mostlyUsedAppsFuture
      searchedApps <- searchResFuture
      categoryList <- categoriesFuture
      updateCount <-appUpdateCount
    } yield (
      searchCount.getResults.length,
      mostUsedApps.getResults.map(ApplicationView(_)),
      searchedApps.getResults.map(ApplicationView(_)),
      categoryList.getResults.map(a => a.id),
      updateCount.getResult
    )

    val (sCount, mostUsed, searched, categories, updateCount) = Await.result(allResult, discoveryApiTimeOut)

    val pagination = if (sCount > viewAllResultPerPage) {
      WebUtil.paginate(count = sCount, baseUrl = "search?")
    } else List.empty

    addPaginationParameters(model)
    model.addAllAttributes(
      Map(
        "searchKey" -> keyWord,
        "searchResult" -> searched.asJava,
        "mostUsedApps" -> mostUsed.asJava,
        "appCategories" -> categories.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request ),
        "active" -> skip / viewAllResultPerPage,
        "userId" -> userId,
        "pagination" -> pagination.asJava,
        "active_page" -> "",
        "appUpdateCount" -> updateCount.count,
        "baseUrl" -> baseUrl
      ).asJava
    )
    addLeftPanelBanners(model, discoveryService)
    "search"
  }
}
