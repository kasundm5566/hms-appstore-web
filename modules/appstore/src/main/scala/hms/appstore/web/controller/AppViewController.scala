/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import hms.appstore.web.domain.UserDetails
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}
import hms.appstore.api.client.{DiscoveryService, DiscoveryInternalService}

import org.springframework.ui.Model
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{ResponseBody, RequestMethod, RequestMapping}

import javax.servlet.http.HttpServletRequest
import com.escalatesoft.subcut.inject.Injectable

import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}
import hms.appstore.web.util.WebUtil._
import hms.appstore.api.json.{AppRatingAddReq, UpdateCountResp, QueryResult, UserCommentReq}
import scala.Some
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.api.client.util.URLEncoder
import hms.appstore.web.util.WebUtil

@Controller
class AppViewController extends Injectable with WebConfig with Logging {

  val bindingModule = ServiceModule

  implicit val executionContext = inject[ExecutionContext]
  private val discoveryService = inject[DiscoveryService]
  private val discoveryInternalService = inject[DiscoveryInternalService]

  @RequestMapping(value = Array("/view"), method = Array(RequestMethod.GET))
  def viewAppDetails(request: HttpServletRequest, model: Model): String = {

    val userDetails = UserDetails(request)
    val authenticated = userDetails.isAuthenticated
    val appId = request.getParameter("appId")

    val appDetailFuture = if (authenticated) {
      discoveryService.appDetails(appId = appId, sessionId = userDetails.sessionId.get)
    } else {
      discoveryService.appDetails(appId)
    }

    val appDetailFutureResult = Await.result(appDetailFuture, discoveryApiTimeOut).getResult
    val appContentId = appDetailFutureResult.downloadStatus.map{ d => d.contentId.toLong }

    var isUpdateAvailable = false
    appDetailFutureResult.downloadableBinaries.map {
      case (mk, mv) => mv.map {
        n => n.map {
          case (k, v) => v.foreach {
            l =>
              val exists = appContentId.exists(_ < l("content-id").toLong)
              if (exists) {
                isUpdateAvailable = true
              }
          }
        }
      }
    }

    val categoriesFuture = discoveryService.categories()
    val relatedAppFuture = discoveryService.similarApps(appId = appId, start = 0, limit = numOfAppsForRightPanel)
    val appUpdateCount = authenticated match { case true => discoveryService.updateCount(userDetails.sessionId.get)
                                               case _ => Future.apply(QueryResult(UpdateCountResp(0))) }
    val baseUrl = appstoreBaseUrl

    val allResult = for {
      related <- relatedAppFuture
      application <- appDetailFuture
      categories <- categoriesFuture
      updateCount <- appUpdateCount
    } yield (
        related.getResults.map(ApplicationView(_)),
        application.result,
        categories.getResults.map(a => a.id),
        updateCount.getResult
    )

    val response = Await.result(allResult, discoveryApiTimeOut)
    val (relatedApps, appDetails, categories, updateCount) = response
    val userId = userDetails.userName

    val appsByDeveloperFuture = discoveryService.appsByDeveloper(start = 0, limit = countOfDeveloperAppsToShow, appDetails.get.developer)
    val appByDeveloper = Await.result(appsByDeveloperFuture, discoveryApiTimeOut).getResults.map(ApplicationView(_))

    val view: ApplicationView = ApplicationView(appDetails.get)
    val appType = {
      if (view.canDownload && !view.canSubscribe) {
        "download"
      } else if (view.canSubscribe && !view.canDownload) {
        "subscribe"
      } else if (view.canUnSubscribe && view.canDownload) {
        "download"
      } else if (view.canUnSubscribe && !view.canDownload) {
        "unSub"
      }
    }
    model.addAllAttributes(
      Map(
        "appsByDeveloper" -> appByDeveloper.asJava,
        "relatedApps" -> relatedApps.asJava,
        "appDetails" -> view,
        "viewAllRight" -> "no",
        "appCategories" -> categories.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "userId" -> userId,
        "appType" -> appType,
        "appUpdateCount" -> updateCount.count,
        "isUpdateAvailable" -> isUpdateAvailable,
        "baseUrl" -> baseUrl
      ).asJava
    )
    addLeftPanelBanners(model, discoveryService)
    "app-view"

  }


  @ResponseBody
  @RequestMapping(value = Array("/addComment"), method = Array(RequestMethod.POST))
  def addComment(req: HttpServletRequest): String = {

    val time = System.currentTimeMillis()
    val appId = req.getParameter("appId")
    val abused = Option(req.getParameter("abused"))
    val comment = req.getParameter("comments")
    val userDetails = UserDetails(req)
    val userName = userDetails.username
    val sessionId = userDetails.sessionId.get
    val reportAbused = if (abused == Some("true")) Some(true) else Some(false)

    val userComment = UserCommentReq(
      appId = appId,
      dateTime = time,
      commentId = None,
      comments = comment,
      username = userName,
      abuse = reportAbused)

    val responseFuture = reportAbused match {
      case Some(true) => discoveryService.reportAbuse(sessionId, userComment.appId, URLEncoder.encode(userComment.comments), Option("WEB"))
      case _ => discoveryService.addComment(userComment, sessionId)
    }
    val response = Await.result(responseFuture, discoveryApiTimeOut)
    s"${response.description}"
  }

  @RequestMapping(value = Array("/removeComment"), method = Array(RequestMethod.GET))
  def removeComment(req: HttpServletRequest): String = {
    val commentId = req.getParameter("commentId")
    val appId = req.getParameter("appId")
    val futureResult = discoveryInternalService.removeComment(commentId)
    val result = Await.result(futureResult, discoveryApiTimeOut).isSuccess.toString
    logger.debug("Remove comment status:" + result)
    "redirect:view?appId="+appId
  }

  @RequestMapping(value = Array("/viewSimilarApps"), method = Array(RequestMethod.GET))
  def viewSimilar(request: HttpServletRequest, model: Model): String = {

    val userDetails = UserDetails(request)
    val authenticated = userDetails.isAuthenticated
    val appId = request.getParameter("appId")
    val appName = request.getParameter("appName")

    val similarAppsFuture = discoveryService.similarApps(appId = appId)
    val similarApps = Await.result(similarAppsFuture, discoveryApiTimeOut).getResults.map(ApplicationView(_))

    val categoriesFuture = discoveryService.categories()
    val categories = Await.result(categoriesFuture, discoveryApiTimeOut).getResults.map(c => c.id)

    val userId = userDetails.userName
    val baseUrl = appstoreBaseUrl

    addPaginationParameters(model)
    model.addAllAttributes(
      Map(
        "appName" -> appName,
        "similarApps" -> similarApps.asJava,
        "appCategories" -> categories.asJava,
        "authenticated" -> authenticated,
        "showRegLink" -> allowViewRegistration(request),
        "userRole" -> isAdminRole(request),
        "userId" -> userId,
        "baseUrl" -> baseUrl
      ).asJava
    )
    addLeftPanelBanners(model, discoveryService)
    "similar-app-view"
  }

  @RequestMapping(value = Array("/submitRating"), method = Array(RequestMethod.POST))
  def addRating(req: HttpServletRequest) = {
    val appRating = req.getParameter("appRating")
    val appId = req.getParameter("appId")
    logger.debug("Saving the rating")
    val ratingFuture = discoveryService.rateApp(
      AppRatingAddReq(appId, appRating.toInt)
    )
    val response = Await.result(ratingFuture, discoveryApiTimeOut)
    logger.debug("successfully submitted the rating. appId:[{}] rating:[{}] response[{}]", appId, appRating, response)
    "redirect:view?appId="+appId
  }

}
