/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation
import java.util.concurrent.TimeoutException
import spray.client.{PipelineException, UnsuccessfulResponseException}
import com.typesafe.scalalogging.slf4j.Logging
import org.springframework.web.servlet.ModelAndView
import javax.servlet.http.HttpServletRequest
import hms.appstore.web.domain.UserDetails

@ControllerAdvice
class ExceptionHandler extends Logging {

  @annotation.ExceptionHandler(Array(classOf[TimeoutException]))
  def handleTimeoutException(ex: Exception,request:HttpServletRequest) = {
    logger.error("TimeoutException Thrown", ex)
    parseErrorMessage("TIMEOUT_EXCEPTION",request)
  }

  @annotation.ExceptionHandler(Array(classOf[PipelineException], classOf[UnsuccessfulResponseException]))
  def handleCommunicationException(ex: Exception,request:HttpServletRequest) = {
    logger.error("Exceptions Thrown | PipelineException | UnsuccessfulResponseException ", ex)
    parseErrorMessage("COMMUNICATION_EXCEPTION",request)
  }

  @annotation.ExceptionHandler(Array(classOf[Exception]))
  def handleOtherException(ex: Exception,request:HttpServletRequest) = {
    logger.error("Exception Thrown ", ex)
    parseErrorMessage("OTHER_EXCEPTION",request)
  }

  private def parseErrorMessage(message: String,request:HttpServletRequest): ModelAndView = {
    val modelView = new ModelAndView("error")
    val userDetails=UserDetails(request)
    modelView.addObject("authenticated",userDetails.isAuthenticated)
    modelView.addObject("userId",userDetails.userName)
    modelView.addObject("message", message)
    modelView
  }
}
