/*
 * (C) Copyright 2010-2014 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.domain

import scala.Predef.String
import hms.iap.proto.Messages
import scala.beans.BeanProperty

/**
 * To store purchase summery to display in confirmation pages until session.
 */
class IapPurchaseInfo {
  @BeanProperty var applicationId: String = ""
  @BeanProperty var applicationName: String = ""
  @BeanProperty var externalTransactionId: String = ""
  @BeanProperty var amount: String = ""
  @BeanProperty var currency: String = ""
  @BeanProperty var itemName: String = ""
  @BeanProperty var itemDescription: String = ""
  @BeanProperty var callbackUrl: String = ""
}

object IapPurchaseInfo {
  def apply(source: Messages.IapRequest): IapPurchaseInfo = {
    val iapPurchaseInfo: IapPurchaseInfo = new IapPurchaseInfo
    iapPurchaseInfo.applicationId = source.getApplicationId
    iapPurchaseInfo.applicationName = source.getApplicationName
    iapPurchaseInfo.externalTransactionId = source.getExternalTransactionId
    iapPurchaseInfo.callbackUrl = source.getCallbackUrl
    val chargingRequest: Messages.IapChargingRequest = source.getExtension(Messages.IapChargingRequest.extension)
    if (chargingRequest != null) {
      iapPurchaseInfo.amount = chargingRequest.getAmount
      iapPurchaseInfo.currency = chargingRequest.getCurrencyCode
      iapPurchaseInfo.itemName = chargingRequest.getItemName
      iapPurchaseInfo.itemDescription = chargingRequest.getItemDescription
    }
    iapPurchaseInfo
  }
}



