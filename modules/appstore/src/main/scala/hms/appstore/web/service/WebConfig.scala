/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.service

import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import scala.util.matching.Regex
import scala.collection.JavaConverters._
import hms.scala.http.util.ConfigUtils
import org.springframework.ui.Model
import hms.appstore.api.client.DiscoveryService
import hms.appstore.web.domain.BannerLocation
import scala.concurrent.Await

trait WebConfig {

  private val _config = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.web")

  private val _casConfig = ConfigFactory.load("cas.properties")

  private val _panelConfig = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.web.panels")

  private val _indexingConfig = ConfigUtils.prepareSubConfig(ConfigFactory.load(), "appstore.web.indexing")

  /**
   * select operator for add instruction when publishing application
   * @return operator
   */
  def operator: String = _config.getString("operator")

  /**
   * cas login url
   * @return url
   */
  def casLoginUrl: String = _casConfig.getString("cas.login.url")

  /**
   * cas spring security check url
   * @return url
   */
  def casSecurityCheckUrl: String = _casConfig.getString("cas.security.check.url")

  /**
   * cas logout url
   * @return url
   */
  def casLogoutUrl: String = _casConfig.getString("cas.logout.url")

  /**
   * number of pagination nodes to show between next prev buttons
   * @return
   */
  def paginationNodes: Int = _config.getInt("pagination.nodes")

  /**
   * maximum applications to load for a given category
   * @return Integer
   */
  def maximumAppsToLoad: Int = _config.getInt("max.app.per.view")

  /**
   * number of top-rated applications to show in home page
   * @return Integer
   */
  def numOfTopRatedApps: Int = _panelConfig.getInt("toprated.apps")

  /**
   * application icon,screen-shots base path
   * @return String
   */
  def imageDirectoryPath: String = "/hms/data/discovery-api/images"

  /**
   * discovery-api max-timeout
   * @return Duration
   */
  def discoveryApiTimeOut: Duration = Duration(_config.getString("discovery.api.timeout"))

  /**
   * image upload compression quality
   * @return Duration
   */
  def imageCompressionQuality: Float = _config.getString("image.compression.quality").toFloat

  /**
   * temporary image path for upload image
   * @return Duration
   */
  def tempImagePath: String = _config.getString("temp.image.path").toString

  /**
   * iap-api max-timeout
   * @return Duration
   */
  def iapApiTimeOut: Duration = Duration(_config.getString("iap.api.timeout"))

  /**
   * number of newly added applications to show in home page
   * @return Integer
   */
  def numOfNewlyAddedApps: Int = _panelConfig.getInt("newlyadded.apps")

  /**
   * number of maximum applications to show per search result page
   * @return Integer
   */
  def searchResultPerPage: Int = _config.getInt("search.apps")

  /**
   * number of applications to load per page when click view-all link
   * @return Integer
   */
  def viewAllResultPerPage: Int = _config.getInt("viewall.apps")

  /**
   * number of application to list under right-panel
   * @return Integer
   */
  def numOfAppsForRightPanel: Int = _panelConfig.getInt("rightpanel.apps")

  /**
   * number of featured applications to load to home page carousel
   * @return Integer
   */
  def numOfFeaturedAppsFroSlider: Int = _config.getInt("slider.apps")

  /**
   * Allowed host ips to enable autoLogin feature
   * @return List of regex to validate host ips
   */
  def allowedHosts: List[Regex] = _config.getStringList("allowed.hosts").asScala.toList.map(_.r)

  /**
   * http path to obtain msisdn and proceed autologin
   * @return autologin URL
   */
  def autologinRedirectPath: String = _config.getString("autologin.redirect.path")

  /**
   * panel titles
   * @return
   */
  def topRatedAppTitle: String = _config.getString("panelTitle.topRated")

  def mostlyUsedAppTitle: String = _config.getString("panelTitle.mostlyUsed")

  def freeAppTitle: String = _config.getString("panelTitle.free")

  def newlyAddedAppTitle: String = _config.getString("panelTitle.newlyAdded")

  def appstoreBaseUrl: String = _indexingConfig.getString("baseUrl")

  def countOfDeveloperAppsToShow: Int = _config.getInt("developer.apps.count")

  def addLeftPanelBanners(model: Model, discoveryService: DiscoveryService) {
    val bannersFuture = discoveryService.bannersByLocation(BannerLocation.LEFT_PANEL.toString)
    val bannerResult = Await.result(bannersFuture, discoveryApiTimeOut).getResults
    val leftBanners = bannerResult.map(b => b.image_url)
    val leftBannerLinks = bannerResult.map(b => b.link)
    model.addAttribute("leftBanners", leftBanners.asJava)
    model.addAttribute("leftBannerLinks", leftBannerLinks.asJava)
  }

}
