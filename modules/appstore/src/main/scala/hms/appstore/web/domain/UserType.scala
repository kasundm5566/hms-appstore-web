package hms.appstore.web.domain

/*
 * (C) Copyright 1997 - 2018 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */
object UserType extends Enumeration {
  type UserType = Value

  val ADMIN_USER = Value("ADMIN_USER")
  val INDIVIDUAL = Value("INDIVIDUAL")
  val CORPORATE = Value("CORPORATE")

}
