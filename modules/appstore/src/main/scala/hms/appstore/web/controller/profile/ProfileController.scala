package hms.appstore.web.controller.profile

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{ResponseBody, RequestParam, RequestMapping, RequestMethod}

import java.lang.String
import javax.servlet.http.HttpServletRequest

import com.escalatesoft.subcut.inject.Injectable

import hms.appstore.web.domain.UserDetails
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{ServiceModule, WebConfig}

import org.springframework.ui._
import scala.collection.JavaConverters._
import scala.concurrent.{Future, ExecutionContext, Await}
import com.typesafe.scalalogging.slf4j.Logging
import hms.appstore.web.util.WebUtil._
import scala.collection.mutable._
import hms.appstore.api.json.{UserProfileUpdateReq, UserDetailsByUsernameReq}
import spray.http._
import org.springframework.web.multipart.MultipartFile
import java.io.{FileOutputStream, OutputStream, File}
import hms.appstore.api.json.UserDetailsByUsernameReq
import scala.util.Random
import javax.imageio.{IIOImage, ImageWriteParam, ImageWriter, ImageIO}
import java.awt.image.BufferedImage
import javax.imageio.stream.ImageOutputStream
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.view.RedirectView
import org.springframework.web.servlet.mvc.support.RedirectAttributes

/**
 * Created by kasun on 3/27/18.
 */
@Controller
class ProfileController extends Injectable with WebConfig with Logging {

  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/profile"), method = Array(RequestMethod.GET))
  def getUserProfilePage(req: HttpServletRequest, model: Model): String = {
    val userDetails = UserDetails(req)
    val authenticated = userDetails.isAuthenticated
    val userName = userDetails.username.get
    val userId = userDetails.userName

    val categoriesFuture = discoveryService.categories()
    val categories = Await.result(categoriesFuture, discoveryApiTimeOut).getResults.map(c => c.id)

    val ud:UserDetails = req.getSession().getAttribute("user.details.key").asInstanceOf[UserDetails]
    val profileFuture = discoveryService.userProfileDetails( ud.sessionId.get )
    val profile = Await.result(profileFuture, discoveryApiTimeOut)
    val baseUrl = appstoreBaseUrl

    logger.debug("found profile by userId [{}] profile: [{}]", userName, profile)

    model.addAllAttributes(
      Map(
        "appCategories" -> categories.asJava,
        "authenticated" -> authenticated,
        "userRole" -> isAdminRole(req),
        "userId" -> userId,
        "active_page" -> "",
        "showRegLink" -> allowViewRegistration(req),
        "userName" -> userName,
        "userId" -> userId,
        "firstName" -> profile.firstName.getOrElse(""),
        "lastName" -> profile.lastName.getOrElse(""),
        "mobile" -> profile.msisdn,
        "email" -> profile.email.getOrElse(""),
        "userType" -> profile.userType,
        "dob" -> profile.birthDate.getOrElse(""),
        "profileImage_1" -> profile.image,
        "profileImage" -> profile.image.concat("?rand="+Random.nextInt()),
        "baseUrl" -> baseUrl
      ).asJava
    )
    addLeftPanelBanners(model, discoveryService)
    "profile"
  }

  @RequestMapping(value = Array("/updateProfile"), method = Array(RequestMethod.POST) )
  @ResponseBody
  def updateProfile(
                     @RequestParam("profileImage") profileImage: MultipartFile , @RequestParam("fullName") fullName: String ,
                     @RequestParam("email") email: String , @RequestParam("mobileNumber") mobileNumber: String,
                     @RequestParam("userName") userName: String, req: HttpServletRequest) = {
    var firstName = ""
    var lastName = ""
    if(fullName.contains(" ")) {
      lastName = fullName.substring( fullName.indexOf(" ") )
      firstName = fullName.split(" ")(0)
    }

    val ud:UserDetails = req.getSession().getAttribute("user.details.key").asInstanceOf[UserDetails]

    val updateFuture = if (profileImage.getSize == 0L) {
      val updateFuture = discoveryService.updateUserProfile(
        ud.sessionId.get,
        MultipartFormData(
          Seq(
            BodyPart(HttpEntity(email), "email"),
            BodyPart(HttpEntity(mobileNumber), "contactNo"),
            BodyPart(HttpEntity(firstName.trim), "firstName"),
            BodyPart(HttpEntity(lastName.trim), "lastName")
          )
        )
      )
      updateFuture
    } else {
      val file = new File(profileImage.getOriginalFilename)
      profileImage.transferTo(file)
      val compressedFile = compressImage(file)

      val updateFuture = discoveryService.updateUserProfile(
        ud.sessionId.get,
        MultipartFormData(
          Seq(
            BodyPart(compressedFile, "image", MediaTypes.`image/jpeg`),
            BodyPart(HttpEntity(email), "email"),
            BodyPart(HttpEntity(mobileNumber), "contactNo"),
            BodyPart(HttpEntity(firstName.trim), "firstName"),
            BodyPart(HttpEntity(lastName.trim), "lastName")
          )
        )
      )
      updateFuture
    }
    val response = Await.result(updateFuture, discoveryApiTimeOut)

    logger.debug("Update the user:[{}]. response:[{}]", userName, response)
    response.statusCode
  }

  def compressImage(file: File): File = {

    val image:BufferedImage = ImageIO.read(file)
    val output:File = new File( tempImagePath + "/temp-".concat(file.getName))
    val out:OutputStream = new FileOutputStream(output)

    val fileFormat = file.getName.substring(file.getName.lastIndexOf('.') + 1)

    val writer: ImageWriter = ImageIO.getImageWritersByFormatName(fileFormat).next()
    val ios: ImageOutputStream = ImageIO.createImageOutputStream(out)
    writer.setOutput(ios)

    val param: ImageWriteParam = writer.getDefaultWriteParam()
    if (param.canWriteCompressed()){
      param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT)
      param.setCompressionQuality(imageCompressionQuality)
    }

    writer.write(null, new IIOImage(image, null, null), param)

    out.close()
    ios.close()
    writer.dispose()
    output

  }

}
