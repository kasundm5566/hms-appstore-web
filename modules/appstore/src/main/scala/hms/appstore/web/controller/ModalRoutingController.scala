/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.controller

import org.springframework.ui.Model
import com.escalatesoft.subcut.inject.Injectable
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}

import scala.Array
import scala.concurrent.{ExecutionContext, Await}
import scala.collection.JavaConverters._

import hms.appstore.web.util.WebUtil._
import hms.appstore.web.domain.UserDetails
import hms.appstore.api.client.DiscoveryService
import hms.appstore.api.client.domain.ApplicationView
import hms.appstore.web.service.{WebConfig, ServiceModule}

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.{RequestMethod, RequestMapping}
import com.typesafe.scalalogging.slf4j.Logging
import java.util

@Controller
class ModalRoutingController extends Injectable with WebConfig with Logging {


  val bindingModule = ServiceModule
  private val discoveryService = inject[DiscoveryService]
  implicit val executionContext = inject[ExecutionContext]

  @RequestMapping(value = Array("/download"), method = Array(RequestMethod.GET))
  def getDwnPage(req: HttpServletRequest, model: Model): String = {

    val userDetails = UserDetails(req)
    val sessionId = userDetails.sessionId.get
    val appId = req.getParameter("appId")

    val appRequest = Await.result(discoveryService.appDetails(sessionId, appId), discoveryApiTimeOut)
    val application = appRequest.getResult
    val appContentId = application.downloadStatus.map { d => d.contentId.toLong }

    val filteredOsList = new util.HashMap[String, Boolean]()
    val filteredVersionList = new util.HashMap[String, Boolean]()

    application.downloadableBinaries.map {
      case (mk, mv) => mv.map {
        n => n.map {
          case (k, v) => v.foreach {
            l =>
              val exists = appContentId.exists(_ < l("content-id").toLong)
              filteredVersionList.put(mk + "-" + k, exists)
              filteredOsList.put(mk, exists | filteredOsList.get(mk))
          }
        }
      }
    }

    if (appRequest.isSuccess) {
      val appDetails = ApplicationView(application)
      val isCharged = !appDetails.chargingLabel.trim.equalsIgnoreCase("FREE")
      val chargingDescription = appDetails.chargingDetails

      model.addAttribute("isCharged", isCharged)
      model.addAttribute("chargingNotice", chargingDescription)
      model.addAttribute("app", appDetails)
      model.addAttribute("operatingSystems", filteredOsList)
      model.addAttribute("versions", filteredVersionList)

      "app-download-modal"
    } else {
      model.addAttribute("message", appRequest.description)
      "500-modal"
    }
  }

  @RequestMapping(value = Array("/subscribe"), method = Array(RequestMethod.GET))
  def getSubPage(model: Model, req: HttpServletRequest): String = {

    val appId = req.getParameter("appId")
    val userDetails = UserDetails(req)
    val sessionId = userDetails.sessionId.getOrElse("")
    val appDetailsRequest = Await.result(discoveryService.appDetails(sessionId, appId), discoveryApiTimeOut)

    if (appDetailsRequest.isSuccess) {
      val appDetails = appDetailsRequest.getResult
      val isCharged = !appDetails.chargingLabel.trim.equalsIgnoreCase("FREE")
      val chargingDescription = appDetails.chargingDetails

      logger.debug("charging label [{}] isCharged [{}]", appDetails.chargingLabel, isCharged.toString)

      model.addAttribute("appId", appId)
      model.addAttribute("isCharged", isCharged)
      model.addAttribute("app", ApplicationView(appDetails))
      model.addAttribute("chargingNotice", chargingDescription)

      "app-subscribe-modal"
    } else {
      model.addAttribute("message", appDetailsRequest.description)
      "500-modal"
    }

  }

  @RequestMapping(value = Array("/review"), method = Array(RequestMethod.GET))
  def getRevPage(model: Model, req: HttpServletRequest): String = {

    val appId = req.getParameter("appId")
    model.addAttribute("appId", appId)
    if (UserDetails(req).isRegistered) {
      "add-review-modal"
    } else {
      "new-user-register-modal"
    }
  }

  @RequestMapping(value = Array("/rateApp"), method = Array(RequestMethod.GET))
  def getAppRatePage(model: Model, req: HttpServletRequest): String = {
    val appId = req.getParameter("appId")
    model.addAttribute("appId", appId)
    if (UserDetails(req).isRegistered) {
      "app-rating-modal"
    } else {
      "new-user-register-modal"
    }
  }

  @RequestMapping(value = Array("/login"), method = Array(RequestMethod.GET))
  def getLoginPage(model: Model, req: HttpServletRequest, res: HttpServletResponse): String = {
    rememberToRedirect(req)
    "signin-modal"
  }

  @RequestMapping(value = Array("/downUrl"), method = Array(RequestMethod.GET))
  def getDwnSuccessPage(model: Model, req: HttpServletRequest): String = {

    val status = Option(req.getParameter("statusCode")).getOrElse("")
    val appId = Option(req.getParameter("appId")).getOrElse("")
    val sessionId = UserDetails(req).sessionId
    val contentId = Option(req.getParameter("contentId")).getOrElse("")
    val dwnReqId = Option(req.getParameter("dwnReqId"))
    val stateDesc = Option(req.getParameter("stateDesc")).getOrElse("")

    logger.debug(
      "download session id :[{}] " +
        "| appId : [{}] " +
        "| contentId : [{}] " +
        "| downReqId : [{}] " +
        "| stateDesc : [{}] ",
      sessionId, appId, contentId, dwnReqId, stateDesc)

    if (status.equals("S1000")) {
      model.addAttribute("status", status)
      model.addAttribute("message", stateDesc)
      "app-download-success-modal"
    } else if (status.equals("S1005")) {

      val downloadResp = Await.result(
        discoveryService.download(sessionId.getOrElse(""), appId, contentId),
        discoveryApiTimeOut
      )
      val paymentInstruments = downloadResp.paymentInstrumentList.get.map(entry => entry.get("name").getOrElse(""))

      model.addAttribute("downReqId", dwnReqId.get)
      model.addAttribute("pInstruments", paymentInstruments.asJava)

      "select-payment-modal"
    } else {
      model.addAttribute("status", "E1000")
      model.addAttribute("message", stateDesc)

      "app-download-success-modal"
    }
  }

  @RequestMapping(value = Array("/subSuccess"), method = Array(RequestMethod.GET))
  def getSubSuccessPage(model: Model, req: HttpServletRequest): String = {

    val status = Option(req.getParameter("sCode")).getOrElse("")
    val stateDesc = Option(req.getParameter("msg")).getOrElse("")

    model.addAttribute("message", stateDesc)
    model.addAttribute("status", status)

    "app-subscribe-success-modal"
  }

  @RequestMapping(value = Array("/unsubscribe"), method = Array(RequestMethod.GET))
  def getUnsubPage(model: Model, req: HttpServletRequest): String = {

    val appId = req.getParameter("appId")
    val sessionId = UserDetails(req).sessionId.get
    val appDetails = Await.result(
      discoveryService.appDetails(sessionId, appId),
      discoveryApiTimeOut)

    if (appDetails.isSuccess) {
      model.addAttribute("appId", appId)
      model.addAttribute("appName", appDetails.getResult.displayName)
      "app-unsubscribe-modal"
    } else {
      model.addAttribute("message", appDetails.description)
      "500-modal"
    }
  }

  @RequestMapping(value = Array("/unSubSuccess"), method = Array(RequestMethod.GET))
  def getUnsubSuccess(model: Model, req: HttpServletRequest): String = {

    val status = Option(req.getParameter("sCode")).getOrElse("")
    val message = Option(req.getParameter("msg")).getOrElse("")

    model.addAttribute("status", status)
    model.addAttribute("message", message)

    "app-unsubscribe-success-modal"
  }

  @RequestMapping(value = Array("/userRole"), method = Array(RequestMethod.GET))
  def getUserRole(model: Model, req: HttpServletRequest): String = {
    "appstore-user-role-validation"
  }
}
