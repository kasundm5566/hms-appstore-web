/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.security;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * validate the host ip against the given host ip list
 * in application.conf
 */
public class IpValidationService {

    private static final Logger logger = LoggerFactory.getLogger(IpValidationService.class);
    private static final IpValidationService validationService = new IpValidationService();
    private static final List<Pattern> regexList = new ArrayList<>();

    static {
        Config webConfig = ConfigFactory.load().getConfig("appstore.web");
        List<String> allowedHosts = webConfig.getStringList("allowed.hosts");
        for (String host : allowedHosts) {
            regexList.add(Pattern.compile(host));
        }
    }

    private IpValidationService() {
    }

    public static IpValidationService getInstance() {
        return validationService;
    }

    public boolean isValidHost(String hostIp) {
        String filteredIp = hostIp;
        if (hostIp.contains(",")) {
            String[] splittedIps = hostIp.split(",");
            filteredIp = splittedIps[splittedIps.length - 1].trim();
        }
        for (Pattern pattern : regexList) {
            logger.debug("validating host ip [{}]",filteredIp);
            if (pattern.matcher(filteredIp).matches()) {
                logger.debug("host ip is valid");
                return true;
            }
        }
        return false;
    }
}
