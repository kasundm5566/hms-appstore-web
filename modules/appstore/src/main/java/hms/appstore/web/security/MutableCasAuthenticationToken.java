/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.security;

import org.jasig.cas.client.validation.Assertion;
import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class MutableCasAuthenticationToken extends CasAuthenticationToken {

    private int keyHash;

    /**
     * Constructor.
     *
     * @param key         to identify if this object made by a given {@link
     *                    org.springframework.security.cas.authentication.CasAuthenticationProvider}
     * @param principal   typically the UserDetails object (cannot  be <code>null</code>)
     * @param credentials the service/proxy ticket ID from CAS (cannot be
     *                    <code>null</code>)
     * @param authorities the authorities granted to the user (from the {@link
     *                    org.springframework.security.core.userdetails.UserDetailsService}) (cannot be <code>null</code>)
     * @param userDetails the user details (from the {@link
     *                    org.springframework.security.core.userdetails.UserDetailsService}) (cannot be <code>null</code>)
     * @param assertion   the assertion returned from the CAS servers.  It contains the principal and how to obtain a
     *                    proxy ticket for the user.
     * @throws IllegalArgumentException
     *          if a <code>null</code> was passed
     */
    private MutableCasAuthenticationToken(String key, Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, UserDetails userDetails, Assertion assertion) {
        super(key, principal, credentials, authorities, userDetails, assertion);
    }

    public MutableCasAuthenticationToken(int keyHash, Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, UserDetails userDetails, Assertion assertion) {
        this("dummy-key", principal, credentials, authorities, userDetails, assertion);
        this.keyHash = keyHash;
    }

    @Override
    public int getKeyHash() {
        return keyHash;
    }
}
