/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.security;

import org.jasig.cas.client.proxy.ProxyGrantingTicketStorage;
import org.jasig.cas.client.util.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.cas.ServiceProperties;
import org.springframework.security.cas.authentication.CasAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import hms.appstore.web.security.AnonymousAuthenticationToken;

public class CasAuthenticationFilter extends AbstractAuthenticationProcessingFilter {


    List<GrantedAuthority> defaultRoles;

    private static final Logger logger = LoggerFactory.getLogger(CasAuthenticationFilter.class);

    //~ Static fields/initializers =====================================================================================

    /**
     * Used to identify a CAS request for a stateful user agent, such as a web browser.
     */
    public static final String CAS_STATEFUL_IDENTIFIER = "_cas_stateful_";

    /**
     * Used to identify a CAS request for a stateless user agent, such as a remoting protocol client (e.g.
     * Hessian, Burlap, SOAP etc). Results in a more aggressive caching strategy being used, as the absence of a
     * <code>HttpSession</code> will result in a new authentication attempt on every request.
     */
    public static final String CAS_STATELESS_IDENTIFIER = "_cas_stateless_";

    /**
     * The last portion of the receptor url, i.e. /proxy/receptor
     */
    private String proxyReceptorUrl;

    /**
     * The backing storage to store ProxyGrantingTicket requests.
     */
    private ProxyGrantingTicketStorage proxyGrantingTicketStorage;

    private String artifactParameter = ServiceProperties.DEFAULT_CAS_ARTIFACT_PARAMETER;

    //Fallback authentication provider (autologin)
    private IAutoLoginFallbackAuthProcessor autoLoginFallbackAuthProcessor;
    private boolean useAutologin = true;
    //~ Constructors ===================================================================================================

    protected CasAuthenticationFilter() {
        super("/j_spring_cas_security_check");
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        final String username = CAS_STATEFUL_IDENTIFIER;
        String password = request.getParameter(this.artifactParameter);
        logger.debug("==================>>>>>>>>> "+this.getClass().getName()+" Attempt authenticating");
        if (password == null) {
            password = "";
        }
        if (password.equals("null")) {
            if(useAutologin){
                logger.debug("DELEGATING authentication to  autoLoginFallbackAuthProcessor");
                return autoLoginFallbackAuthProcessor.authenticate(request, response);
            }else{
                logger.debug("Creating Anonymous Authentication Token with roles [{}]",defaultRoles);
                return new AnonymousAuthenticationToken("dummy", "anonymousUser", defaultRoles);
            }
        } else {
            final UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
            authRequest.setDetails(authenticationDetailsSource.buildDetails(request));
            final Authentication authenticate = this.getAuthenticationManager().authenticate(authRequest);
            final CasAuthenticationToken casAuthToken = (CasAuthenticationToken) authenticate;
            final MutableCasAuthenticationToken modifiedCasAuthToken = addDefaultRolesToCasAuthToken(casAuthToken);
            return modifiedCasAuthToken;
        }
    }

    private MutableCasAuthenticationToken addDefaultRolesToCasAuthToken(CasAuthenticationToken casAuthToken) {
        final Collection<GrantedAuthority> authorities = casAuthToken.getAuthorities();
        final ArrayList<GrantedAuthority> fullAuthorities = new ArrayList<>(authorities);
        fullAuthorities.addAll(defaultRoles);
        return new MutableCasAuthenticationToken(casAuthToken.getKeyHash(), casAuthToken.getPrincipal(), casAuthToken.getCredentials(), fullAuthorities, casAuthToken.getUserDetails(), casAuthToken.getAssertion());
    }

    /**
     * Overridden to provide proxying capabilities.
     */
    protected boolean requiresAuthentication(final HttpServletRequest request, final HttpServletResponse response) {
        final String requestUri = request.getRequestURI();

        if (CommonUtils.isEmpty(this.proxyReceptorUrl) || !requestUri.endsWith(this.proxyReceptorUrl) || this.proxyGrantingTicketStorage == null) {
            return super.requiresAuthentication(request, response);
        }

        try {
            CommonUtils.readAndRespondToProxyReceptorRequest(request, response, this.proxyGrantingTicketStorage);
            return false;
        } catch (final IOException e) {
            return super.requiresAuthentication(request, response);
        }
    }

    public final void setProxyReceptorUrl(final String proxyReceptorUrl) {
        this.proxyReceptorUrl = proxyReceptorUrl;
    }

    public final void setProxyGrantingTicketStorage(
            final ProxyGrantingTicketStorage proxyGrantingTicketStorage) {
        this.proxyGrantingTicketStorage = proxyGrantingTicketStorage;
    }

    public final void setServiceProperties(final ServiceProperties serviceProperties) {
        this.artifactParameter = serviceProperties.getArtifactParameter();
    }

    public void setAutoLoginFallbackAuthProcessor(IAutoLoginFallbackAuthProcessor autoLoginFallbackAuthProcessor) {
        this.autoLoginFallbackAuthProcessor = autoLoginFallbackAuthProcessor;
    }

    public void setDefaultRoles(List<GrantedAuthority> defaultRoles) {
        this.defaultRoles = defaultRoles;
    }

    public void setUseAutologin(boolean useAutologin) {
        this.useAutologin = useAutologin;
    }
}
