/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

package hms.appstore.web.security;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class AutoLoginFallbackAuthProcessor implements IAutoLoginFallbackAuthProcessor {

    private static final Logger logger = LoggerFactory.getLogger(AutoLoginFallbackAuthProcessor.class);

    private List<GrantedAuthority> autologinRoles;
    private List<GrantedAuthority> defaultRoles;

    public AutoLoginFallbackAuthProcessor() {}

    public void setAutologinRoles(List<GrantedAuthority> autologinRoles) {
        this.autologinRoles = autologinRoles;
    }

    public void setDefaultRoles(List<GrantedAuthority> defaultRoles) {
        this.defaultRoles = defaultRoles;
    }

    @Override
    public Authentication authenticate(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

        final String msisdn = request.getHeader("msisdn");
        final String hostIp = request.getHeader("X-Forwarded-For");

        IpValidationService validationService = IpValidationService.getInstance();
        boolean isValidHost = validationService.isValidHost(hostIp);

        logger.debug("authenticating  msisdn [{}] host-ip [{}]", msisdn, hostIp);

        if (isValidHost && msisdn != null) {
            logger.debug("authenticating  msisdn [{}] host-ip [{}] granted roles [{}]", msisdn, hostIp, autologinRoles.toArray());
            return new AutologinAuthenticationToken("autologin", msisdn, autologinRoles);
        } else {
            logger.debug("authenticating  msisdn [{}] host-ip [{}] granted roles [{}]", msisdn, hostIp, defaultRoles.toArray());
            return new AnonymousAuthenticationToken("dummy", "anonymousUser", defaultRoles);
        }
    }
}
