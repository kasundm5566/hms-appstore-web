<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--Navigation start--%>
<div class="navbar navbar-default vdf-main-header navbar-static-top hidden-xs" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><fmt:message key="appstore.main.navigation.toggle"/></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

                <li class=<c:if test="${active_page!='user'}">'active'</c:if>>
                    <a href="#">
                        <fmt:message key="appstore.main.navigation.all.apps"/>
                    </a>
                </li>
                <%--<li class=<c:if test="${active_page=='user'}">'active'</c:if>>--%>
                    <%--<c:if test="${(authenticated)&&(!userRole)}">--%>
                        <%--<a href="user">--%>
                            <%--<fmt:message key="appstore.myapp"/>--%>
                        <%--</a>--%>
                    <%--</c:if>--%>
                <%--</li>--%>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${authenticated}">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary-vdf"><c:out value="${userId}"/></button>

                        </div>
                    </li>
                </c:if>
            </ul>

        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div class="container hidden-xs">
    <div class="row row-shortned">
        <div class="navbar-collapse collapse">
            <ul class="nav nav-justified branding-background">


            </ul>
        </div>
    </div>
</div>
<%--Navigation end--%>

<%--slide navigation--%>
<%--<div class="hidden-lg hidden-md hidden-sm visible-xs">--%>

    <%--<nav class="navbar navbar-default navbar-default-min" role="navigation">--%>
        <%--<div class="container">--%>
            <%--<div class="navbar-header">--%>
                <%--<button type="button" class="sb-toggle-left navbar-toggle" data-toggle="collapse"--%>
                        <%--data-target="#xs-navbar">--%>
                    <%--<span class="sr-only">Toggle navigation</span>--%>
                    <%--<span class="icon-bar"></span>--%>
                    <%--<span class="icon-bar"></span>--%>
                <%--</button>--%>
                <%--<div style="float: left;padding-top: 2%;text-align: right;width: 45%;">--%>
                <%--<a class="navbar-brand" style="margin-left: 28%;margin-top: 2%; float: none" href="/appstore"><fmt:message--%>
                        <%--key="appstore.main.navigation.all.apps"/></a></div>--%>
                <%--<c:if test="${!authenticated}">--%>
                    <%--<a style="top:0.3em;right: 1em; position: relative" class="pull-right"--%>
                       <%--href="<fmt:message key='appstore.cas.login'/>?service=<fmt:message key='cas.security.check.url'/>">--%>
                        <%--<button class="btn btn-primary-vdf"><fmt:message--%>
                                <%--key='appstore.main.navigation.sign.in'/></button>--%>
                    <%--</a>--%>
                <%--</c:if>--%>
                <%--<c:if test="${authenticated}">--%>
                    <%--<a style="top:0.3em;right: 1em; position: relative" class="pull-right"--%>
                       <%--href="<fmt:message key='appstore.cas.logout'/>">--%>
                        <%--<button class="btn btn-primary-vdf">--%>
                            <%--<fmt:message key='appstore.logout'/></button>--%>
                    <%--</a>--%>
                <%--</c:if>--%>
            <%--</div>--%>
            <%--<div class="hidden collapse navbar-collapse" id="">--%>
                <%--<ul class="nav navbar-nav">--%>
                    <%--<li class=<c:if test="${active_page=='user'}">'active'</c:if>>--%>
                        <%--<c:if test="${(authenticated)&&(!userRole)}">--%>
                            <%--<a href="user">--%>
                                <%--<fmt:message key="appstore.myapp"/>--%>
                            <%--</a>--%>
                        <%--</c:if>--%>
                    <%--</li>--%>
                    <%--&lt;%&ndash;auth links&ndash;%&gt;--%>
                    <%--<c:if test="${userRole}">--%>
                        <%--<li>--%>
                            <%--<a href='<fmt:message key="appstore.admin.home.url"/>'>--%>
                                <%--<fmt:message key="appstore.admin"/>--%>
                            <%--</a>--%>
                        <%--</li>--%>
                    <%--</c:if>--%>
                    <%--<c:if test="${showRegLink}">--%>
                        <%--<li>--%>
                            <%--<a href='<fmt:message key="registration.home"/>'>--%>
                                <%--<fmt:message key="appstore.registration"/>--%>
                            <%--</a>--%>
                        <%--</li>--%>
                    <%--</c:if>--%>
                    <%--&lt;%&ndash;end auth links&ndash;%&gt;--%>

                    <%--<li><a href="freeApps"><fmt:message key="appstore.main.navigation.free.apps"/></a></li>--%>
                    <%--<li><a href="top-rated"><fmt:message key="appstore.main.navigation.top.rated.apps"/></a></li>--%>
                    <%--<li><a href="newly-added"><fmt:message key="appstore.main.navigation.newly.added.apps"/></a></li>--%>
                    <%--<li><a href="mostly-used"><fmt:message key="appstore.main.navigation.mostly.used.apps"/></a></li>--%>


                    <%--<li class="dropdown">--%>
                        <%--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--%>
                            <%--<fmt:message key="appstore.main.navigation.app.categories"/>--%>
                            <%--<b class="caret"></b>--%>
                            <%--<ul class="dropdown-menu">--%>
                                <%--<c:forEach items="${appCategories}" var="category">--%>
                                    <%--<c:if test="${!fn:contains(category,'Dialog' )}">--%>
                                        <%--<li><a href='apps?category=<c:out value="${category}"/>'><c:out--%>
                                                <%--value="${category}"/> </a></li>--%>
                                    <%--</c:if>--%>
                                <%--</c:forEach>--%>
                            <%--</ul>--%>
                        <%--</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</div>--%>
            <%--<div class="sb-slidebar sb-left">--%>
                <%--<nav>--%>
                    <%--<ul class="sb-menu">--%>
                        <%--<c:if test="${authenticated}">--%>
                            <%--<li>--%>
                                <%--<button type="button" style="width: 100%; border-radius: 0px;" class="btn btn-default"><c:out value="${userId}"/></button>--%>
                            <%--</li>--%>
                        <%--</c:if>--%>
                        <%--<li class=<c:if test="${active_page=='user'}">'active'</c:if>>--%>
                            <%--<c:if test="${(authenticated)&&(!userRole)}">--%>
                                <%--<a href="user">--%>
                                    <%--<fmt:message key="appstore.myapp"/>--%>
                                <%--</a>--%>
                            <%--</c:if>--%>
                        <%--</li>--%>
                        <%--<c:if test="${userRole}">--%>
                            <%--<li>--%>
                                <%--<a href='<fmt:message key="appstore.admin.home.url"/>'>--%>
                                    <%--<fmt:message key="appstore.admin"/>--%>
                                <%--</a>--%>
                            <%--</li>--%>
                        <%--</c:if>--%>
                        <%--<c:if test="${showRegLink}">--%>
                            <%--<li>--%>
                                <%--<a href='<fmt:message key="registration.home"/>'>--%>
                                    <%--<fmt:message key="appstore.registration"/>--%>
                                <%--</a>--%>
                            <%--</li>--%>
                        <%--</c:if>--%>
                        <%--<li><a href="freeApps"><fmt:message key="appstore.main.navigation.free.apps"/></a></li>--%>
                        <%--<li><a href="top-rated"><fmt:message key="appstore.main.navigation.top.rated.apps"/></a></li>--%>
                        <%--<li><a href="newly-added"><fmt:message key="appstore.main.navigation.newly.added.apps"/></a>--%>
                        <%--</li>--%>
                        <%--<li><a href="mostly-used"><fmt:message key="appstore.main.navigation.mostly.used.apps"/></a>--%>
                        <%--</li>--%>
                        <%--<li>--%>
                            <%--<a href="#" class="sb-toggle-submenu"><fmt:message--%>
                                    <%--key="appstore.main.navigation.app.categories"/><span class="sb-caret"></span></a>--%>
                            <%--<ul class="sb-submenu" style="display: none;">--%>
                                <%--<c:forEach items="${appCategories}" var="category">--%>
                                    <%--<c:if test="${!fn:contains(category,'Dialog' )}">--%>
                                        <%--<li><a href='apps?category=<c:out value="${category}"/>'><c:out--%>
                                                <%--value="${category}"/> </a></li>--%>
                                    <%--</c:if>--%>
                                <%--</c:forEach>--%>
                            <%--</ul>--%>
                        <%--</li>--%>

                    <%--</ul>--%>
                <%--</nav>--%>
            <%--</div>--%>
        <%--</div>--%>
    <%--</nav>--%>

    <%--&lt;%&ndash;search for small devices&ndash;%&gt;--%>
    <%--<div class="container row">--%>
        <%--<div class="col-xs-12">--%>
            <%--<form id="custom-search-form" class="form-search form-horizontal" action="search" method="get">--%>
                <%--<div class="input-group">--%>
                    <%--<input type="text"--%>
                           <%--placeholder="<fmt:message key="appstore.main.navigation.Search.button.place.holder"/>"--%>
                           <%--class="form-control vdf-form-control" name="searchKey" value='<c:out value="${searchKey}"/>'--%>
                           <%--required>--%>
                    <%--<span class="input-group-btn">--%>
                        <%--<button type="submit" class="btn btn-default"><i class="icon-search"></i></button>--%>
                    <%--</span>--%>
                <%--</div>--%>
            <%--</form>--%>
        <%--</div>--%>
    <%--</div>--%>
    <%--<br/>--%>
<%--</div>--%>
<%--end slide navigation--%>