<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">

<!DOCTYPE html>
<html lang="en">
<head>

    <title>
        <fmt:message key="iap.title"/>
    </title>

    <script src="./resources/js/jquery.min.js"></script>
    <script src="./resources/js/custom.js"></script>

    <script type="text/javascript">
        window.history.forward();
        function noBack() {
            window.history.forward();
        }
    </script>
</head>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();" onunload="" data-twttr-rendered="true"
      itemscope="" itemtype="http://schema.org/Product">

<div class="panel panel-default col-sm-8 col-sm-offset-2" style="margin-top: 5%;">

    <div class="panel-body">
        <h4 class="branding-font-style">
            <fmt:message key="iap.purchase.confirmation"/>
        </h4>


        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <fmt:message key="iap.purchase.summary"/>
                </div>
                <div class="panel-body" style="padding-left:0px;padding-right:0px;">
                    <table class="table table-borderless table-responsive">
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.application.name"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info.applicationName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.item.name"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info.itemName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.item.description"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info.itemDescription}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.amount"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info.currency}"/> <c:out
                                    value="${purchase_info.amount}"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <form role="form" method="post" action="purchase" id="purchaseForm">

                <div class="form-group">
                    <label for="selectPaymentInstrument">
                        <fmt:message key="iap.select.payment.method"/>
                    </label>
                    <select id="selectPaymentInstrument" class="form-control valid" required=""
                            name="pInstrument">
                        <c:forEach var="pIns" items="${pInstruments}">
                            <option value="${pIns}"><c:out value="${pIns}"/></option>
                        </c:forEach>

                    </select>
                </div>
                <div class="form-group">
                    <div id="pinblock" class="form-group hidden">
                        <label for="pin">
                            <fmt:message key="iap.pin"/>
                        </label>
                        <input id="pin" class="form-control" type="password" maxlength="4"
                               name="pin"
                               required="required" placeholder='<fmt:message key="iap.pin.placeholder"/>'/>
                    </div>
                    <script type="text/javascript">
                        $("#selectPaymentInstrument").on('change', function (e) {
                            console.log("value changes :")
                            var value = $(this).val();
                            console.info(value)
                            if (value.indexOf(config['mpaisa']) > -1) {
                                $("#pinblock").removeClass("hidden");
                                $("#pinblock:input").attr("required", "required");
                            } else {
                                $("#pinblock").addClass("hidden");
                                $("#pinblock:input").removeAttr("required");
                            }
                        });

                    </script>
                </div>


                <div class="pull-right" style="margin: 2%">
                    <a href="javascript:history.back()">
                        <button class="btn btn-default">Back</button>
                    </a>

                    <input id="iap-confirm-button" onclick="disable()"
                           class="btn branding-background" type="button"
                           value="Confirm">
                    </input>
                    <script type="text/javascript">
                        function disable() // no ';' here
                        {
                            function validateMpaissaPin(){
                                var retVal=true;
                                var pinElement = $('#pin');
                                var pinHidden = pinElement.parent().hasClass('hidden');
                                console.log('pin is hidden : '+pinHidden)
                                if(!pinHidden){
                                    retVal = ((pinElement.val().length > 0));
                                }
                                if(!retVal){
                                    pinElement.css('border','#E60000 solid 1px');
                                }else{
                                    pinElement.removeAttr('style');
                                }
                                console.info("validation result : "+retVal);
                                return retVal;
                            }

                            if(validateMpaissaPin()){
                                var elem = document.getElementById("iap-confirm-button");
                                elem.setAttribute("disabled", "true");
                                elem.value = "Purchase in progress...";
                                document.getElementById('purchaseForm').submit();
                            }
                        }
                    </script>

                </div>
            </form>
        </div>

    </div>
        <%--panel-body--%>
</div>

</body>
</html>
</fmt:bundle>