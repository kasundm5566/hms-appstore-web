<div>
    <footer align="center">
        <div class="footer-content">
            <div class="col-md-3 col-sm-3 col-xs-12">

            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 footer-text">
                <%--<a href='<fmt:message key=""/>'><fmt:message key="appstore.footer.menu.help"/></a> |--%>
                <a href='<fmt:message key="vodafone.support"/>'><fmt:message key="appstore.footer.menu.support"/></a> |
                <a href='<fmt:message key="vodafone.termscondt"/>'><fmt:message
                        key="appstore.footer.menu.terms.and.conditions"/></a> |
                <%--<a href='<fmt:message key=""/>'><fmt:message key="appstore.footer.menu.privacy.policy"/></a> |--%>
                <a href='<fmt:message key="vodafone.contactus"/>'><fmt:message
                        key="appstore.footer.menu.contact.us"/></a>
                <%--<a href='<fmt:message key=""/>'><fmt:message key="appstore.footer.menu.about.us"/></a> |--%>
                <%--<a href='<fmt:message key="appstore.vodafone.blog.url"/>'><fmt:message key="appstore.footer.menu.blog"/></a>--%>
                <br/>
                <fmt:message key="appstore.footer.copy.rights.notice"/>
            </div>

            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="responsive-social-icons">
                    <a href='<fmt:message key="vodafone.facebook"/>'>
                        <div class="footer-social-icon">
                            <i class="icon-facebook"></i>
                        </div>
                    </a>
                    <a href='<fmt:message key="vodafone.twitter"/>'>
                        <div class="footer-social-icon">
                            <i class="icon-twitter"></i>
                        </div>
                    </a>
                    <a href='<fmt:message key="vodafone.youtube"/>'>
                        <div class="footer-social-icon">
                            <i class="icon-youtube"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>