<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">

    <!DOCTYPE html>
    <html lang="en">
    <head>

    <title>
            <fmt:message key="iap.title"/>
        </title>
    </head>
  <body>


  <div class="panel panel-default col-sm-8 col-sm-offset-2" style="margin-top: 5%;">

                        <div class="panel-body">
                            <h4 class="branding-font-style">
                                <fmt:message key="iap.purchase.confirmation"/>
                            </h4>


                            <div class="col-sm-6">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <fmt:message key="iap.purchase.summary"/>
                                    </div>
                                    <div class="panel-body" style="padding-left:0px;padding-right:0px;">
                                        <table class="table table-borderless table-responsive">
                                            <tr>
                                                <td>
                                                <fmt:message key="iap.purchase.info.application.name"/>
                                                </td>
                                                <td>
                                                <c:out value="${purchase_info.applicationName}"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <fmt:message key="iap.purchase.info.item.name"/>
                                                </td>
                                                <td>
                                                <c:out value="${purchase_info.itemName}"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <fmt:message key="iap.purchase.info.item.description"/>
                                                </td>
                                                <td>
                                                <c:out value="${purchase_info.itemDescription}"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                <fmt:message key="iap.purchase.info.amount"/>
                                                </td>
                                                <td>
                                                <c:out value="${purchase_info.currency}"/> <c:out
                                                    value="${purchase_info.amount}"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="col-sm-11">
                                    Press Continue to confirm the purchase of <c:out value="${purchase_info.itemName}"/>
                                    from <c:out value="${purchase_info.applicationName}"/>
                                    for <c:out value="${purchase_info.currency}"/> <c:out
                                        value="${purchase_info.amount}"/>
                                </div>
                                <div class="pull-right" style="margin: 2%">
                                    <a href="javascript:closeWindow()">
                                        <button class="btn btn-default">Cancel</button>
                                    </a>
                                    <a href="confirmation">
                                        <button class="btn branding-background" type="submit">
                                            Continue
                                        </button>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <!--panel-body-->
                    </div>

<script type="text/javascript">
    function closeWindow(){
        window.open('','_self');
        window.close();
    }
</script>

    </body>
    </html>
</fmt:bundle>