<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

    <div class="col-md-4 col-sm-12 col-xs-12">

        <div class="row">
            <div class="row app-panel-title-area-right vdf-app-panel-header">
                <span class="vdf-app-panel-title app-panel-title-right">
                    <c:if test="${panelRightTitle!=''}">
                        ${panelRightTitle}
                    </c:if>
                    <c:if test='${panelRightTitle=="" || panelRightTitle==null}'>
                        <fmt:message code="appstore.mostly.used.panel.title"/>
                    </c:if>
                </span>
                <span class="vdf-app-panel-view-all app-title-view-all">
                    <c:if test="${viewAllRight!='no'}">
                        <c:if test="${viewAllRight!=''}">
                            <a href="${viewAllRight}">
                        </c:if>
                        <c:if test='${viewAllRight=="" || viewAllRight==null}'>
                            <a href="mostly-used">
                        </c:if>
                        <fmt:message code="appstore.app.panel.view.all.text"/></a>
                    </c:if>
                </span>
            </div>
            <c:forEach items="${mostUsedApps}" var="mostApp">
                <a href='view?appId=<c:out value="${mostApp.id}"/>' class="hidden-xs hidden-sm">
                    <div class="appstore-app-right container">
                        <div class="row">
                            <div class="col-md-6">
                                <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${mostApp.appIcon}"/>'
                                     class="appstore-app-icon-right-card img-thumbnail"/>
                            </div>
                            <div class="col-md-6 vdf-fix-layout" style="width: 50%;">
                                <div class="row appstore-ellipsis">
                                <span><c:out value="${mostApp.name}"/></span>
                                </div>
                                    <%--application rating begin--%>
                                <div class="row">
                                <c:if test="${mostApp.rating ge 0}">
                                    <%--limit rating--%>
                                    <c:if test="${mostApp.rating gt 5}">
                                        <c:set var="end" value="5"/>
                                    </c:if>
                                    <c:if test="${mostApp.rating le 5}">
                                        <c:set var="end" value="${mostApp.rating}"/>
                                    </c:if>

                                    <c:forEach begin="1" end="${end}">
                                        <i class="icon-star rating-color"></i>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${end ge 1}">
                                    <c:forEach begin="1" end="${5 - end}">
                                        <i class="icon-star-empty rating-color"></i>
                                    </c:forEach>
                                </c:if>
                                <c:if test="${end eq 0}">
                                    <c:forEach begin="1" end="5">
                                        <i class="icon-star-empty rating-color"></i>
                                    </c:forEach>
                                </c:if>
                                    <%--application rating end--%>
                                </div>
                                <div class="row">
                                <c:out value="${mostApp.developer}"/>
                                </div>
                            </div>
                            <div class="pull-right">
                                <c:if test="${mostApp.canDownload||mostApp.canSubscribe||mostApp.canUnSubscribe}">
                                    <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${mostApp.name}"/>'
                                            app-id='<c:out value="${mostApp.id}"/>'
                                            app-charging='<c:out value="${mostApp.chargingLabel}"/>'
                                            app-type='<c:if test="${mostApp.canSubscribe}">subscribe</c:if>
                                             <c:if test="${mostApp.downloadable}">download</c:if>
                                             <c:if test="${mostApp.canUnSubscribe}">unSub</c:if>'>

                        <span class="appstore-app-right-button vdf-button">
                            <c:if test="${mostApp.canDownload && !mostApp.canSubscribe}">
                                <i class="icon-download-alt appstore-app-right-button-icon"></i> <%--<fmt:message
                                    code="appstore.download.button.title"/>--%>
                            </c:if>
                            <c:if test="${mostApp.canSubscribe}">
                                <i class="icon-rss appstore-app-right-button-icon"></i> <%--<fmt:message
                                    code="appstore.subscribe.button.title"/>--%>
                            </c:if>
                            <c:if test="${mostApp.canUnSubscribe}">
                                <i class="icon-ban-circle add-r-margin"></i><%--<fmt:message
                                    code="appstore.unsub.button.title"/>--%>
                            </c:if>
                        </span>
                                    </appAction>
                                </c:if>
                            </div>
                            <div class="row left-panel-shortdesc">
                                <c:out value="${fn:substring(mostApp.shortDesc,0 ,45)}"/>
                            </div>
                        </div>
                        <%--<div class="row appstore-ellipsis">--%>
                            <%--<span><c:out value="${fn:substring(mostApp.shortDesc,0,75)}"/></span>--%>
                        <%--</div>--%>
                    </div>
                </a>
                
                <%--responsive design begin--%>
                <a href='view?appId=<c:out value="${mostApp.id}"/>' class="hidden-lg hidden-md ">
                    <div class="col-md-3 col-sm-4 col-xs-6 app-tile">
                        <div class="appstore-app container">
                            <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${mostApp.appIcon}"/>'
                                 class="appstore-app-icon img-thumbnail"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${mostApp.name}"/></span>
                                <br/>
                                        <%--application rating begin--%>
                                        <c:if test="${mostApp.rating ge 0}">
                                            <%--limit rating--%>
                                            <c:if test="${mostApp.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${mostApp.rating le 5}">
                                                <c:set var="end" value="${mostApp.rating}"/>
                                            </c:if>
                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (emd lt 5)}">
                                            <c:forEach begin="1" end="${5-end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <%--application rating end--%>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><c:out value="${mostApp.developer}"/></span>
                                    <c:if test="${mostApp.canSubscribe||mostApp.canDownload||mostApp.canUnSubscribe}">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${mostApp.name}"/>'
                                                app-id='<c:out value="${mostApp.id}"/>'
                                                app-charging='<c:out value="${mostApp.chargingLabel}"/>'

                                                app-type='
                                    <c:if test="${mostApp.canDownload}">
                                        download
                                    </c:if>
                                    <c:if test="${mostApp.canSubscribe}">
                                        subscribe
                                    </c:if>'>
                                    <span class="appstore-app-action-icon vdf-button ">
                                       <c:if test="${mostApp.canDownload && !mostApp.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                       </c:if>
                                        <c:if test="${mostApp.canSubscribe && !mostApp.canDownload}">
                                            <i class="icon-rss"></i>
                                        </c:if>
                                    </span>
                                        </appAction>
                                    </c:if>
                                </span>
                            </span>
                        </div>
                    </div>
                </a>
                <%--responsive app tile hide--%>
                
            </c:forEach>


        </div>
    </div>