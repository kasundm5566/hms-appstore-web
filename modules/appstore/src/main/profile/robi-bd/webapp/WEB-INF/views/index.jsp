<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <title><fmt:message code="appstore.home.page.sub.title"/> <fmt:message code="appstore.main.title"/></title>
</head>
<body>
    <%--Page content begins--%>
<div class="row">
    <%--SLIDE SHOW GOES HERE--%>
    <%@ include file="../decorators/appstore-slide-show.jsp" %>
</div>

<div class="row">

    <div class="row app-panel-title-area vdf-app-panel-header">
        <span class="vdf-app-panel-title app-panel-title">${panelTopTitle}</span>
        <span class="vdf-app-panel-view-all app-title-view-all">
            <a href="${viewAllTop}"><fmt:message code="appstore.app.panel.view.all.text"/></a>
        </span>
    </div>

    <div class="appstore-apps add-lr-margin">

        <c:forEach items="${panelTopApps}" var="topApp">
            <a href='view?appId=<c:out value="${topApp.id}"/>'>
                <div class="col-md-3 col-sm-4 col-xs-6 app-tile">
                    <div class="appstore-app container">
                        <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${topApp.appIcon}"/>'
                             class="appstore-app-icon img-thumbnail"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${topApp.name}"/></span>
                                <br/>
                                        <%--application rating begin--%>
                                        <c:if test="${topApp.rating ge 0}">
                                            <%--limit rating--%>
                                            <c:if test="${topApp.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${topApp.rating le 5}">
                                                <c:set var="end" value="${topApp.rating}"/>
                                            </c:if>

                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (end lt 5)}">
                                            <c:forEach begin="1" end="${5-end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <%--application rating end--%>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><c:out value="${topApp.developer}"/></span>
                                    <c:if test="${topApp.canSubscribe||topApp.canDownload||topApp.canUnSubscribe}">
                                        <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${topApp.name}"/>'
                                            app-id='<c:out value="${topApp.id}"/>'
                                            app-charging='<c:out value="${topApp.chargingLabel}"/>'

                                            app-type='
                                    <c:if test="${topApp.canDownload}">
                                        download
                                    </c:if>
                                    <c:if test="${topApp.canSubscribe}">
                                        subscribe
                                    </c:if>'>
                                    <span class="appstore-app-action-icon vdf-button ">
                                       <c:if test="${topApp.canDownload && !topApp.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                       </c:if>
                                        <c:if test="${topApp.canSubscribe}">
                                           <i class="icon-rss"></i>
                                       </c:if>
                                        <c:if test="${topApp.canUnSubscribe}">
                                           <i class="icon-ban-circle"></i>
                                       </c:if>

                                    </span>
                                    </appAction>
                                    </c:if>
                                </span>
                            </span>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>
</div>

<div class="row">

    <div class="row app-panel-title-area vdf-app-panel-header ">
        <span class="vdf-app-panel-title app-panel-title">${panelBottomTitle}</span>
        <span class="vdf-app-panel-view-all app-title-view-all"><a href="${viewAllBottom}"><fmt:message code="appstore.app.panel.view.all.text"/></a></span>
    </div>

    <div class="appstore-apps add-lr-margin">

        <c:forEach items="${panelBottomApps}" var="newApp">
            <a href='view?appId=<c:out value="${newApp.id}"/>'>
                <div class="col-md-3 col-sm-4 col-xs-6 app-tile">
                    <div class="appstore-app container">
                        <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${newApp.appIcon}"/>'
                             class="appstore-app-icon img-thumbnail"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${newApp.name}"/></span>
                                <br/>
                                        <%--application rating begin--%>
                                        <c:if test="${newApp.rating ge 0}">
                                            <%--limit rating--%>
                                            <c:if test="${newApp.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${newApp.rating le 5}">
                                                <c:set var="end" value="${newApp.rating}"/>
                                            </c:if>
                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (end lt 5)}">
                                            <c:forEach begin="1" end="${5 - end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <%--application rating end--%>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><c:out value="${newApp.developer}"/></span>
                                   <c:if test="${newApp.canUnSubscribe || newApp.canSubscribe||newApp.canDownload}">
                                    <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${newApp.name}"/>'
                                            app-id='<c:out value="${newApp.id}"/>'
                                            app-charging='<c:out value="${newApp.chargingLabel}"/>'

                                            app-type='
                                    <c:if test="${newApp.canDownload}">
                                        download
                                    </c:if>
                                    <c:if test="${newApp.canSubscribe}">
                                        subscribe
                                    </c:if>'>
                                    <span class="appstore-app-action-icon vdf-button ">
                                       <c:if test="${newApp.canDownload && !newApp.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                       </c:if>
                                        <c:if test="${newApp.canSubscribe && !newApp.canDownload}">
                                           <i class="icon-rss"></i>
                                       </c:if>
                                    </span>
                                    </appAction>
                                    </c:if>
                                </span>
                            </span>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>
</div>
    <%--Page content ends--%>
</body>
</html>