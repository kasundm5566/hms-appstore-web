<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <sitemesh:write property='head'></sitemesh:write>

        <link rel="shortcut icon" href="//icons.iconarchive.com/icons/kyo-tux/phuzion/256/Misc-Apps-icon.png">

        <title>Error</title>

        <!-- Bootstrap core CSS -->
        <link href="./resources/css/bootstrap.min.css" rel="stylesheet">

            <%--font awesome css--%>
        <link rel="stylesheet" href="./resources/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="./resources/css/custom.css" rel="stylesheet">
        <link href="./resources/css/vodafone.css" rel="stylesheet">
        <link href="./resources/css/offcanvas.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./resources/js/html5shiv.js"></script>
        <script src="./resources/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body itemscope itemtype="http://schema.org/Product">
    <div id="fb-root"></div>

        <%--Page content begins--%>
    <div class="" id="containerElement">
        <div class="row">
            <div class="col-md-12 col-sm-8 col-xs-12" id="mainBodyCol">

                    <%-------------------------------------------------------------------------------------%>
                <getPage id="error"></getPage>

                <h1 class="vdf-aubergine-font" style="text-align: center; margin-top: 190px;"></i> <fmt:message
                        key="appstore.Error.oops"/></h1>
                <hr>
                <h3 style="text-align: center;">
                    <c:choose>
                        <c:when test="${message=='TIMEOUT_EXCEPTION'}">
                            <fmt:message key="exception.timeout"/>
                        </c:when>
                        <c:when test="${message=='COMMUNICATION_EXCEPTION'}">
                            <fmt:message key="exception.communication"/>
                        </c:when>
                        <c:when test="${message=='OTHER_EXCEPTION'}">
                            <fmt:message key="exception.internal"/>
                        </c:when>
                    </c:choose>
                </h3>
                <hr>

                <div class="error-details" style="text-align:center; margin-bottom: 190px;">

                </div>
            </div>


        </div>
    </div>
        <%--Page content ends--%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./resources/js/jquery.js"></script>
    <script src="./resources/js/bootstrap.min.js"></script>
    <script src="./resources/js/offcanvas.js"></script>
    <script src="./resources/js/custom.js"></script>
        <%--<script src="./resources/js/DownloadController.js"></script>--%>
    </body>
    </html>
</fmt:bundle>