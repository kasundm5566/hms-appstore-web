<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <sitemesh:write property='head'></sitemesh:write>

        <link rel="shortcut icon" href="//icons.iconarchive.com/icons/kyo-tux/phuzion/256/Misc-Apps-icon.png">

        <title>
            <fmt:message key="appstore.Error.500"/>
        </title>

        <!-- Bootstrap core CSS -->
        <link href="./resources/css/bootstrap.min.css" rel="stylesheet">

            <%--font awesome css--%>
        <link rel="stylesheet" href="./resources/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="./resources/css/custom.css" rel="stylesheet">
        <link href="./resources/css/dialog.css" rel="stylesheet">
        <link href="./resources/css/offcanvas.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./resources/js/html5shiv.js"></script>
        <script src="./resources/js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body itemscope itemtype="http://schema.org/Product">
    <div id="fb-root"></div>
        <%--Navigation start--%>
    <%@ include file="../decorators/top-navigation.jsp" %>
        <%--Navigation end--%>

        <%--Page content begins--%>
    <div class="" id="containerElement">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12" id="mainBodyCol">

                    <%-------------------------------------------------------------------------------------%>

                <getPage id="500"></getPage>

                <h1 class="vdf-aubergine-font" style="text-align: center; margin-top: 190px;"></i> <fmt:message key="appstore.Error.oops"/></h1>
                <hr>
                <h2  style="text-align: center;"><fmt:message key="appstore.Error.500.title"/></h2>
                <hr>

                <div class="error-details" style="text-align:center; margin-bottom: 190px;">
                    <fmt:message key="appstore.Error.500.message"/>
                </div>
                    <%------------------------------------------------------------------------------------%>

            </div>

                <%--left app panel begin--%>
            <appstoreLeftPanel id="appStoreLeftPanel">
                <%@ include file="../decorators/left-app-panel.jsp" %>
            </appstoreLeftPanel>
                <%--left app panel end--%>

        </div>
    </div>
        <%--Page content ends--%>

        <%--footer begin--%>
    <%@ include file="../decorators/footer.jsp" %>
    <%@ include file="../decorators/modals.jsp" %>
        <%--footer end--%>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./resources/js/jquery.js"></script>
    <script src="./resources/js/bootstrap.min.js"></script>
    <script src="./resources/js/offcanvas.js"></script>
    <script src="./resources/js/custom.js"></script>
        <%--<script src="./resources/js/DownloadController.js"></script>--%>
    </body>
    </html>
</fmt:bundle>