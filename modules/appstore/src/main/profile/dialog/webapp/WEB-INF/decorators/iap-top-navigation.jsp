<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--Navigation start--%>
<div class="navbar navbar-default vdf-main-header navbar-static-top " role="navigation">
    <div class="container">
        <div class="navbar-header">
        </div>
        <div class="navbar-collapse">
            <ul class="nav navbar-nav">

                <li class=<c:if test="${active_page!='user'}">'active'</c:if>>
                        <a href="#"><img src="./resources/images/logo.png" alt="logo"></a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${authenticated}">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary-vdf"><c:out value="${userId}"/></button>

                        </div>
                    </li>
                </c:if>
            </ul>

        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div class="container">
    <div class="row row-shortned">
        <div class="navbar-collapse collapse">
            <ul class="nav nav-justified branding-background">
            </ul>
        </div>
    </div>
</div>
