<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<title><fmt:message code="appstore.footer.menu.terms.and.conditions"/> <fmt:message
        code="appstore.main.title"/></title>

<getPage id="terms"></getPage>
<h4 class="text-center">All Apps Terms and Conditions</h4>

<h5><i class="icon-edit"></i> Customer Care</h5>
<hr>
If you have a general enquiry about Dialog products and services, please contact Dialog Customer
Care on Telephone +94 777 678 678 or email at service@dialog.lk
<hr>

<h5><i class="icon-edit"></i> Use of content</h5>
<hr>
<ul>
    <li>The services that we are providing to you via the Sites consist of the Content and the Functionalities
        available on the Sites or otherwise provided to you as a result of your use of the Sites ("the Services").
    </li>
    <li> You acknowledge and agree that you are only permitted to use the Sites and the Services as expressly set
        out in these Terms and Conditions or on the Sites.
    </li>
    <li>You agree that the Sites and the Services are for your own personal use only on a single computer or
        device.
    </li>
    <li>
        You may not:
        <ul>
            <li>copy, disclose, modify, reformat, display, distribute, license, transmit, sell, perform, publish,
                transfer or otherwise make available any of the Services or any information learned by you whilst
                using the Services or accessing the Sites.
            </li>
            <li>remove, change or obscure in any way anything on the Sites and/or the Services or otherwise use any
                material obtained whilst using the Sites and/or the Services except as set out in these Terms and
                Conditions.
            </li>
            <li>reverse engineer or decompile (whether in whole or in part) any software used in the Sites and/or
                the Services (except to the extent expressly permitted by applicable law).
            </li>
            <li>copy or use any material from the Sites and/or the Services for any commercial purpose.</li>
            <li>remove, obscure or change any copyright, trade mark or other intellectual property right notices
                contained in the original material or from any material copied or printed off from the Sites or
                obtained as a result of the Services.
            </li>
            <li> set an hyperlink to any part of the sites in any form from your site without Dialog's written
                consent.
            </li>
        </ul>
    </li>
    <li>Any use of caching, http accelerators such as Harvest, Squid, Netscape proxy or Microsoft Catapult, or
        similar technology is permitted, however, you have the responsibility of ensuring you are viewing the most
        recent version of the web-page or content.
    </li>
    <li>You may establish a link or "deep link" to the Sites from your site, provided that you have obtained
        Dialog's prior written consent and that at Dialog's sole discretion, the context is relevant and the link or
        its description is not detrimental to Dialog.
    </li>
    <li>Users that are not subscribers to the Dialog Network acknowledge that access to the Sites and/or the
        Services may be restricted at the sole discretion of Dialog in the interest of fully subscribed customers of
        the Dialog Network.
    </li>
    <li>Some areas of the sites or services may not be accessible for you even you are a subscribed customer of
        Dialog's Mobile Telephony Network, depending on the subscribed services and credit limits.
    </li>
</ul>
<hr>


<h5><i class="icon-edit"></i> Your obligations</h5>
<hr>
<ul>
    <li>You warrant that you will only use the Sites and the Services in accordance with these Terms and Conditions
        and in an appropriate and lawful manner and by way of example and not as a limitation that you shall not
        (and shall not authorise or permit any other party to):
    </li>
    <ul>
        <li>receive, access or transmit any Content which is obscene, pornographic, threatening, racist,
            menacing, offensive, defamatory, in breach of confidence, in breach of any intellectual property
            right (including copyright) or otherwise objectionable or unlawful
        </li>
        <li>circumvent user authentication or security of any host, network or account (referred to as
            "cracking" or "hacking") nor interfere with service to any user, host or network (referred to as
            "denial of service attacks") nor copy any pages or register identical keywords with search engines
            to mislead other users into thinking that they are reading Dialog's legitimate web pages (referred
            to as "page-jacking") or use the Sites or the Services for any other unlawful or objectionable
            conduct. Users who violate systems or network security may incur criminal or civil liability and
            Dialog will at its absolute discretion fully co-operate with investigations of suspected criminal
            violations, violation of systems or network security under the leadership of law enforcement or
            relevant authorities
        </li>
        <li>use the Sites and/or the Services to advertise or offer to sell any goods or services for any
            commercial purpose without Dialog's written consent
        </li>
        <li>knowingly or recklessly transmit any electronic Content (including viruses) through the Sites and/or
            the Services which shall cause or is likely to cause detriment or harm, in any degree, to computer
            systems owned by Dialog or other Internet users
        </li>
        <li>hack into, make excessive traffic demands, deliver or forward chain letters, "junk mail" or "spam"
            of any kind, surveys, contests, pyramid schemes or otherwise engage in any other behavior intended
            to inhibit other users from using and enjoying the Sites and/or the Services or any other web site,
            or which is otherwise likely to damage or destroy Dialog's reputation or the reputation of any third
            party.
        </li>
    </ul>

    <li>You acknowledge that chat, discussion group or bulletin board services and similar services offered by
        Dialog ("Public Communication Services") are public communications and your communications may be available
        to others and consequently you should be cautious when disclosing personal or sensitive information or any
        information which may identify you. Dialog shall not be responsible for, and does not control or endorse any
        Content of any Public Communication Services.
    </li>
    <li>If any information provided by you is untrue, inaccurate, not current or incomplete, Dialog has the right to
        terminate your account and refuse any and all current or future use of the Services or access to the Sites.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Access information</h5>
<hr>
<ul>
    <li>Following registration you may be issued with Access Information that may be used to access the Sites and/or the
        Services and any pages for which registration is required. Dialog reserves the right to change your Access
        Information at any time at its sole discretion and shall notify you of this change as soon as reasonably
        practicable
    </li>
    <li>Your account is to be used by a single user only and you will not allow simultaneous access using the same
        Access Information. You acknowledge that you are responsible for ensuring that no unauthorised access to the
        Sites and the Services is obtained using your Access Information and that you are liable for all such activities
        conducted through your account whether authorised or not. When choosing Access Information, you must not use
        words that are obscene, abusive or likely to cause offence.
    </li>
    <li>You as the registered user of the account will:
        <ul>
            <li> keep your Access Information secure and not let it become public knowledge and ensure that your Access
                Information will not be stored anywhere on a computer or your mobile phone in plain text
            </li>
            <li>provide true, accurate, current and complete information in all fields indicated as compulsory when
                registering on the Sites and/or the Services and notify us immediately of any changes or mistakes
            </li>
            <li> if your Access Information becomes known to any other unauthorised user you must inform Dialog
                immediately or take subsequent measures to change your access codes
            </li>
        </ul>
    </li>
    <li>Once you have ‘signed-in' to any secure areas of the web-site, where you are requested to provide your Access
        Information, you must not at any time leave the Internet terminal or device from which you have accessed the
        Sites and the Services or let anyone else use the Internet terminal or device until you have logged out of the
        secure area of the Sites and the Services. You will be responsible for ensuring that you have logged out of the
        secure area at the end of any session.
    </li>
    <li>You must not access the Sites and the Services from any computer connected to a local area network (LAN) or any
        public Internet access device or access point without first making sure that no-one else will be able to observe
        or copy your access or get access to the Sites and the Services pretending to be you.
    </li>
    <li>You agree to check your bills carefully and inform us immediately of any discrepancy.</li>
    <li>Your information Dialog's usage of your personal information is governed by the Dialog's privacy Policy
        Statement.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Proprietary rights</h5>
<hr>
<ul>
    <li>All Trade Marks used on the Sites and/or the Services are the trade marks of Dialog or one of Dialog's
        affiliates or partnered companies. You shall only make fair use of the Trade Marks and will not use the Trade
        Marks, whether design or word marks: (1) as or as part of your own trade marks; (2) in a manner which is likely
        to cause confusion; (3) to identify products to which they do not relate; (4) to imply endorsement or otherwise
        of products or services to which they do not relate; or (5) in any manner which does or may cause damage to the
        reputation of Dialog or the Trade Marks.
    </li>
    <li>You acknowledge and agree that the Services and the Sites or any part thereof, whether presented to you by
        Dialog, advertisers or any third party are protected by copyrights, trademarks, service marks, patents, or other
        proprietary rights and laws. All rights are expressly reserved.
    </li>
    <li>You are only allowed to use the Sites and the Services as set out in these Terms and Conditions and nothing on
        the Sites and/or the Services shall be construed as conferring any licence or other transfer of rights to you of
        any intellectual property or other proprietary rights of Dialog, any member of the Dialog partnerships or any
        third party, whether by estoppel, implication or otherwise.
    </li>
    <li>You hereby grant to Dialog a perpetual, irrevocable, royalty-free, world-wide licence to reproduce, transfer,
        modify, adapt and/or publish any Content provided by you to us by email, post or otherwise on the Sites and/or
        the Services as Dialog sees fit and without notice to you, unless you have indicated otherwise in such
        communication.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Costs</h5>
<hr>
<ul>
    <li>Use of the Sites is currently free. However, Dialog reserves the right to charge for access to part or all of
        the Sites in the future, subject to giving you clear notice when entering any part to which charges apply. Some
        Services may be chargeable as indicated on the Sites and in any accompanying terms and conditions.
    </li>
    <li>You will need to provide all equipment necessary to access the Sites and the Services on the Internet and be
        liable for payment for the local telephone call charges at the rates published by the telephone operator with
        whom you make your local calls or any other Internet access charges to which you may be subject. If your
        equipment does not support relevant technology including but not limited to encryption you may not be able to
        use certain Services or access certain information on the Sites.
    </li>
    <li></li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Liability for content</h5>
<hr>
<ul>
    <li>It is your sole responsibility to satisfy yourself prior to using the Sites and the Services in any way that
        they are suitable for your purposes and up to date. The Services and in particular, prices are periodically
        updated and you should check the Sites and the Services regularly to ensure that you have the latest
        information. You should also refresh your browser each time you visit the Sites and the Services to ensure that
        you download the most up to date version of the Sites and the Services.
    </li>
    <li> The Sites and the Services are provided on an "as is" basis. Although every effort has been made to provide
        accurate information on these pages, neither Dialog, nor any of its employees, nor any member of the Dialog's
        affiliate or partner companies, their suppliers, nor any of their employees, make any warranty, expressed or
        implied, or assume any legal liability (to the extent permitted by law) or responsibility for the suitability,
        reliability, timeliness, accuracy or completeness of the Services or any part thereof contained on the Sites or
        in the Services.
    </li>
    <li>You acknowledge that Dialog is unable to exercise control over the security or subject matter of Content passing
        over the Dialog Network, the Sites or via the Services and Dialog hereby excludes all liability of any kind for
        the transmission or reception of infringing Content of whatever nature.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Liability for third party content</h5>
<hr>
<ul>
    <li>The Sites contains links to web sites, web pages, products and services also operated or partnered by Dialog and
        you agree that your use of each web site, web page and service is also subject to the terms and conditions, if
        any, contained within each website or webpage or attached to any products or services. These Terms and
        Conditions shall be deemed as incorporated into each set of terms and conditions. In the event that there is any
        conflict, the terms and conditions contained within the relevant website or web page or attached to the relevant
        products or services shall prevail over these Terms and Conditions.
    </li>
    <li>Dialog assumes no responsibility for and does not endorse unless expressly stated, Content created or published
        by third parties that is included in the Sites and the Services or which may be linked to and from the Sites.
    </li>
    <li>The Sites and/or the Services may be used by you to link into other websites, resources and/or networks
        worldwide. Dialog accepts no responsibility for the Content, services or otherwise in respect of these and you
        agree to conform to the acceptable use policies of such websites, resources and/or networks.
    </li>
    <li>Subject to Clause 13, you agree that Dialog does not generally and is not required to monitor or edit the use to
        which you or others use the Sites and the Services or the nature of the Content and Dialog is excluded from all
        liability of any kind arising from the use of the Services, and in particular but without limitation to the
        foregoing, the nature of any Content. Notwithstanding the foregoing, Dialog reserves the right to edit, bar or
        remove any Services and/or Content, at any time as Dialog in its sole discretion believes to be necessary in
        order to prevent any breach of these Terms and Conditions or any breach of applicable laws or regulations.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Exclusion of liability</h5>
<hr>
<ul>
    <li>Nothing in these Terms and Conditions shall act to limit or exclude Dialog's liability for death or personal
        injury resulting from Dialog's negligence, fraud or any other liability, which may not by applicable law be
        limited or excluded.
    </li>
    <li>Dialog shall use its reasonable endeavours to ensure the maintenance and availability of the Sites and the
        Services but availability may be affected by your equipment, other communications networks, user congestion of
        the Dialog Network or the Internet at the same time or other causes of interference and may fail or require
        maintenance without notice.
    </li>
    <li>Neither Dialog nor any member of the Dialog shall be liable for any special, indirect or consequential damages
        or any damages whatsoever, whether in an action of contract, negligence or other tortuous action, arising out of
        or in connection with the performance of or use of Services available on the Sites and in particular, but
        without limitation to the foregoing, Dialog specifically excludes all liability whatsoever in respect of any
        loss arising as a result of :
        <ul>
            <li>use which you make of the Sites and the Services or reliance on Services or any loss of any Services or
                your Content resulting from delays, non-deliveries, missed deliveries, or service interruptions
            </li>
            <li>defects that may exist or for any costs, loss of profits, loss of your Content or consequential losses
                arising from your use of, or inability to use or access or a failure, suspension or withdrawal of all or
                part of the Sites and the Services at any time.
            </li>
        </ul>
    </li>
    <li>All conditions or warranties which may be implied or incorporated into these Terms and Conditions by law or
        otherwise are hereby expressly excluded to the extent permitted by law.
    </li>
    <li>Your only remedy under these Terms and Conditions is to discontinue using the Sites and the Services.</li>
    <li>Dialog makes every effort to ensure the security of your communications. You are however advised that for
        reasons beyond our control, there is a risk that your communications may be unlawfully intercepted or accessed
        by those other than the intended recipient. For example, your communications may pass over third party networks
        over which we have no control. The Internet is not a secure environment. Unwanted programs or material maybe
        downloaded without your knowledge, which may give unauthorised persons access to your mobile phone and the
        information stored on your mobile phone. These programs may perform actions that you have not authorised,
        possibly without your knowledge.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Variation</h5>
<hr>
<ul>
    <li>Dialog reserves the right to modify the Sites and/or the Services or suspend or terminate the Sites and / or the
        Services or access to part or all of them at any time.
    </li>
    <li>Dialog reserves the right to revise these Terms and Conditions at any time. Such variations shall become
        effective two weeks after being posted on the website. By continuing to use this website you will be deemed to
        have accepted the varied Terms and Conditions.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Indemnity</h5>
<hr>
<ul>
    <li>You hereby agree to fully indemnify and to hold Dialog harmless from and against any claim brought by a third
        party resulting from the use of the Sites and the Services or the provision of Content to Dialog by you and in
        respect of all losses, costs, actions, proceedings, claims, damages, expenses (including reasonable legal costs
        and expenses), or liabilities, whatsoever suffered or incurred directly or indirectly by Dialog in consequence
        of such use of the Sites and the Services or provision of Content or your breach or non-observance of any of
        these Terms and Conditions.
    </li>
    <li>You shall defend and pay all costs, damages, awards, fees (including any reasonable legal fees) and judgments
        awarded against Dialog arising from the above claims and shall provide Dialog with notice of such claims, full
        authority to defend, compromise or settle such claims and reasonable assistance necessary to defend such claims,
        at your sole expense.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Monitoring / recording of communications</h5>
<hr>
<ul>
    <li>Monitoring or recording of your calls, emails, text messages or other communications may take place when
        required in accordance with the law, and in particular for Dialog's business purposes, such as for quality
        control and training, to prevent unauthorised use of Dialog's telecommunication systems and to ensure effective
        systems operation and in order to prevent or detect crime.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Processing your instructions</h5>
<hr>
<ul>
    <li>You request and authorise us to rely and act upon all apparently valid communications as instructions properly
        authorised by you, even if they may conflict with any other instructions given at any time concerning bill or
        service requirements.
    </li>
    <li>An instruction will only be accepted by us if you have passed through certain security criteria.</li>
    <li>You agree that we can act on any instructions given to us even if these instructions were not authorised by
        you.
    </li>
    <li>We will make reasonable efforts to process any instruction where you request us to do so but we shall not be
        liable for any failure to comply with such a request unless it is due to Dialog's failure to make reasonable
        efforts to do so.
    </li>
    <li>You must make sure that any instruction is accurate and complete and we are not liable if this is not the
        case.
    </li>
    <li>A transaction being carried out is not always simultaneous with an instruction being given. Some matters may
        take time to process and certain instructions may only be processed during normal working hours even though the
        service may be accessible outside such hours.
    </li>
    <li>You will be responsible for all losses and payments (including the amount of any transaction carried out without
        your authority) if you have acted with gross negligence so as to facilitate that unauthorised transaction, or
        you have acted fraudulently. For the purposes of this Clause gross negligence shall be deemed to include failure
        to observe any of your security duties referred to in these Terms and Conditions.
    </li>
</ul>
<hr>

<h5><i class="icon-edit"></i> Profligate use</h5>
<hr>
<ul>
    <li>Profligate use of the Dialog GPRS/Data Network is prohibited. Dialog considers that any application which
        transmit live video, live audio or make similar excessive traffic demands across the Dialog Network by whatever
        means, unless provided by Dialog, constitutes making profligate use of the Dialog Network. Use of IP Multicast,
        other than by means provided and co-ordinated by Dialog is also prohibited.
    </li>
</ul>
<hr>
<hr>

<h5><i class="icon-edit"></i> Termination</h5>
<hr>
<ul>
    <li>Dialog may elect to suspend, vary or terminate the Services and the Sites at any time without prior notice for
        repair or maintenance work or in order to upgrade or update the Sites and the Services or for any other reason
        whatsoever.
    </li>
    <li>Dialog may elect to terminate the Services or your access to the Sites forthwith on breach of any of these Terms
        and Conditions by you, including, without limitation, delayed or non payment of sums due to Dialog or if Dialog
        ceases to offer the Sites and the Services to its customers for any reason whatsoever.
    </li>
</ul>
<hr>
<hr>

<h5><i class="icon-edit"></i> General</h5>
<hr>
<ul>
    <li>Governing Law and Jurisdiction - These Terms and Conditions are governed by and construed in accordance with
        English law and you hereby submit to the non-exclusive jurisdiction of the Sri Lankan courts.
    </li>
    <li>Severability - These Terms and Conditions are severable in that if any provision is determined to be illegal or
        unenforceable by any court of competent jurisdiction such provision shall be deemed to have been deleted without
        affecting the remaining provisions of these Terms and Conditions.
    </li>
    <li>Waiver - Dialog's failure to exercise any particular right or provision of these Terms and Conditions shall not
        constitute a waiver of such right or provision unless acknowledged and agreed to by Dialog in writing.
    </li>
    <li>Representations - You acknowledge and agree that in entering into these Terms and Conditions you do not rely on,
        and shall have no remedy in respect of, any statement, representation, warranty or understanding (whether
        negligently or innocently made) of any person (whether party to these Terms and Conditions or not) other than as
        expressly set out in these Terms and Conditions as a warranty. Nothing in this Clause shall, however, operate to
        limit or exclude any liability for fraud.
    </li>
    <li>Assignment - You in entering into these Terms and Conditions undertake that you will not assign, re-sell,
        sub-lease or in any other way transfer your rights or obligations under these Terms and Conditions or part
        thereof. Contravention of this restriction in any way, whether successful or not, will result in the Services
        being terminated by Dialog forthwith. Dialog may assign these Terms and Conditions in whole or in part to any
        third party at its discretion.
    </li>
    <li>Rights of Third Parties - Except in the case of any permitted assignment of this Agreement under Clause 17.5, a
        person who is not a party to this Agreement has no right of enforcement of any term or condition contained in
        this Agreement.
    </li>
    <li>Force Majeure - Dialog shall not be liable in respect of any breach of these Terms and Conditions due to any
        cause beyond its reasonable control including but not limited to, Act of God, inclement weather, act or omission
        of Government or public telephone operators or other competent authority or other party for whom Dialog is not
        responsible.
    </li>
</ul>
<hr>


<h5><i class="icon-edit"></i> Definitions</h5>
<hr>
<ul>
    <li>"Access Information" means the access information that we require from you before entering certain parts of the
        Sites for which you have registered, which may be a username, password, your mobile phone number or CLI or
        similar recognition device
    </li>
    <li>"Content" means all data, information, material and content, including but not limited to text, pictures,
        photographs, software, video, music, sound and graphics
    </li>
    <li>"Functionalities" means the services offered on or via the Sites, which may include (but is not limited to)
        chat, discussion group or bulletin board services or email, SMS or voice messaging services, online
        transactions, search engines and e-commerce facilitators
    </li>
    <li>"Services" has the meaning given to it in Clause 2.1</li>
    <li>"Terms and Conditions" means the contract between Dialog and you incorporating these terms and conditions</li>
    <li>"Trade Marks" means the word or mark "Dialog", however represented, including stylised representations, all
        associated logos and symbols, and combinations of the foregoing with another word or mark
    </li>
    <li>"Dialog" means Dialog Axiata PLC, whose registered office is at 475 Union Place, Colombo 2, Sri Lanka and may
        also be referred to as "Dialog", "we" or "us" in these Terms and Conditions.
    </li>
</ul>
<hr>
<hr>
