<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="vdf-slideshow" align="center">
    <%--<img src="./resources/images/top-banner.jpg" style="width: 100%;"/>--%>
    <%--SLIDE SHOW--%>
    <!-- Carousel ================================================== -->
    <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <c:set value="0" var="count"/>
            <c:forEach items="${featuredApps}" var="app">
            <li data-target="#myCarousel" data-slide-to='<c:out value="${count}"/>'
                class='<c:if test="${count==0}">active</c:if>'></li>
            <c:set value="${count+1}" var="count"/>
            </c:forEach>
        </ol>
        <div class="carousel-inner">
            <c:set value="1" var="count"/>
            <c:forEach items="${featuredApps}" var="app">
                <div class='item <c:if test="${count eq 1}">active</c:if> '>
                        <%--dummy images remove this later--%>
                    <c:forEach items="${app.appBanner}" var="entry">
                        <c:if test='${!fn:contains(entry["url"],"Mobile_Banner")}'>
                            <img src='<c:out value="${entry['url']}"/>' class="appstore-slideshow-image"/>
                        </c:if>
                    </c:forEach>

                    <a href='view?appId=<c:out value="${app.id}"/>'>
                        <div class="appstore-slideshow-description">

                            <span class="appstore-slideshow-app-name appstore-ellipsis"><c:out
                                    value="${app.name}"/></span>

                            <br/>
                                    <span class="appstore-slideshow-app-category"><c:out
                                            value="${app.category}"/> </span>
                        <span class="appstore-slideshow-app-rating">
                            <c:if test="${app.rating ge 0}">
                                <c:forEach begin="1" end="${app.rating}">
                                    <i class="icon-star rating-color slide-rating"></i>
                                </c:forEach>
                            </c:if>
                            <c:if test="${(app.rating ge 1) && (app.rating lt 5)}">
                                <c:forEach begin="1" end="${5 - app.rating}">
                                    <i class="icon-star-empty rating-color slide-rating"></i>
                                </c:forEach>
                            </c:if>
                            <c:if test="${app.rating eq 0}">
                                <c:forEach begin="1" end="5">
                                    <i class="icon-star-empty rating-color slide-rating"></i>
                                </c:forEach>
                            </c:if>
                        </span>
                                    <span class="appstore-slideshow-app-dev-name"><fmt:message
                                            code="appstore.app.developer.by"/> <c:out
                                            value="${app.developer}"/></span><br/>
                    <span class="appstore-slideshow-app-des">
                        <c:out value="${fn:substring(app.description,0,90)}"/>...
                    </span>
                        </div>
                    </a>
                    <c:set value="${count+1}" var="count"/>

                </div>
            </c:forEach>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span
                class="glyphicon glyphicon-chevron-left appstore-slideshow-icon"></span></a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span
                class="glyphicon glyphicon-chevron-right appstore-slideshow-icon"></span></a>
    </div>
    <!-- /.carousel -->
    <%--SLIDE SHOW--%>

</div>