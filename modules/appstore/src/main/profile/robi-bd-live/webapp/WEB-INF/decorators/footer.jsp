<div>
    <img class="img-responsive alpona" src="./resources/images/robi_alpona_2.png"/>
    <footer align="center">
        <div class="footer-content">
            <div class="col-md-3 col-sm-3 col-xs-12">

            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 footer-text">
                <%--<a href='<fmt:message code=""/>'><fmt:message code="appstore.footer.menu.help"/></a> |--%>
                <a href='<fmt:message code="robi.support"/>'><fmt:message code="appstore.footer.menu.support"/></a> |
                <a href='<fmt:message code="robi.termscondt"/>'><fmt:message
                        code="appstore.footer.menu.terms.and.conditions"/></a> |
                <%--<a href='<fmt:message code=""/>'><fmt:message code="appstore.footer.menu.privacy.policy"/></a> |--%>
                <a href='<fmt:message code="robi.contactus"/>'><fmt:message
                        code="appstore.footer.menu.contact.us"/></a>
                <%--<a href='<fmt:message code=""/>'><fmt:message code="appstore.footer.menu.about.us"/></a> |--%>
                <%--<a href='<fmt:message code="appstore.vodafone.blog.url"/>'><fmt:message code="appstore.footer.menu.blog"/></a>--%>
                <br/>
                <fmt:message code="appstore.footer.copy.rights.notice"/>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="responsive-social-icons">
                    <a href='<fmt:message code="robi.facebook"/>'>
                        <div class="footer-social-icon">
                            <i class="icon-facebook"></i>
                        </div>
                    </a>
                    <a href='<fmt:message code="robi.twitter"/>'>
                        <div class="footer-social-icon">
                            <i class="icon-twitter"></i>
                        </div>
                    </a>
                    <a href='<fmt:message code="robi.youtube"/>'>
                        <div class="footer-social-icon">
                            <i class="icon-youtube"></i>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </footer>
</div>