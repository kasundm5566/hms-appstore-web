<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags"%>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <sitemesh:write property='head'></sitemesh:write>

        <link rel="shortcut icon" href="./resources/images/favicon.ico">

        <title>
            <sitemesh:write property='title'/>
        </title>
        <!-- Bootstrap core CSS -->
        <link href="./resources/css/bootstrap.min.css" rel="stylesheet">

            <%--font awesome css--%>
        <link rel="stylesheet" href="./resources/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="./resources/css/custom.css" rel="stylesheet">
        <link href="./resources/css/robi.css" rel="stylesheet">
        <link href="./resources/css/slidebars.min.css" rel="stylesheet"/>
        <link href="./resources/css/offcanvas.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./resources/js/html5shiv.js"></script>
        <script src="./resources/js/respond.min.js"></script>
        <![endif]-->
    <%--colorbox css--%>
        <%--essential javascript--%>
        <script src="./resources/js/jquery.js"></script>
        <script src="./resources/js/bootstrap-paginator.js"></script>

        <link href="./resources/css/colorbox.css" type="text/css" rel="stylesheet"/>

    </head>

    <body itemscope itemtype="http://schema.org/Product">
    <div id="sb-site">
    <div class="row">
            <%--Navigation start--%>
        <%@ include file="top-navigation.jsp" %>
            <%--Navigation end--%>
    </div>
    <div class="row" style="min-height: 100%;">
        <div class="">

            <div id="fb-root"></div>

                <%--Page content begins--%>
            <div class="" id="containerElement">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12" id="mainBodyCol">
                        <sitemesh:write property='body'/>
                    </div>

                        <%--left app panel begin--%>
                    <appstoreLeftPanel id="appStoreLeftPanel">
                        <%@ include file="left-app-panel.jsp" %>
                    </appstoreLeftPanel>
                        <%--left app panel end--%>

                </div>
            </div>

                <%--Page content ends--%>
            <%@ include file="modals.jsp" %>

        </div>
    </div>

        <%--footer begin--%>
    <div class="row">
        <%@ include file="footer.jsp" %>
    </div>
        <%--footer end--%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="./resources/js/bootstrap.min.js"></script>
    <script src="./resources/js/offcanvas.js"></script>
    <script src="./resources/js/bootstrap-tooltip.js"></script>
    <script src="./resources/js/bootstrap-popover.js"></script>
    <script src="./resources/js/jquery.colorbox-min.js"></script>
    <script src="./resources/js/jquery.validate.js"></script>
    <script src="./resources/js/slidebars.min.js"></script>
    <script src="./resources/js/custom.js"></script>

    </div>
    </body>
    </html>