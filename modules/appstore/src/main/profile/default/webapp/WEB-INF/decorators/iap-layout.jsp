<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport"
              content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <sitemesh:write property='head'></sitemesh:write>

        <link rel="shortcut icon" href="./resources/images/fevicon.png">

        <title>
            <sitemesh:write property='title'/>
        </title>
        <!-- Bootstrap core CSS -->
        <link href="./resources/css/bootstrap.min.css" rel="stylesheet">

            <%--font awesome css--%>
        <link rel="stylesheet" href="./resources/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="./resources/css/custom.css" rel="stylesheet">
        <link href="./resources/css/dialog.css" rel="stylesheet">
        <link href="./resources/css/offcanvas.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./resources/js/html5shiv.js"></script>
        <script src="./resources/js/respond.min.js"></script>
        <![endif]-->
        <script src="./resources/js/jquery.min.js"></script>
        <script src="./resources/js/bootstrap.min.js"></script>
    </head>

    <body itemscope itemtype="http://schema.org/Product">
    <div id="sb-site">
        <div class="row">
                <%--Navigation start--%>
            <%@ include file="iap-top-navigation.jsp" %>
                <%--Navigation end--%>
        </div>

        <div class="row" style="min-height: 100%;">
            <div class="container">

                <div id="fb-root"></div>

                    <%--Page content begins--%>
                <div class="" id="containerElement">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" id="mainBodyCol">
                            <sitemesh:write property='body'/>
                        </div>
                    </div>
                </div>

                    <%--Page content ends--%>
                <%@ include file="modals.jsp" %>

            </div>
        </div>


    </div>
    </body>
        <%--footer begin--%>
    <div class="row" style="position: absolute;width: 100%;top:900px" id="iap-footer">
        <%@ include file="footer.jsp" %>
    </div>
        <%--footer end--%>
    <%--<script type="text/javascript">
        function fixFooter(){
            var height = Math.min($(document).height(),1000);
            console.info('fixing footer location : '+height);
            $('#iap-footer').css('top', height);
        }
        $(window).resize(fixFooter);
        $(document).ready(function () {
            fixFooter();
        })
    </script>--%>
    </html>
</fmt:bundle>