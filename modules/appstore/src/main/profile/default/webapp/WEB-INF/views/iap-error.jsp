<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title>
            <fmt:message key="iap.title"/>
        </title>
    </head>
    <body>

    <div class="panel panel-default col-sm-8 col-sm-offset-2" style="margin-top: 5%;">

        <div class="panel-body">
            <h4 class="branding-font-style">
                <fmt:message key="iap.invalid.request"/>
            </h4>

            <p><c:out value="${iap_error_message}"/></p>
        </div>
    </div>

    </body>
    </html>
</fmt:bundle>