/*
 * (C) Copyright 2010-2013 hSenid Mobile Solutions (Pvt) Limited.
 * All Rights Reserved.
 *
 * These materials are unpublished, proprietary, confidential source code of
 * hSenid Mobile Solutions (Pvt) Limited and constitute a TRADE SECRET
 * of hSenid Mobile Solutions (Pvt) Limited.
 *
 * hSenid Mobile Solutions (Pvt) Limited retains all title to and intellectual
 * property rights in these materials.
 */

var config = {
    "discovery-api-base-url": "https://apps.sdp.hsenidmobile.com/discovery-api/v2/",
    appId: ''                        // App ID from the app dashboard
}

$('.carousel').carousel({
    interval: 3000
});

/*
 * appAction is a user-defined tag to use in app download and subscribe buttons
 * */
$("appAction").click(function () {
    loadAppActionModal(this);
});

$("appAddReview").click(function () {
    loadAppReviewModal(this);
});

$("#signInButton").click(function () {
    loadSignInModal();
});

$(".unSUbButton").click(function () {
    loadUnsubscibeModal(this);
});

//This is to change the left panel title in app view
$(function () {
    var getURL = window.location.pathname;
    var getPanelNameElement = $('.app-panel-title-right');
    var getURLParamIndex = getURL.indexOf("view");

    if (getURLParamIndex != -1) {
        getPanelNameElement.html("Related Apps");
    } else if (getURL.indexOf("mostly-used") > 0) {
        getPanelNameElement.html("Newly Added Apps")
    }
});
//End the changing of panel name

$(function () {
    var imageTag = $("img");
    var imgSrc = imageTag.attr("src");

    if (imgSrc == "" || imgSrc == null) {
        imageTag.attr("src", "./resources/images/sample_app_icon.png");
    } else {

    }
});

function loadAppActionModal(getClickingElement) {
    console.log("APP ACTION TRIGGER")
    var getAppId = $(getClickingElement).attr("app-id");
    var getAppType = $(getClickingElement).attr("app-type");
    if (getAppType.trim() == 'download') {
        console.log("Downloadable App")
        checkUserLoginSession(('download?appId=' + getAppId));

    } else if (getAppType.trim() == 'unSub') {
        loadUnsubscibeModal(getClickingElement);
    }
    else {
        console.log("Subscription App")
        checkUserLoginSession(('subscribe?appId=' + getAppId));
    }

    console.log(getAppId);
    console.log(getAppType.trim());
}

function loadSignInModal() {
    loadModalContent('login');
}

function loadUnsubscibeModal(getClickingElement) {
    var getAppId = $(getClickingElement).attr("app-id");
    checkUserLoginSession(('unsubscribe?appId=' + getAppId));
}

function loadAppReviewModal(getClickingElement) {

    var getAppName = $(getClickingElement).attr("app-name");
    var getAppId = $(getClickingElement).attr("app-id");

    checkUserLoginSession('review?appId=' + getAppId);

    console.log(getAppId)
    console.log(getAppName)
}

String.prototype.replaceUrlAt = function (index, string) {
    var base = this.toString().substring(0, index);
    var retVal = base + string;
    return retVal;
}

function checkUserLoginSession(modalName) {
    var url = document.baseURI;
    var i_replace = (url.indexOf("appstore") + "appstore".length + 1);
    url = url.replaceUrlAt(i_replace, "checkUserSession");
    var userRoleURL = "isAdminRole";
    $('#modalContent').html('<img src="resources/images/loading.gif"/>');
    console.log("CHECK SESSION");

    var jqXHR = $.ajax({
        type: "GET",
        url: url,
        success: function (data, status) {
            console.log(jqXHR.responseText + "->" + status);
            if (jqXHR.responseText == 'sessionNotOk') {
                console.log("---------------" + jqXHR.responseText);
                loadSignInModal();
            } else {

                /*validate user role*/
                var jqXHRUserRole = $.ajax({
                    type: "GET",
                    url: userRoleURL,
                    success: function (data, status) {
                        console.log(jqXHRUserRole.responseText + "->" + status);

                        if (jqXHRUserRole.responseText == 'isAdmin') {
                            loadModalContent('userRole');
                        } else {
                            loadModalContent(modalName);
                        }
                    }
                });
                /*validate user role*/
            }
        }
    });
}

function loadModalContent(modalPath) {
    var url = document.baseURI;
    var i_replace = (url.indexOf("appstore") + "appstore".length + 1);
    url = url.replaceUrlAt(i_replace, "");

    $('#modalContent').html('<img src="resources/images/loading.gif"/>');

    $.ajax({
        type: "GET",
        url: url + modalPath,
        success: function (data) {
            $('#modalContent').html(data);
        }
    });
}

$(function () {
    $("p[title='comment']").popover();
});

//image uploading
function getFile(elementID) {
    document.getElementById(elementID).click();
    return false;
}

function preview(input, previewArea, placeHolder, textBoxID, uploadType, fileNamePrefix, errorNoteId) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {

            var fileSrc = e.target.result;
            var getPreviewArea = "#" + previewArea;
            var getPlaceHolder = "#" + placeHolder;

            base64ImgToId(fileSrc, textBoxID, uploadType, fileNamePrefix, errorNoteId, getPlaceHolder, getPreviewArea);
        };

    } else {
    }

    reader.readAsDataURL(input.files[0]);
}

function base64ImgToId(base64, textBoxID, uploadType, fileNamePrefix, errorNoteId, imagePlaceHolder, getPreviewArea) {

    var baseURL = config["discovery-api-base-url"];
    var actionURL = baseURL + "image-upload";
    var imageSrc = base64;
    var fType = base64.split(';')[0].split('/')[1]
    if (fType == "jpeg") {
        fType = "jpg";
    }
    base64 = base64.split(';')[1].split(',')[1]

    var jsonParsed = JSON.stringify({
        fileType: fType,
        uploadType: uploadType,
        fileNamePrefix: fileNamePrefix,
        data: base64
    });

    $.ajax({
        type: "POST",
        url: actionURL,
        contentType: 'application/json',
        data: jsonParsed,
        dataType: 'json',

        success: function (data, status, jqXHR) {

            var genID = data.result.uid;
            var hashTextId = "#" + textBoxID;
            var hashTextBox = $(hashTextId);
            var errorTextId = "#" + errorNoteId;
            var getErrorText = $(errorTextId);

            if (genID == -1) {
                var errorNote = "<i class='icon-remove'></i> " + data.statusDescription;
                getErrorText.html(errorNote);
                hashTextBox.attr("value", "");
                $(getPreviewArea).attr("src", "");
                $(getPreviewArea).attr("style", "display:none;");
                $(imagePlaceHolder).attr("style", "");
            } else {
                getErrorText.html("");
                $(imagePlaceHolder).attr("style", "display:none");
                hashTextBox.attr("value", genID);
                $(getPreviewArea).attr("src", imageSrc);
                $(getPreviewArea).attr("style", "");
            }


        }
    });

}


//load left panel to app store
$(function () {
    var getPageElement = $('getPage');
    var getPageId = getPageElement.attr('id');
    console.log(getPageId);
    if (getPageId == 'support' ||
        getPageId == 'help' ||
        getPageId == 'terms' ||
        getPageId == 'privacy' ||
        getPageId == 'about' ||
        getPageId == 'contact' ||
        getPageId == '404' ||
        getPageId == '500' ||
        getPageId == 'error' ||
        getPageId == 'publish') {
        var getLeftPanelElement = $('appStoreLeftPanel');
        var getContainerElement = $('#containerElement');
        var getMainColumnElement = $('#mainBodyCol');

        getLeftPanelElement.remove();
        getContainerElement.attr("class", "container");
        getMainColumnElement.attr("class", "col-md-12, col-sm-12, col-xs-12");
    } else {
    }
});


/*--------------------------------------------------*/
$(function () {
    $('#fb_share').click(function (e) {

        var fbshare = $('#fb_share');
        var appName = fbshare.attr("fb_app_name");
        var appIns = fbshare.attr("fb_app_ins");
        var appDes = fbshare.attr("fb_app_des");
        var appIcon = fbshare.attr("fb_app_icon");
        var appURL = fbshare.attr("fb_app_url");

        e.preventDefault();
        FB.ui({
            method: 'feed',
            name: appName.trim(),
            link: appURL.trim(),
            picture: appIcon.trim(),
            caption: appIns.trim(),
            description: appDes.trim(),
            message: ''
        });
    });
});

$(function () {
    window.fbAsyncInit = function () {
        // init the FB JS SDK
        FB.init({
            appId: config['appId'],                        // App ID from the app dashboard
            status: true,                                 // Check Facebook Login status
            xfbml: true                                  // Look for social plugins on the page
        });

        // Additional initialization code such as adding Event Listeners goes here
    };

    // Load the SDK asynchronously
    (function () {
        // If we've already installed the SDK, we're done
        if (document.getElementById('facebook-jssdk')) {
            return;
        }

        // Get the first script element, which we'll use to find the parent node
        var firstScriptElement = document.getElementsByTagName('script')[0];

        // Create a new script element and set its id
        var facebookJS = document.createElement('script');
        facebookJS.id = 'facebook-jssdk';

        // Set the new script's source to the source of the Facebook JS SDK
        facebookJS.src = '//connect.facebook.net/en_US/all.js';

        // Insert the Facebook JS SDK into the DOM
        firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
    }());
});

/*================Social Network Integration================*//*
 *//**//*twitter begins*/
/**/
!function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (!d.getElementById(id)) {
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);
    }
}(document, "script", "twitter-wjs");
//twitter ends

//G+ begins
(function () {
    var po = document.createElement('script');
    po.type = 'text/javascript';
    po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();
/*G+ ends*/


$("form[action='subscribeNow']").on("submit", function (e) {
    e.preventDefault();
    console.info(this.serialize());
});

function closeRefresh() {
    $('.modal').modal('hide');
    window.location = document.baseURI;
}

function validateSearch() {
    if ($('.navbar-form input')[0].value == "") {
        return false;
    }
    return true;
}


/*app comment remove function*/
$('a.remove-comment-link').click(function (e) {
    e.preventDefault();
    var ans = confirm("are you sure to remove this comment?");
    if (ans) {
        var commentId = this.href.toString().split('=')[1];
        $.post(('removeComment?commentId=' + commentId), null, function (res) {
            if (res == 'true') {
                $(e.currentTarget.parentNode).fadeOut(1000, function () {
                    $(e.currentTarget.parentNode).remove();
                });

            }
        });
        console.log('comment id is : ' + commentId);
    }
    console.log('remove comment : ' + ans);
});

// *********** region validating the downloadable versions *********** //

var downloadBinary = [];

function emptyVersionList() {
    //removing loaded select options
    console.info('filtering versions begun.')
    $("#selectDeviceModal").children().each(function () {
        if ($(this).attr('selected') != 'selected') {
            $(this).remove();
        }
    });
}

function validateSelectOptions(platfoam, allDownloads) {

    emptyVersionList();

    //filter the list of valid downloads
    console.info('allDownloads ' + allDownloads);
    var filteredList = [];
    for (var i = 0; i < allDownloads.length; i++) {
        if (platfoam.toString().length > 0) {

            if (allDownloads[i].toString().indexOf(platfoam) > -1) {
                filteredList.push(allDownloads[i]);
            }
        } else {
            filteredList = [];
        }
    }
    console.info('filtered list : ' + filteredList);
    var parent = document.getElementById('selectDeviceModal');
    for (var i = 0; i < filteredList.length; i++) {

        var item = document.createElement('option');
        item.setAttribute('value', filteredList[i]);
        item.innerHTML = filteredList[i];
        parent.appendChild(item);
    }
    console.info('filtering process finished.');
}

//loading initial data
$('#appStoreModal').on('shown.bs.modal', function (e) {
    downloadBinary = [];
    function callDelay() {
        $('#selectDeviceModal').children().each(function () {
            console.info('loading versions to array');
            downloadBinary.push($(this).val());

        });
        emptyVersionList();
        $('#selectDeviceMake').change(function (e) {
            console.info('selection changed.');
            var selectedPlatfoam = $(this).val();
            console.info('selectedPlatfoam :' + selectedPlatfoam);
            validateSelectOptions(selectedPlatfoam, downloadBinary);
        });
    }
    setTimeout(callDelay,500);
});
// *********** end region validating the downloadable versions *********** //

//******************** colorbox start ******************
$(document).ready(function () {
    $("a.colorbox-view").colorbox({
        rel: 'group',
        transition: "fade",
        width: "55%",
        height: "75%",
        scalePhotos: true
    });
});

//css hack for responsive devices set app tile
$(document).ready(function () {
    resizeTiles();
    resizeRatingStars();
    if ($.slidebars) {
        $.slidebars();
        $('.sb-toggle-submenu').off('click').on('click', function () {
            $submenu = $(this).parent().children('.sb-submenu');
            $(this).add($submenu).toggleClass('sb-submenu-active'); // Toggle active class.

            if ($submenu.hasClass('sb-submenu-active')) {
                $submenu.slideDown(200);
            } else {
                $submenu.slideUp(200);
            }
        });
    }
});
$(window).resize(function () {
    resizeTiles();
    resizeRatingStars();
    alignTopNav();
});

$(function () {
    alignTopNav();
});

function alignTopNav() {
    if (window.innerWidth < 325) {
        $('#nav_wrapper_resp').css({'text-align': 'left'});
    } else {
        $('#nav_wrapper_resp').css({'text-align': 'right'});
    }
    if (window.innerWidth < 380) {
        $('.footer-content a:nth-child(3)').css({'display': 'block'});
    } else {
        $('.footer-content a:nth-child(3)').removeAttr('style');
    }
}

function resizeRatingStars() {
    if (window.innerWidth < 534) {
        $('.icon-star').css({'font-size': '10px'});
        $('.icon-star-empty').css({'font-size': '10px'});
        $('.appstore-app-name').css({'font-size': '12px'});
    } else {
        $('.icon-star').removeAttr('style');
        $('.icon-star-empty').removeAttr('style');
        $('.appstore-app-name').removeAttr('style');
    }
}
function resizeTiles() {
    if (window.innerWidth < 768 && window.innerWidth > 480) {
//        console.info('altering app-tiles');
        $('.app-tile').css('width', '31%');
    } else {
        $('.app-tile').removeAttr('style');
    }

    if (window.innerWidth < 600 && window.innerWidth > 480) {
        $('.appstore-app').css('height', '200px');
    }
    else if (window.innerWidth <= 400) {
        $('.appstore-app').css('height', '200px');
    } else {
        $('.appstore-app').removeAttr('style');
    }
}

