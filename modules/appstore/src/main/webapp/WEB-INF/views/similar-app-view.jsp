<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><c:out value="${panelTitle}"/> <fmt:message code="appstore.main.title"/></title>
</head>

<body>
<%--Page content begins--%>
<div class="row">
</div>

<div class="row">
    <div>

        <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
            <div class="col-md-12 padding-left-right-0 vdf-app-panel-title app-panel-title">
                Apps Similar to ${appName}
            </div>
        </div>

        <%--alert if no applications found--%>
        <c:if test="${fn:length(similarApps) eq 0}">
            <h4 class="alert alert-warning"><fmt:message code="appstore.app.app.category.no.apps.notice"/></h4>
        </c:if>

        <c:if test="${fn:length(similarApps) ge 0}">
            <c:forEach items="${similarApps}" var="app">

                <a href='view?appId=<c:out value="${app.id}"/>'>
                    <div class="col-md-2 col-sm-4 col-xs-6 app-tile">
                        <div class="appstore-app container">
                            <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                 class="appstore-app-icon"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${app.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>

                                <br/>

                                <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${app.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${app.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${app.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${app.canDownload && !app.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                            </span>
                        </div>
                    </div>
                </a>

            </c:forEach>
        </c:if>
    </div>
</div>
<%--Page content ends--%>

</body>
</html>