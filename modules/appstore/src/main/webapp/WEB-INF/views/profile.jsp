<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><c:out value="${userName}"/> - <fmt:message code="appstore.main.title"/></title>
</head>

<body>
<%--Page content begins--%>
<div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
    <div class="col-md-12 padding-left-right-0 vdf-app-panel-title app-panel-title"><fmt:message code="appstore.my.account"/></div>
</div>

<div class="row profile-detail-tile-inner">
    <form id="editProfileForm" class="form-horizontal">
        <div class="row padding-15">

            <div class="col-md-3" style="padding-left: 30px">
                <div class="col-md-12 container">
                    <c:if test="${profileImage_1 != '' }">
                        <img src='${baseUrl}${profileImage}' class="profile-pic-circle" width="100%">
                    </c:if>
                    <c:if test="${profileImage_1 == '' }">
                        <img src='./resources/images/appstore_default_profile.jpg' class="profile-pic-circle" width="100%">
                    </c:if>
                    <i id="upload-button" style="display: none" class="glyphicon glyphicon-camera upload-button"></i>
                    <input id="image-upload" class="file-upload" type="file" name="profileImage" accept="image/*"/>
                </div>
            </div>

            <div class="col-md-6">
                <input type="hidden" name="userName" value="${userName}">
                <input type="hidden" name="mobileNumber" value="${mobile}">
                <div class="form-group">
                    <label for="fullName" class="col-md-3 control-label"> <fmt:message code="appstore.user.profile.full.name"/> </label>
                    <label id="fullNamError" class="error-labels"></label>
                    <div class="col-md-9">
                        <c:set var="fullName" value="${firstName} ${lastName}"/>
                        <input type="text" name="fullName" class="form-control" id="fullName" value="${fn:trim(fullName)}" disabled required="true"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-3 control-label"> <fmt:message code="appstore.user.profile.email"/> </label>
                    <div class="col-md-9">
                        <input name="email" class="form-control" id="email" value="${email}" disabled/>
                        <c:if test="${ userType ne 'INDIVIDUAL'}">
                            <input type="hidden" name="email" value="${email}"/>
                        </c:if>

                    </div>
                </div>

                <div class="form-group" id="emailValidationError">
                    <label class="col-md-3"></label>
                    <label class="col-md-9 text-danger"><fmt:message
                            code="appstore.user.profile.email.validation.error"/></label>
                </div>

                <div class="form-group" id="emailAlreadyUsedError">
                    <label class="col-md-3"></label>
                    <label class="col-md-9 text-danger"><fmt:message
                            code="appstore.user.profile.email.already.used"/></label>
                </div>

                <div class="form-group">
                    <label for="mobile" class="col-md-3 control-label"><fmt:message code="appstore.user.profile.mobile"/></label>
                    <div class="col-md-9">
                        <label id="mobile" class="form-control" disabled> ${mobile} </label>
                    </div>
                </div>
                <div class="form-group margin-top-bottom-0">
                    <div class="col-md-offset-3 col-md-9">
                        <button id="editBtn" type="button" onclick="editProfile('${userType}')" class="btn btn-danger"> <fmt:message code="appstore.user.profile.edit"/> </button>
                        <button id="cancelBtn" style="display: none" class="btn btn-default">
                            <a href="profile" style="color: #000000"> <fmt:message code="appstore.user.profile.cancel"/> </a>
                        </button>
                        <button id="saveBtn" style="display: none" type="submit" class="btn btn-danger"> <fmt:message code="appstore.user.profile.save"/> </button>
                    </div>
                </div>
                <script>
                    function editProfile(userType) {
                        document.getElementById("editBtn").style.display = "none";
                        document.getElementById("saveBtn").style.display = "inline";
                        document.getElementById("cancelBtn").style.display = "inline";
                        document.getElementById("upload-button").style.display = "inline";
                        document.getElementById("fullName").disabled = false;
                        if( userType === 'INDIVIDUAL') {
                            document.getElementById("email").disabled = false;
                        }
                    }
                </script>
            </div>

        </div>

    </form>

    <%--alert modal--%>
    <button id="alertBtn" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#alertModal" style="display: none">
        Open Modal
    </button>
    <div id="alertModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header vdf-modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title branding-font-style text-center">
                        <i class="glyphicon glyphicon-exclamation-sign"></i>
                        <fmt:message code="appstore.invalid.image.resolution"/>
                        <span id='downloadableAppName'> <c:out value="${app.name}"/> </span></h4>
                </div>

                <div class="modal-body">
                    <p><fmt:message code="appstore.invalid.image.resolution.message"/></p>
                </div>

            </div>
        </div>
    </div>

</div>
<%--Page content ends--%>

<script>
    $(document).ready(function () {
        $("#emailAlreadyUsedError").hide();
        $("#emailValidationError").hide();
        var readURL = function (input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.profile-pic-circle').attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        };

        $(".file-upload").on('change', function(){
            var files = document.getElementById("image-upload").files[0];
            var image = new Image();
            image.src = window.URL.createObjectURL( files );

            image.onload = function() {
                var width = image.naturalWidth;
                var height = image.naturalHeight;
                if(width != height) {
                    document.getElementById('saveBtn').disabled = true;
                    document.getElementById("alertBtn").click();
                    return false;
                } else {
                    document.getElementById('saveBtn').disabled = false;
                    readURL(document.getElementById("image-upload"));
                }
            };
        });

        $(".upload-button").on('click', function() {
            $(".file-upload").click();
        });

        $("#editProfileForm").submit(function (e) {
            $("#emailAlreadyUsedError").hide();
            var emailReg = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/;
            if (!emailReg.test($("#email").val())) {
                $("#emailValidationError").show();
                return false;
            }else{
                $("#emailValidationError").hide();
            }
            $.ajax({
                type: "POST",
                url: "updateProfile",
                data: new FormData($(this)[0]),
                async: false,
                cache: false,
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result === "S1000") {
                        window.location.href="profile";
                        e.preventDefault();
                    } else if (result === "E1332") {
                        $("#emailAlreadyUsedError").show();
                        e.preventDefault();
                    } else {
                        console.log("Error in profile update: " + result);
                        e.preventDefault();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log("Error in profile update: " + jqXHR.status + " " + errorThrown);
                    e.preventDefault();
                }
            });
        });
    });
</script>

</body>
</html>

