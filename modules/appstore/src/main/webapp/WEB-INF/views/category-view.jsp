<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><c:out value="${panelTitle}"/> <fmt:message code="appstore.main.title"/></title>
</head>

<body>
    <%--Page content begins--%>
<div class="row">

    <div class="row app-panel-common-title-area vdf-app-panel-header">
        <span class="vdf-app-panel-title"><c:out value="${panelTitle}"/></span>
    </div>

    <div class="appstore-apps add-lr-margin">

            <%--alert if no applications found--%>
        <c:if test="${fn:length(appList) eq 0}">
            <h4 class="alert alert-warning"><fmt:message code="appstore.app.app.category.no.apps.notice"/></h4>
        </c:if>

        <c:set value="1" var="count"/>
        <c:if test="${fn:length(appList) ge 1}">
            <c:forEach items="${appList}" var="app">

                <c:if test="${ count eq 1 || count eq 11}">
                    <a href='view?appId=<c:out value="${app.id}"/>'>
                        <div class="col-md-3 col-sm-4 col-xs-6 app-tile-3 hidden-xs">
                            <div class="appstore-app container">
                                <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                     class="appstore-app-icon-3 img-thumbnail"/>
                            <span class="appstore-app-name hidden-md hidden-lg">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                        <%--application rating begin--%>
                                        <c:if test="${app.rating ge 0}">

                                            <%--limit rating--%>
                                            <c:if test="${app.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${app.rating le 5}">
                                                <c:set var="end" value="${app.rating}"/>
                                            </c:if>

                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (end lt 5)}">
                                            <c:forEach begin="1" end="${5 - end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <%--application rating end--%>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><fmt:message code="appstore.app.developer.by"/> <c:out
                                            value="${app.developer}"/></span>
                                </span>
                            </span>

                            <span class="appstore-app-detail-sub-3 hidden-sm hidden-xs">
                                <div class="appstore-app-name-3 appstore-ellipsis">
                                    <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                </div>
                                <div class="appstore-app-rating-area-3">
                                        <%--application rating begin--%>
                                    <c:if test="${app.rating ge 0}">
                                        <%--limit rating--%>
                                        <c:if test="${app.rating gt 5}">
                                            <c:set var="end" value="5"/>
                                        </c:if>
                                        <c:if test="${app.rating le 5}">
                                            <c:set var="end" value="${app.rating}"/>
                                        </c:if>

                                        <c:forEach begin="1" end="${end}">
                                            <i class="icon-star rating-color"></i>
                                        </c:forEach>
                                    </c:if>
                                            <c:if test="${(end ge 1) && (end lt 5)}">
                                        <c:forEach begin="1" end="${5 - end}">
                                            <i class="icon-star-empty rating-color"></i>
                                        </c:forEach>
                                    </c:if>
                                    <c:if test="${end eq 0}">
                                        <c:forEach begin="1" end="5">
                                            <i class="icon-star-empty rating-color"></i>
                                        </c:forEach>
                                    </c:if>
                                        <%--application rating end--%>
                                </div>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><fmt:message code="appstore.app.developer.by"/> <c:out
                                            value="${app.developer}"/></span>
                                </span>
                            </span>

                                <c:if test="${app.canUnSubscribe||app.canDownload||app.canSubscribe}">
                                    <appAction
                                        href=''
                                        data-target='#appStoreModal'
                                        data-toggle='modal'
                                        id='appActionButton'
                                        app-name='<c:out value="${app.name}"/>'
                                        app-id='<c:out value="${app.id}"/>'
                                        app-charging='<c:out value="${app.chargingLabel}"/>'
                                        app-type='
                                <c:if test="${app.canSubscribe}">
                                    subscribe
                                </c:if>
                                <c:if test="${app.canDownload}">
                                    download
                                </c:if>
                                <c:if test="${app.canUnSubscribe}">
                                    unSub
                                </c:if>'>
                                    <span class="appstore-app-detail-sub-3-2 hidden-sm hidden-xs">
                                            <span class="appstore-app-action-icon vdf-button" style="padding: 5px">
                                                <c:if test="${app.canSubscribe && !app.canDownload}">
                                                    <i class="icon-rss"></i> <fmt:message code="appstore.subscribe.button.title"/>
                                                </c:if>
                                                <c:if test="${app.canDownload && !app.canSubscribe}">
                                                    <i class="icon-download-alt"></i> <fmt:message code="appstore.download.button.title"/>
                                                </c:if>
                                                <c:if test="${app.canSubscribe && app.canDownload}">
                                                    <i class="icon-rss"></i> <fmt:message code="appstore.subscribe.button.title"/>
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i> <fmt:message code="appstore.unsub.button.title"/>
                                                </c:if>
                                            </span>
                                    </span>
                                        <span class="visible-sm hidden-md hidden-lg appstore-app-action-icon vdf-button">
                                       <c:if test="${app.canDownload && !app.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                       </c:if>
                                        <c:if test="${app.canSubscribe}">
                                            <i class="icon-rss"></i>
                                        </c:if>
                                        <c:if test="${app.canUnSubscribe}">
                                            <i class="icon-ban-circle"></i>
                                        </c:if>
                                </appAction>
                                </c:if>
                                <div class="appstore-app-description-3 hidden-sm hidden-xs">
                                    <c:out value="${app.description}"/>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href='view?appId=<c:out value="${app.id}"/>'>
                        <div class="col-md-3 col-sm-4 col-xs-6 app-tile hidden-lg hidden-md hidden-sm visible-xs">
                            <div class="appstore-app container">
                                <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                     class="appstore-app-icon img-thumbnail"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                        <%--application rating begin--%>
                                        <c:if test="${app.rating ge 0}">

                                            <%--limit rating--%>
                                            <c:if test="${app.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${app.rating le 5}">
                                                <c:set var="end" value="${app.rating}"/>
                                            </c:if>

                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (end lt 5)}">
                                            <c:forEach begin="1" end="${5 - end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <%--application rating end--%>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><fmt:message code="appstore.app.developer.by"/> <c:out
                                            value="${app.developer}"/></span>

                                    <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='
                                <c:if test="${app.canDownload}">
                                    download
                                </c:if>
                                <c:if test="${app.canSubscribe}">
                                    subscribe
                                </c:if>
                                <c:if test="${app.canUnSubscribe}">
                                    unSub
                                </c:if>'>
                                    <span class="appstore-app-action-icon vdf-button">
                                       <c:if test="${app.canDownload && !app.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                       </c:if>
                                        <c:if test="${app.canSubscribe}">
                                            <i class="icon-rss"></i>
                                        </c:if>
                                        <c:if test="${app.canUnSubscribe}">
                                            <i class="icon-ban-circle"></i>
                                        </c:if>
                                    </span>
                                        </appAction>
                                    </c:if>
                                </span>
                            </span>
                            </div>
                        </div>
                    </a>
                </c:if>

                <c:if test="${(count gt 1 && count lt 11) || (count gt 11)}">
                    <a href='view?appId=<c:out value="${app.id}"/>'>
                        <div class="col-md-3 col-sm-4 col-xs-6 app-tile">
                            <div class="appstore-app container">
                                <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                     class="appstore-app-icon img-thumbnail"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                        <%--application rating begin--%>
                                        <c:if test="${app.rating ge 0}">
                                            <%--limit rating--%>
                                            <c:if test="${app.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${app.rating le 5}">
                                                <c:set var="end" value="${app.rating}"/>
                                            </c:if>

                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (end lt 5)}">
                                            <c:forEach begin="1" end="${5 - end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <%--application rating end--%>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><fmt:message code="appstore.app.developer.by"/> <c:out
                                            value="${app.developer}"/></span>

                                    <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                        <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${app.name}"/>'
                                            app-id='<c:out value="${app.id}"/>'
                                            app-charging='<c:out value="${app.chargingLabel}"/>'
                                            app-type='
                                <c:if test="${app.canDownload}">
                                    download
                                </c:if>
                                <c:if test="${app.canSubscribe}">
                                    subscribe
                                </c:if>
                                <c:if test="${app.canUnSubscribe}">
                                    unSub
                                </c:if>'>
                                    <span class="appstore-app-action-icon vdf-button">
                                       <c:if test="${app.canDownload && !app.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                       </c:if>
                                        <c:if test="${app.canSubscribe}">
                                            <i class="icon-rss"></i>
                                        </c:if>
                                        <c:if test="${app.canUnSubscribe}">
                                            <i class="icon-ban-circle"></i>
                                        </c:if>
                                    </span>
                                </appAction>
                                    </c:if>
                                </span>
                            </span>
                            </div>
                        </div>
                    </a>
                </c:if>

                <c:if test="${count eq 16}">
                    <c:set var="count" value="0"/>
                </c:if>

                <c:set var="count" value="${count+1}"/>
            </c:forEach>
        </c:if>
    </div>
</div>
    <%--Page content ends--%>
    <div align="center">
        <div class="pagination">

        </div>
    </div>

    <script type="text/javascript">

        var pageSize = parseInt('<c:out value="${appsPerPage}"/>');
        var categoryKey = '<c:out value="${param.category}"/>';
        <%--var searchKey = '<c:out value="${param.searchKey}"/>';--%>

        var options = {
            currentPage:<c:out value="${active+1}"/>,
            totalPages:<c:out value="${fn:length(pagination)}"/>,
            useBootstrapTooltip: true,
            numberOfPages:<c:out value="${numberOfPages}"/>,
            pageUrl: function (type, page, current) {
                return ("?category="+categoryKey+"&skip=" + ((page - 1) * pageSize) + "&limit=" + pageSize);
            }
        };

        $(document).ready(function(){
            $(".pagination").bootstrapPaginator(options);
        });

    </script>

<%--complete pagination plugin integration--%>
<%--<%@include file="../decorators/paginator.jsp"%>--%>

</body>
</html>