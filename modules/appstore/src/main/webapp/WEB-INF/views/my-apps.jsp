<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title><c:out value="${userName}"/> <fmt:message code="appstore.main.title"/></title>
    </head>

    <body>

    <div class="row">
        <c:if test="${fn:length(appBanners) gt 0}">
            <%--app banner--%>
            <div id="myCarousel" class="carousel slide">
                <ol class="carousel-indicators">
                    <c:set value="0" var="count"/>
                    <c:forEach items="${appBanners}" var="b">
                        <li data-target="#myCarousel" data-slide-to='<c:out value="${count}"/>' class='<c:if test="${count==0}">active</c:if>'></li>
                        <c:set value="${count+1}" var="count"/>
                    </c:forEach>
                </ol>
                <div class="carousel-inner">
                    <c:set value="1" var="count"/>
                    <c:forEach items="${appBanners}" var="banner">
                        <div class='item <c:if test="${count eq 1}">active</c:if> '>
                            <a href="${banner.link}" target="_blank">
                                <img src="${baseUrl}${banner.image_url}" width="100%"/>
                            </a>
                        </div>
                        <c:set value="${count+1}" var="count"/>
                    </c:forEach>
                </div>
            </div>
        </c:if>
    </div>

    <%--<br/>--%>

    <div class="row">

        <%--My apps details--%>
        <div class="row col-md-9 padding-left-right-0">

            <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
                <div class="col-md-12 padding-left-right-0 vdf-app-panel-title app-panel-title"><fmt:message code="appstore.myapp"/></div>
            </div>

            <div class="row background-white">
            <div class="row profile-row vdf-gray-font">
                <!-- Nav tabs -->
                <ul class="nav nav-pills">
                    <li class="active"><a href="#appstoreSubscribes" data-toggle="tab"><i class="icon-rss"></i>
                        <fmt:message code="appstore.subscribes.label"/></a>
                    </li>
                    <li>
                        <a href="#appstoreDownloads" data-toggle="tab">
                            <i class="icon-download-alt"></i> <fmt:message code="appstore.downloads.label"/></a>
                    </li>
                    <li>
                        <a href="#appstoreUpdates" data-toggle="tab">
                            <i class="icon-refresh"></i> <fmt:message code="appstore.updates.label"/>
                        </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane" id="appstoreDownloads">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.number.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.name.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.status.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.requested.date.column.header"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:set var="count" value="1"/>
                            <c:forEach items="${dwnAppList}" var="dwnApp">
                                <tr>
                                    <td><c:out value="${count}"/></td>
                                    <td><a class="branding-font-color" href='view?appId=<c:out value="${dwnApp.id}"/>'>
                                        <c:out value="${dwnApp.name}"/></a></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${ dwnApp.downloadStatus.status eq 'url-expired' }">
                                                <span class="label label-danger"><c:out value="${dwnApp.downloadStatus.status}"/></span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="label label-success"><c:out value="${dwnApp.downloadStatus.status}"/></span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td><joda:format value="${dwnApp.downloadStatus.requestedDate}"
                                                     pattern="MMM dd YYYY"/></td>
                                </tr>
                                <c:set value="${count+1}" var="count"/>
                            </c:forEach>
                            </tbody>
                        </table>
                        <c:if test="${fn:length(dwnAppList) eq 0}">
                            <h4 class="alert alert-warning"><fmt:message code="appstore.no.download.apps"/></h4>
                        </c:if>
                    </div>

                    <div class="tab-pane active" id="appstoreSubscribes">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.number.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.name.column.header"/></th>
                                <%--<th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.status.column.header"/></th>--%>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.requested.date.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.unsubscribe"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:set var="count" value="1"/>
                            <c:forEach items="${subAppList}" var="app">
                                <tr>
                                    <td><c:out value="${count}"/></td>
                                    <td><a class="branding-font-color" href='view?appId=<c:out value="${app.id}"/>'>
                                        <c:out value="${app.name}"/> </a></td>
                                        <%--<td><c:out value="${app.subscriptionStatus}"/></td>--%>
                                    <td><format:formatDate value="${app.requestedDate}" pattern="MMM dd YYYY"/></td>
                                    <td>
                                        <div class="unSUbButton"
                                             href=''
                                             data-toggle='modal'
                                             app-name='<c:out value="${app.name}"/>'
                                             app-id='<c:out value="${app.id}"/>'

                                             data-target='#appStoreModal'>
                                            <i class="icon-ban-circle" style="cursor: pointer;color: #d9534f;"></i>
                                        </div>
                                    </td>
                                </tr>
                                <c:set value="${count+1}" var="count"/>
                            </c:forEach>
                            </tbody>
                        </table>
                        <c:if test="${fn:length(subAppList) eq 0}">
                            <h4 class="alert alert-warning"><fmt:message code="appstore.no.subscribe.apps"/></h4>
                        </c:if>

                    </div>

                    <div class="tab-pane" id="appstoreUpdates">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.number.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.name.column.header"/></th>
                                <th><fmt:message
                                        code="appstore.app.user.profile.apps.table.application.update.column.header"/></th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:set var="count" value="1"/>
                            <c:forEach items="${updateList}" var="app">
                                <tr>
                                    <td><c:out value="${count}"/></td>
                                    <td><a class="branding-font-color" href='view?appId=<c:out value="${app.id}"/>'>
                                        <c:out value="${app.name}"/> </a></td>
                                    <td>
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='download'>

                                                <span style="cursor: pointer; padding-left: 20px;">
                                                        <i class="icon-download-alt"></i>
                                                </span>
                                        </appAction>
                                    </td>
                                </tr>
                                <c:set value="${count+1}" var="count"/>
                            </c:forEach>
                            </tbody>
                        </table>
                        <c:if test="${fn:length(updateList) eq 0}">
                            <h4 class="alert alert-warning"><fmt:message code="appstore.no.app.updates"/></h4>
                        </c:if>
                    </div>
                </div>
            </div>
            </div>
        </div>

        <%--Popular apps--%>
        <div class="row col-md-3 padding-left-right-0">

            <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
                <div class="col-md-8 padding-left-5 padding-right-0 vdf-app-panel-title app-panel-title">
                    <fmt:message code="appstore.main.navigation.popular.apps"/>
                </div>
                <div class="col-md-4 popular-app-view-all">
                    <a href="popular-apps" class="btn btn-danger btn-md padding-1-3"><fmt:message code="appstore.app.panel.view.all.text"/></a>
                </div>
            </div>

            <c:set var="count" value="0"/>
            <c:forEach items="${popularApps}" var="app">
                <c:if test="${count % 2 eq 0}">
                    <div class="row col-md-12 padding-left-right-0">
                </c:if>
                <div class="col-md-6 app-detail-tile">
                    <a href='view?appId=<c:out value="${app.id}"/>'>
                        <div class="row app-detail-tile-inner">
                            <div class="row">
                                <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>' width="100%" class="margin-bottom-5"/>
                            </div>
                            <div class="row app-detail-tile-text">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${app.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>

                                <br/>

                                <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${app.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${app.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${app.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${app.canDownload && !app.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                                <c:if test="${ !app.canSubscribe && !app.canDownload && !app.canUnSubscribe}">
                                    <div class="row app-tile-button" style="background-color: #ffffff">
                                        &nbsp;
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </a>
                </div>
                <c:if test="${count % 2 eq 1}">
                    </div>
                </c:if>
                <c:set value="${count+1}" var="count"/>
            </c:forEach>
        </div>

    </div>
    </body>
</html>

