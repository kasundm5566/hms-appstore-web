<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><fmt:message code="appstore.home.page.sub.title"/> <fmt:message code="appstore.main.title"/></title>
</head>
<body>
<%--Page content begins--%>
<div class="row">
    <%--SLIDE SHOW GOES HERE--%>
    <%@ include file="../decorators/appstore-slide-show.jsp" %>
</div>

<%--pannel top apps--%>
<div class="row">

    <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
        <div class="col-md-10 padding-left-right-0 vdf-app-panel-title app-panel-title">${panelTopTitle}</div>
        <div class="col-md-2 padding-left-right-0  app-title-view-all">
            <a href="${viewAllTop}" class="btn btn-danger btn-md padding-1-3" style="font-size: 16px;">
                <fmt:message code="appstore.app.panel.view.all.text"/>
            </a>
        </div>
    </div>

    <div class="appstore-apps add-lr-margin">
        <c:forEach items="${panelTopApps}" var="topApp">

            <a href='view?appId=<c:out value="${topApp.id}"/>'>
                <div class="col-md-2 col-sm-4 col-xs-6 app-tile">
                    <div class="appstore-app container">
                        <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${topApp.appIcon}"/>'
                             class="appstore-app-icon"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${topApp.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${topApp.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${topApp.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>
                                <br/>
                                <c:if test="${topApp.canSubscribe||topApp.canDownload||topApp.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${topApp.name}"/>'
                                                app-id='<c:out value="${topApp.id}"/>'
                                                app-charging='<c:out value="${topApp.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${topApp.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${topApp.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${topApp.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${topApp.canDownload && !topApp.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${topApp.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${topApp.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                            </span>
                    </div>
                </div>
            </a>

        </c:forEach>
    </div>

</div>

<%--pannel middle apps--%>
<div class="row">

    <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
        <div class="col-md-10 padding-left-right-0 vdf-app-panel-title app-panel-title">${panelMiddleTitle}</div>
        <div class="col-md-2 padding-left-right-0  app-title-view-all">
            <a href="${viewAllMiddle}" class="btn btn-danger btn-md padding-1-3" style="font-size: 16px;">
                <fmt:message code="appstore.app.panel.view.all.text"/>
            </a>
        </div>
    </div>

    <div class="appstore-apps add-lr-margin">
        <c:forEach items="${panelMiddleApps}" var="topApp">
            <a href='view?appId=<c:out value="${topApp.id}"/>'>
                <div class="col-md-2 col-sm-4 col-xs-6 app-tile">
                    <div class="appstore-app container">
                        <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${topApp.appIcon}"/>'
                             class="appstore-app-icon"/>
                        <span class="appstore-app-name">
                            <span class="appstore-ellipsis"><c:out value="${topApp.name}"/></span>
                            <br/>
                            <span class="appstore-app-owner">
                                <span class="appstore-app-owner-name">
                                    <c:out value="${topApp.developer}"/>
                                </span>
                                <span class="pull-right">
                                    <format:formatNumber type="number"  groupingUsed="false" value="${topApp.rating}" />
                                    <i class="icon-star rating-color"></i>
                                </span>
                            </span>
                            <br/>
                            <c:if test="${topApp.canSubscribe||topApp.canDownload||topApp.canUnSubscribe}">
                                <div class="row app-tile-button">
                                    <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${topApp.name}"/>'
                                            app-id='<c:out value="${topApp.id}"/>'
                                            app-charging='<c:out value="${topApp.chargingLabel}"/>'
                                            app-type='
                                                <c:if test="${topApp.canDownload}">
                                                    download
                                                </c:if>
                                                <c:if test="${topApp.canSubscribe}">
                                                    subscribe
                                                </c:if>
                                                <c:if test="${topApp.canUnSubscribe}">
                                                    unSub
                                                </c:if>'>
                                        <span class="app-tile-text">
                                           <c:if test="${topApp.canDownload && !topApp.canSubscribe}">
                                               <i class="icon-download-alt"></i>
                                               DOWNLOAD
                                           </c:if>
                                            <c:if test="${topApp.canSubscribe}">
                                                <i class="icon-rss"></i>
                                                SUBSCRIBE
                                            </c:if>
                                            <c:if test="${topApp.canUnSubscribe}">
                                                <i class="icon-ban-circle"></i>
                                                UN-SUBSCRIBE
                                            </c:if>
                                        </span>
                                    </appAction>
                                </div>
                            </c:if>
                        </span>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>

</div>

<%--pannel bottom apps--%>
<div class="row">

    <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
        <div class="col-md-10 padding-left-right-0 vdf-app-panel-title app-panel-title">${panelBottomTitle}</div>
        <div class="col-md-2 padding-left-right-0  app-title-view-all">
            <a href="${viewAllBottom}" class="btn btn-danger btn-md padding-1-3" style="font-size: 16px;">
                <fmt:message code="appstore.app.panel.view.all.text"/>
            </a>
        </div>
    </div>

    <div class="appstore-apps add-lr-margin">
        <c:forEach items="${panelBottomApps}" var="newApp">
            <a href='view?appId=<c:out value="${newApp.id}"/>'>
                <div class="col-md-2 col-sm-4 col-xs-6 app-tile">
                    <div class="appstore-app container">
                        <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${newApp.appIcon}"/>'
                             class="appstore-app-icon"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${newApp.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${newApp.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${newApp.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>
                                <br/>
                                <c:if test="${newApp.canSubscribe||newApp.canDownload||newApp.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${newApp.name}"/>'
                                                app-id='<c:out value="${newApp.id}"/>'
                                                app-charging='<c:out value="${newApp.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${newApp.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${newApp.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${newApp.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${newApp.canDownload && !newApp.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${newApp.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${newApp.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                            </span>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>

</div>
<%--Page content ends--%>
</body>
</html>