<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title><fmt:message code="appstore.search.page.sub.title"/> <fmt:message code="appstore.main.title"/></title>
    </head>

    <body>
    <div class="row">

        <div class="row app-panel-title-area vdf-app-panel-header" style="margin-top: 0px">
            <span class="vdf-app-panel-title app-panel-title">
                <fmt:message code="appstore.app.search.result.for"/>
                <c:out value="${searchKey}"/>
            </span>
        </div>

        <div class="appstore-apps add-lr-margin">
            <c:if test="${fn:length(searchResult) eq 0}">
                <h4><p class="alert-warning">
                    <fmt:message code="appstore.app.search.no.result.found.notice"/>  <i><c:out value="${searchKey}"/></i>
                </p></h4>
            </c:if>
            <%--<c:forEach items="${searchResult}" var="sApp">
                <a href='view?appId=<c:out value="${sApp.id}"/>'>
                    <div class="col-md-3 col-sm-6 col-xs-6 app-tile">
                        <div class="appstore-app container">
                            <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${sApp.appIcon}"/>'
                                 class="appstore-app-icon img-thumbnail"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${sApp.name}"/></span>
                                <br/>
                                        &lt;%&ndash;application rating begin&ndash;%&gt;
                                        <c:if test="${sApp.rating ge 0}">
                                            &lt;%&ndash;limit rating&ndash;%&gt;
                                            <c:if test="${sApp.rating gt 5}">
                                                <c:set var="end" value="5"/>
                                            </c:if>
                                            <c:if test="${sApp.rating le 5}">
                                                <c:set var="end" value="${sApp.rating}"/>
                                            </c:if>

                                            <c:forEach begin="1" end="${end}">
                                                <i class="icon-star rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${(end ge 1) && (end lt 5)}">
                                            <c:forEach begin="1" end="${5-end}">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        <c:if test="${end eq 0}">
                                            <c:forEach begin="1" end="5">
                                                <i class="icon-star-empty rating-color"></i>
                                            </c:forEach>
                                        </c:if>
                                        &lt;%&ndash;application rating end&ndash;%&gt;
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name"><c:out value="${sApp.developer}"/></span>
                                <c:if test="${sApp.canDownload||sApp.canSubscribe||sApp.canUnSubscribe}">
                                    <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${sApp.name}"/>'
                                            app-id='<c:out value="${sApp.id}"/>'
                                            app-charging='<c:out value="${sApp.chargingLabel}"/>'
                                            app-type='<c:if test="${sApp.canSubscribe}">subscribe</c:if>
                                                            <c:if test="${sApp.canDownload}">download</c:if>
                                                            <c:if test="${appDetails.canUnSubscribe}">unSub</c:if>'>
                                          <span class="appstore-app-action-icon vdf-button branding-background-hover">
                                           <c:if test="${sApp.canDownload && !sApp.canSubscribe}">
                                               <i class="icon-download-alt"></i>
                                           </c:if>
                                          <c:if test="${sApp.canSubscribe && !sApp.canDownload}">
                                              <i class="icon-rss"></i>
                                          </c:if>
                                          <c:if test="${sApp.canUnSubscribe}">
                                              <i class="icon-ban-circle"></i>
                                          </c:if>
                                           </span>
                                    </appAction>
                                </c:if>
                                </span>
                            </span>
                        </div>
                    </div>
                </a>
            </c:forEach>--%>
            <c:forEach items="${searchResult}" var="app">

                <a href='view?appId=<c:out value="${app.id}"/>'>
                    <div class="col-md-2 col-sm-4 col-xs-6 app-tile">
                        <div class="appstore-app container">
                            <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                 class="appstore-app-icon"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${app.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>

                                <br/>

                                <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${app.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${app.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${app.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${app.canDownload && !app.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                            </span>
                        </div>
                    </div>
                </a>

            </c:forEach>
        </div>
    </div>

    <br/>

    <div align="center">
        <div class="pagination">

        </div>
    </div>

    <script type="text/javascript">

        var pageSize = parseInt('<c:out value="${appsPerPage}"/>');
        var searchKey = '<c:out value="${param.searchKey}"/>';

        var options = {
            currentPage:<c:out value="${active+1}"/>,
            totalPages:<c:out value="${fn:length(pagination)}"/>,
            useBootstrapTooltip: true,
            numberOfPages:<c:out value="${numberOfPages}"/>,
            pageUrl: function (type, page, current) {
                return ("?searchKey=" + searchKey + "&skip=" + ((page - 1) * pageSize) + "&limit=" + pageSize);
            }
        };

        $(document).ready(function () {
            $(".pagination").bootstrapPaginator(options);
        });

    </script>

    <%--Page content ends--%>
    </body>
    </html>