<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <div class="modal-header vdf-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style" id="subscribeModalLabel">
            <i class="icon-download"></i>
            <fmt:message code="appstore.download.button.title"/>
        </h4>
    </div>
    <div class="modal-body" id="paymentSelectionNotice">
        <form role="form" method="post" action="downloadCharged" id="downloadCharged">

            <div class="form-group" id="downloadPaymentBlock">
                <label for="selectPaymentInstrument"><fmt:message code="appstore.app.payment.instrument"/> </label>

                <select class="form-control" id='selectPaymentInstrument' name="pInstrument" required>
                    <c:forEach var="pIns" items="${pInstruments}">
                                <option value="${pIns}"><c:out value="${pIns}"/></option>
                    </c:forEach>
                </select>
            </div>

            <div class="form-group hidden" id="pinblock">
                <label for="pin">Pin</label>
                <input type="password" name="pin" id="pin" class="form-control" maxlength="4"/>
            </div>

                <%----%>
            <input type="hidden" class="hidden" value="${downReqId}" name="downReqId"/>
            <button type="submit" class="btn vdf-button branding-background-hover">
                <fmt:message    code="appstore.ok.button.title"/></button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><fmt:message code="appstore.cancel.button.title"/></button>

        </form>
    </div>
    <script type="text/javascript">

        $("#selectPaymentInstrument").on('change',function(e){
            console.log("value changes :")
           var value=$(this).val();
            console.info(value)
            if(value.indexOf('PAiSA')>-1){
                $("#pinblock").removeClass("hidden");
                $("#pinblock input").attr("required","required");
            }else{
                $("#pinblock").addClass("hidden");
                $("#pinblock input").removeAttr("required");
            }
        });

        $("#downloadCharged").validate({
            rules: {
                pin: {
                    required: true,
                    maxlength: 4,
                    minlength:4,
                    number:true
                }
            }
        });

        $("#downloadCharged").submit(function (e) {
            e.preventDefault();

            $("#downloadCharged").validate();
            var valid=$("#downloadCharged").valid();
            if(!valid){
                return false;
            }
            var parameters = $("#downloadCharged").serialize();

            console.log(parameters);

            $.post("downloadCharged", parameters, function (data) {
                $("#paymentSelectionNotice").html(data+
                 '<br/><br/><button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            });
        });
    </script>
