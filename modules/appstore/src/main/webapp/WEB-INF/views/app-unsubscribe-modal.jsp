<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="modal-header vdf-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style text-center" id="subscribeModalLabel"><i class="icon-rss"></i>
            <fmt:message code="appstore.unsubscribe.modal.title"/> <span id='subscribingAppName'> <c:out value="${appName}"/>
            </span>
        </h4>
    </div>
    <div class="modal-body">
        <p class="text-center" id='subscribePaymentNotice'><fmt:message code="appstore.unsubscribe.pre.confirm.message"/> <c:out value="${appName}"/>.</p>

        <form role="form" action="unSubscribeNow" method="post" id="unSubscribeNow">
                <%--Add application Id here--%>
            <input type="text" class="hidden" value='<c:out value="${appId}"/>' name="appId"/>

            <div class="col-md-12 popup-btn">
                <button type="submit" class="btn btn-default" data-dismiss="modal">
                    <fmt:message code="appstore.cancel.button.title"/>
                </button>
                <button type="submit" class="btn vdf-button branding-background-hover">
                    <fmt:message code="appstore.yes.button.title"/>
                </button>
            </div>

        </form>
    </div>

<script type="text/javascript">

    $("#unSubscribeNow").submit(function (e) {

        e.preventDefault();
        var parameters = $("#unSubscribeNow").serialize();
//        console.log(parameters);

        $.post("unSubscribeNow", parameters, function (data) {

            console.info("subscribe response");
            var response=data.toString().trim().split("|");
            var statusCode =response[0];
            var stateDesc=response[1];
            console.log("----------------------a"+statusCode);
            loadModalContent('unSubSuccess?sCode='+statusCode+'&msg='+stateDesc);

        });
    });

</script>