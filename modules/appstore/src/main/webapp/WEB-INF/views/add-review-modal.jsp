<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <form class="form-horizontal" role="form" method="POST" action="/addComment" id="addReview">
        <div class="modal-header vdf-modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title branding-font-style text-center" id="reviewModalLabel">
                <i class="icon-comments-alt"></i>
                <fmt:message code="appstore.app.app.view.page.add.reviews.title"/>
                <span id='reviewingAppName'></span>
            </h4>
        </div>

        <div class="modal-body">
            <div class="form-group add-review-form-group">
                <div class="col-md-12 row">
                    <label for="abused">Comment type :</label>
                    <select name="abused" id="abused" class="form-control">
                        <option value="false" selected="selected">User Comment</option>
                        <option value="true">Report abuse</option>
                    </select>
                </div>
            </div>
            <div class="form-group add-review-form-group">
                <div class="col-md-12">
                    <textarea class="form-control" rows="7" required="" name="comments"></textarea>
                    <span>
                        <div class="hidden" id="alertDiv">
                            <span id="responseMessage" class="text-danger"></span>
                        </div>
                    </span>
                </div>
            </div>

            <input type="hidden" name="appId" value='<c:out value="${appId}"/>'/>

            <div class="col-md-12 popup-btn">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <fmt:message code="appstore.cancel.button.title"/>
                </button>
                <button type="submit" class="btn vdf-button branding-background-hover">
                    &nbsp;&nbsp;<fmt:message code="appstore.add.button.title"/>&nbsp;&nbsp;
                </button>
            </div>

        </div>
    </form>

<script type="text/javascript">

    $("#addReview").submit(function (e) {

        e.preventDefault();

        var data = $("#addReview").serialize();

        console.log(data);
        var url = "addComment?" + data;

        $.ajax({
            url: url,
            type: "POST",
            data: data,
            contentType: "application/json",
            success: function (data) {
                $("#responseMessage").text(data);
                $("#alertDiv").removeClass("hidden");
                setTimeout(closeRefresh,2000);
            }
        });
    });

</script>