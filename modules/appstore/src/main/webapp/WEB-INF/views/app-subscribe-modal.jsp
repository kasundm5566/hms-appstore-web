<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

    <div class="modal-header vdf-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style text-center" id="subscribeModalLabel">
            <i class="icon-rss"></i>
            <fmt:message code="appstore.subscribe.modal.title"/>
        </h4>
    </div>
    <div class="modal-body">

        <c:if test="${(app.canUnSubscribe)}">
            <p class="vdf-gray-font text-center"><fmt:message code="appstore.subscribed.message"/>
                <c:out value="${app.name}"/>
            </p>
        </c:if>
        <c:if test="${app.canSubscribe}">
            <p class="text-center" id='subscribePaymentNotice'><c:out value='${fn:replace(chargingNotice,"+tax" ,"") }'/></p>

            <form role="form" action="subscribeNow" method="post" id="subscribeNow">
                    <%--Add application Id here--%>
                <input type="text" class="hidden" value='<c:out value="${appId}"/>' name="appId"/>
                    <%----%>
                <c:if test="${isCharged}">
                    <div class="form-group" id="subscribePaymentBlock">
                        <label for="selectSubscribePaymentType"><fmt:message
                                code="appstore.app.payment.instrument"/></label>
                        <select class="form-control" id='selectSubscribePaymentType' name="payment_method" disabled="disabled">
                            <option selected='selected'>mobile</option>
                                <%--<option>Dialog easy cash</option>--%>
                        </select>
                    </div>

                </c:if>

                <div class="col-md-12 popup-btn">
                    <button type="submit" class="btn vdf-button branding-background-hover">
                        <fmt:message code="appstore.subscribe.button.title"/>
                    </button>
                    <button type="submit" class="btn btn-default" data-dismiss="modal">
                        <fmt:message code="appstore.cancel.button.title"/>
                    </button>
                </div>

            </form>
        </c:if>
    </div>

<script type="text/javascript">

    $("#subscribeNow").submit(function (e) {

        e.preventDefault();
        var parameters = $("#subscribeNow").serialize();
        console.log(parameters);

        $.post("subscribeNow", parameters, function (data) {

            console.info("subscribe response");
            var response=data.toString().trim().split("|");
            var statusCode =response[0];
            var stateDesc=response[1];
            console.log("----------------------" + statusCode);
            loadModalContent('subSuccess?sCode=' + statusCode+"&msg="+stateDesc);

        });
    });

</script>