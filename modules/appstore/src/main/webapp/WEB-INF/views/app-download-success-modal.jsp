<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <div class="modal-header vdf-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style text-center" id="subscribeModalLabel">
            <i class="icon-download"></i>
            <fmt:message code="appstore.download.button.title"/>
        </h4>
    </div>

    <div class="modal-body">


                <%--Add application Id here--%>
            <c:if test="${status=='S1000'}">
                <p class="text-center" style="font-family: sans-serif;"><fmt:message code="appstore.download.success.message"/></p>
            </c:if>
            <c:if test="${status!='S1000'}">
                <p class="text-center" style="font-family: sans-serif;"><c:out value="${message}"/></p>
            </c:if>

        <div class="col-md-12 popup-btn">
            <button type="close" class="btn vdf-button branding-background-hover" data-dismiss="modal">
                <fmt:message code="appstore.ok.button.title"/>
            </button>
        </div>

    </div>