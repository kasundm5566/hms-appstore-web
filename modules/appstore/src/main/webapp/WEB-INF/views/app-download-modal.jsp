<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

    <div class="modal-header vdf-modal-header ">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style text-center" id="DownloadModalLabel"><i class="icon-download-alt"></i>
            <fmt:message code="appstore.download.modal.title"/>
            <span id='downloadableAppName'> <c:out value="${app.name}"/> </span></h4>
    </div>
    <div class="modal-body">
        <p id='downloadPaymentNotice'><c:out value='${fn:replace(chargingNotice,"+tax" ,"") }'/></p>

        <form role="form" method="post" action="downloadNow" id="downloadNow">
            <div class="form-group">
                <label for="selectDownloadType"><fmt:message
                        code="appstore.app.download.form.search.download.by"/> </label>
                <select class="form-control" id='selectDownloadType' disabled="disabled">
                    <option selected="selected">Platform</option>
                    <option>Device</option>
                </select>
            </div>
            <div class="form-group">
                <label for="selectDeviceMake"><fmt:message
                        code="appstore.app.download.form.select.device.platform"/></label>
                <select class="form-control" id='selectDeviceMake' name="platform" required onchange="enableDownloadBtn()">
                    <option selected="selected"></option>
                    <c:forEach var="item" items="${operatingSystems}">
                        <option value="${item.key}">
                                ${item.key}<c:if test="${item.value == true}"> - Update available</c:if>
                        </option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="selectDeviceModal"><fmt:message
                        code="appstore.app.download.form.select.platform.version"/> </label>
                <select class="form-control" id='selectDeviceModal' name="version" required onchange="enableDownloadBtn()">
                    <option selected="selected"></option>

                    <c:forEach var="item" items="${versions}">
                        <option value="${item.key}" data-updated="${item.value}"> ${item.key} </option>
                    </c:forEach>
                </select>
            </div>

            <input type="hidden" class="hidden" value='<c:out value="${app.id}" />' name="appId"/>

            <div class="col-md-12 popup-btn">
                <button type="close" class="btn btn-default" data-dismiss="modal"><fmt:message
                        code="appstore.cancel.button.title"/></button>
                <button disabled="true" id="downloadBtn" type="submit" class="btn vdf-button  branding-background-hover"><fmt:message
                        code="appstore.download.button.title"/>
                </button>
            </div>

        </form>
    </div>

<script type="text/javascript">

    function enableDownloadBtn() {
        var make = document.getElementById('selectDeviceMake').selectedIndex;
        var model = document.getElementById('selectDeviceModal').selectedIndex;
        if(make != 0 && model != 0) {
            document.getElementById('downloadBtn').disabled=false;
        } else {
            document.getElementById('downloadBtn').disabled=true;
        }
    }


    $("#downloadNow").submit(function (e) {
        e.preventDefault();
        $("#downloadNow").validate();
        var parameters = $("#downloadNow").serialize();
        $.post("downloadNow", parameters, function (response) {
            console.info("download response" + response);
            var statusCode = response['statusCode'];
            var appId = response['appId'];
            var contentId = response['contentId'];
            var downReqId = response['downloadRequestId'];
            var stateDesc = response['downloadStateDesc'];

            loadModalContent(
                            'downUrl?statusCode='
                            + statusCode
                            + "&appId="
                            + appId
                            + "&contentId="
                            + contentId
                            + "&dwnReqId="
                            + downReqId
                            + "&stateDesc="
                            + stateDesc);
        });
    });

</script>