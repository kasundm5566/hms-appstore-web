<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:set var="baseURL" value="${pageContext.request.localName}"/>
    <%--Social Network Integration Meta Tags--%>
    <!-- for Google -->
    <meta name="description" content="<c:out value="${appDetails.description}"/>"/>
    <meta name="keywords" content="<c:out value="${appDetails.name}"/>"/>

    <meta name="author" content=""/>
    <meta name="copyright" content=""/>
    <meta name="application-name" content="<c:out value="${appDetails.name}"/>"/>

    <meta itemprop="name" content="<c:out value="${appDetails.name}"/>">
    <meta itemprop="description" content="<c:out value="${appDetails.description}"/>">
    <meta itemprop="image"
          content="<fmt:message code="appstore.app.icon.base.url"/><c:out value="${appDetails.appIcon}" />">
    <meta property="og:image" content="<fmt:message code="appstore.app.icon.base.url"/><c:out value="${appDetails.appIcon}"/>" />

    <!-- for Twitter -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="<c:out value="${appDetails.name}"/>"/>
    <meta name="twitter:description" content="<c:out value="${appDetails.description}"/>"/>
    <meta name="twitter:image"
          content="<fmt:message code="appstore.app.icon.base.url"/><c:out value="${appDetails.appIcon}" />"/>
    <%--Social Network Integration Meta Tags End--%>

    <title><c:out value="${appDetails.name}"/> <fmt:message code="appstore.main.title"/></title>
</head>

<body>
<div id="fb-root"></div>

<br/>

<div class="row">
    <%--app details section--%>
    <div class="row col-md-9 padding-left-right-0 background-white">

        <div class="row col-md-12">
            <%--app icon--%>
            <div class="col-md-3 padding-left-right-0">
                <div style="margin: 10px;">
                    <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${appDetails.appIcon}" />'
                         width="100%"/>
                </div>
            </div>

            <%--app description--%>
            <div class="col-md-6 padding-left-right-0">
                <div class="margin-15-0">
                    <%--<span class="font-large-bold"><c:out value="${appDetails.name}"/></span>--%>
                    <div class="col-md-8 padding-left-0 padding-right-0 vdf-app-panel-title app-panel-title">
                        ${appDetails.name}
                    </div>
                    <br/>
                    <span class="appstore-app-view-app-dev-name"><c:out value="${appDetails.category}"/></span>
                    <br/>
                <span class="appstore-app-view-app-dev-name">
                    <fmt:message code="appstore.app.developer.by"/>
                    <a href='developer-profile?developerName=<c:out value="${appDetails.developer}"/>'>
                        <c:out value="${appDetails.developer}"/>
                    </a>
                </span>
                    <br/>
                <span class="appstore-app-view-app-dev-name"> <format:formatNumber type="number"  groupingUsed="false" value="${appDetails.rating}" /> <i class="icon-star rating-color"></i></span>
                    <br/>
                    <c:if test="${appDetails.canDownload}">
                        <span class="appstore-app-view-app-dev-name"> ${appDetails.downloadsCount} <fmt:message code="appstore.downloads.label"/></span>
                    </c:if>
                    <c:if test="${appDetails.canSubscribe||appDetails.canUnSubscribe}">
                        <span class="appstore-app-view-app-dev-name"> ${appDetails.subscriptionsCount} <fmt:message code="appstore.subscribes.label"/></span>
                    </c:if>
                </div>
            </div>

            <div class="col-md-3 padding-left-right-0">
                <div class="margin-15-10">
                    <%--app price details--%>
                    <div class="appstore-app-view-price vdf-gray-font row">
                        <c:if test="${appDetails.canSubscribe||appDetails.canDownload||appDetails.canUnSubscribe}">
                            <c:if test="${appDetails.canSubscribe && !appDetails.canDownload}">
                                <c:out value='${fn:replace(appDetails.chargingLabel,"+tax" ,"" ) }'/> <%--LKR <sup>Daily</sup>--%>
                            </c:if>
                            <c:if test="${appDetails.canDownload && !appDetails.canSubscribe}">
                                <c:if test="${isUpdateAvailable == true}">
                                    <fmt:message code="appstore.update.free"/>
                                </c:if>
                                <c:if test="${isUpdateAvailable != true}">
                                    <c:out value='${fn:replace(appDetails.chargingLabel,"+tax" ,"" ) }'/> <%--LKR <sup>Daily</sup>--%>
                                </c:if>
                            </c:if>
                            <c:if test="${appDetails.canDownload && appDetails.canSubscribe}">
                                <c:out value='${fn:replace(appDetails.chargingLabel,"+tax" ,"" ) }'/> <%--LKR <sup>Daily</sup>--%>
                            </c:if>
                            <c:if test="${appDetails.canUnSubscribe && !appDetails.canDownload}">
                                <c:out value='${fn:replace(appDetails.chargingLabel,"+tax" ,"" ) }'/> <%--LKR <sup>Daily</sup>--%>
                            </c:if>
                        </c:if>
                    </div>

                    <%--social media--%>
                    <div class="app-view-action-row padding-top-5">
                        <a href="https://plus.google.com/share?url=<fmt:message code='appstore.app.icon.base.url'/>/view?appId=${appDetails.id}"
                           onclick="javascript:window.open(this.href, '',
                           'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                            <div class="app-view-social-icon">
                                <i class="icon-google-plus"></i>
                            </div>
                        </a>
                        <a href="https://twitter.com/share?url=<fmt:message code='appstore.app.icon.base.url'/>/view?appId=${appDetails.id}&text=To use <c:out value="${appDetails.name}"/>, visit"
                           target="_blank" data-lang="en"
                           onclick="javascript:window.open(this.href, '',
                           'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                            <div class="app-view-social-icon">
                                <i class="icon-twitter"></i>
                            </div>
                        </a>
                        <a id="fb_share"
                           fb_app_name="<c:out value="${appDetails.name}"/>"
                           fb_app_ins="By <c:out value='${appDetails.developer}'/> "
                           fb_app_des="<c:out value="${appDetails.description}"/>"
                           fb_app_url="<fmt:message code='appstore.app.icon.base.url'/>view?appId=<c:out value='${appDetails.id}'/> "
                           fb_app_icon="<fmt:message code="appstore.app.icon.base.url"/><c:out value="${appDetails.appIcon}" />">
                            <div class="app-view-social-icon">
                                <i class="icon-facebook"></i>
                            </div>
                        </a>
                    </div>

                    <%--download/subscribe button--%>
                    <div class="app-view-action-row padding-top-10" style="text-align:right;">
                        <c:if test="${appDetails.canSubscribe||appDetails.canDownload||appDetails.canUnSubscribe}">
                            <appAction href=''
                                       data-toggle='modal'
                                       id='appActionButton'
                                       app-name='<c:out value="${appDetails.name}"/>'
                                       app-id='<c:out value="${appDetails.id}"/>'
                                       app-charging='<c:out value="${appDetails.chargingLabel}"/>'
                                       data-target='#appStoreModal'
                                       app-type='<c:out value="${appType}"/>'>

                                <div class="appstore-app-view-action-button vdf-button branding-background-hover">
                                    <c:if test="${appDetails.canSubscribe && !appDetails.canDownload}">
                                        <i class="icon-rss add-r-margin"></i>
                                        <fmt:message code="appstore.subscribe.button.title"/>
                                    </c:if>
                                    <c:if test="${appDetails.canDownload && !appDetails.canSubscribe}">
                                        <i class="icon-download-alt add-r-margin"></i>
                                        <c:if test="${isUpdateAvailable == true}">
                                            <fmt:message code="appstore.update.button.title"/>
                                        </c:if>
                                        <c:if test="${isUpdateAvailable != true}">
                                            <fmt:message code="appstore.download.button.title"/>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${appDetails.canDownload && appDetails.canSubscribe}">
                                        <i class="icon-rss add-r-margin"></i>
                                        <fmt:message code="appstore.subscribe.button.title"/>
                                    </c:if>
                                    <c:if test="${appDetails.canUnSubscribe && !appDetails.canDownload}">
                                        <i class="icon-ban-circle add-r-margin"></i>
                                        <fmt:message code="appstore.unsub.button.title"/>
                                    </c:if>
                                </div>
                            </appAction>
                        </c:if>

                        <c:if test="${appDetails.subscriptionStatus eq 'PENDING CHARGE'}">
                            <span class="label label-green"><fmt:message code="appstore.charging.pending"/></span>
                        </c:if>
                    </div>

                </div>
            </div>
        </div>

        <hr/>

        <%--app description--%>
        <div class="row col-md-12">
            <div class="row col-md-12">
                <span class="text-muted font-large"><fmt:message code="appstore.app.app.view.page.app.description.label"/></span>
                <br/>
                <span><c:out value="${appDetails.description}"/></span>
            </div>
        </div>

        <hr/>

        <%--app instruction--%>
        <div class="row col-md-12">
            <div class="row col-md-12">
                <span class="text-muted font-large">
                    <fmt:message code="appstore.app.app.view.page.app.instruction.label"/>
                </span>
                <br/>
                <span>
                    <c:if test="${fn:length(appDetails.instructions)>1}">
                        <c:forEach items="${appDetails.instructions}" var="entry">
                            <c:out value="${entry.key}"/> : <c:out value="${entry.value}"/>
                        </c:forEach>
                    </c:if>
                <c:if test="${fn:length(appDetails.instructions)<=1}">
                    <c:forEach items="${appDetails.instructions}" var="entry">
                        <c:out value="${entry.value}"/>
                    </c:forEach>
                </c:if>
                </span>
            </div>
        </div>

        <hr/>

        <%--app cost--%>
        <div class="row col-md-12">
            <div class="row col-md-12">
                <span class="text-muted font-large">
                    <fmt:message code="appstore.app.app.view.page.app.cost.label"/>
                </span>
                <br/>
                <span>
                    <c:out value='${fn:replace(appDetails.chargingDetails,"+tax" ,"")}'/>
                </span>
            </div>
        </div>

        <hr/>

        <%--screen shots--%>
        <c:if test="${fn:length(appDetails.screenShots) ge 1}">
            <div class="row col-md-12">
                <div class="row col-md-12">
                    <span class="text-muted font-large">
                        <fmt:message code="appstore.app.app.view.page.app.screen.shots.label"/>
                    </span>
                    <br/>
                    <div class="row col-md-12 padding-left-right-0">
                        <c:set var="imgCount" value="1"/>

                        <c:forEach items="${appDetails.screenShots}" var="map">
                            <c:if test="${!fn:contains(map['url'],'mobile')}">
                                <c:set var="url" value="${map['url']}"/>
                                <div class="column">
                                    <img src="<fmt:message code='appstore.app.icon.base.url'/><c:out value="${url}"/>"
                                         onclick="openModal();currentSlide(${imgCount})" class="hover-shadow">
                                </div>
                                <c:set var="imgCount" value="${imgCount+1}"/>
                            </c:if>
                        </c:forEach>

                        <!-- The Modal/Lightbox -->
                        <div id="myModal" class="image-view-modal">
                            <div class="image-view-modal-content">
                                <span class="closeModal" onclick="closeModal()">&times;</span>
                                <c:set var="imgViewCount" value="1"/>
                                <c:forEach items="${appDetails.screenShots}" var="map">
                                    <c:if test="${!fn:contains(map['url'],'mobile')}">
                                    <c:set var="url" value="${map['url']}"/>
                                    <div class="mySlides">
                                        <div class="numbertext">${imgViewCount} / ${imgCount}</div>
                                        <img src="<fmt:message code='appstore.app.icon.base.url'/><c:out value="${url}"/>" style="height:100%">
                                    </div>
                                    <c:set var="imgViewCount" value="${imgViewCount+1}"/>
                                    </c:if>
                                </c:forEach>
                                <!-- Next/previous controls -->
                                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                <a class="next" onclick="plusSlides(1)">&#10095;</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </c:if>

        <hr/>

        <%--reviews & rating--%>
        <div class="row col-md-12">
            <div class="row col-md-12">
                <span class="text-muted font-large">
                    <fmt:message code="appstore.app.app.view.page.app.reviews.label"/>
                </span>
                <span class="pull-rights">
                    <appAddReview
                            href=""
                            class="appstore-app-view-action-button vdf-button branding-background-hover"
                            data-toggle="modal"
                            data-target="#appStoreModal"
                            app-name="<c:out value="${appDetails.name}"/>"
                            app-id="<c:out value="${appDetails.id}"/>">
                        <fmt:message code="appstore.app.app.view.page.add.review"/>
                    </appAddReview>
                    &nbsp;
                    <appAddRate
                            style="margin-right:10px"
                            href=""
                            class="appstore-app-view-action-button vdf-button branding-background-hover"
                            data-toggle="modal"
                            data-target="#appStoreModal"
                            app-name="<c:out value="${appDetails.name}"/>"
                            app-id="<c:out value="${appDetails.id}"/>">
                        <fmt:message code="appstore.app.app.view.page.rate.app"/>
                    </appAddRate>

                </span>
                <br/>
                <br/>

                <%--App comments--%>
                <span>
                    <c:if test="${fn:length(appDetails.userComments) eq 0}">
                        <fmt:message code="appstore.app.app.view.page.app.no.reviews.label"/>
                    </c:if>

                    <c:if test="${fn:length(appDetails.userComments) gt 0}">
                        <c:set var="commentCount" value="0"/>
                        <c:forEach items="${appDetails.userComments}" var="comment">
                            <c:if test="${commentCount gt 3}">
                                <div class="collapse viewDetails">
                            </c:if>
                                <div class="row padding-left-right-0">
                                    <div class="col-md-1 padding-left-right-0">
                                        <img src="./resources/images/account-72x72.png" alt="avatar" class="img-circle" width="100%" style="background-color: #EBEBEB">
                                        <%-- If it is required to show user profile picture with comments,
                                         comment the above line and un-comment the following c:choose block. --%>
<%--                                        <c:choose>
                                            <c:when test="${comment.image ne '' }">
                                                <img src='${baseUrl}${comment.image}' class="profile-pic-circle" width="100%">
                                            </c:when>
                                            <c:otherwise>
                                                <img src="./resources/images/account-72x72.png" alt="avatar" class="img-circle" width="100%" style="background-color: #EBEBEB">
                                            </c:otherwise>
                                        </c:choose>--%>
                                    </div>
                                    <div class="col-md-9 padding-right-0">
                                        <strong><c:out value="${comment.userName}"/></strong>
                                        <br/>
                                        <p>
                                            <c:if test="${fn:length(comment.comments) le 150}">
                                                <c:out value="${comment.comments}"/>
                                            </c:if>
                                            <c:if test="${fn:length(comment.comments) gt 150}">
                                                <c:set var="commentLenght" value="${fn:length(comment.comments)}" />
                                                <c:set var="comment0" value="${fn:substring(comment.comments, 0, 150)}" />
                                                <c:set var="comment1" value="${fn:substring(comment.comments, 150, commentLenght)}" />
                                                <c:out value="${comment0}"/>
                                                <span class="collapse" id="viewdetails${commentCount}"> <c:out value="${comment1}"/> </span>
                                                <a data-toggle="collapse" data-target="#viewdetails${commentCount}" onclick="changeText(this)">Read More..</a>
                                            </c:if>
                                        </p>
                                    </div>
                                    <div class="col-md-2 padding-left-right-0 review-date">
                                        <span>
                                            <format:formatDate value="${comment.date}" pattern="MMMM dd, yyyy"/>
                                        </span>
                                        <br/>
                                        <%--insert remove comment function here--%>
                                        <sec:authorize ifAllGranted="ROLE_APP_ADMIN">
                                            <a href="removeComment?commentId=<c:out value='${comment.commentId}'/>&appId=<c:out value='${appDetails.id}'/>">
                                                <i class="icon-ban-circle comment-ban"></i>
                                            </a>
                                        </sec:authorize>
                                    </div>
                                </div>
                                <br/>
                            <c:if test="${commentCount gt 3}">
                                </div>
                            </c:if>
                            <c:set value="${commentCount+1}" var="commentCount"/>

                        </c:forEach>

                        <c:if test="${commentCount gt 3}">
                            <a data-toggle="collapse" data-target=".viewDetails" id="readMoreReview">
                                <fmt:message code="appstore.main.lable.read.more.reviews"/>
                            </a>
                            <script>
                            $("#readMoreReview").click(function() {
                                var text = ( $(this).text().trim() === 'Read More Reviews') ? 'Less Reviews' : 'Read More Reviews';
                                $(this).text(text);
                            });

                            function changeText(x) {
                                var text = (x.text === 'Read More..') ? 'Less..' : 'Read More..';
                                x.text = text;
                            }

                        </script>
                        </c:if>

                    </c:if>

                </span>

            </div>
        </div>

        <hr/>

        <%--from developer--%>
        <div class="row col-md-12">
            <div class="row col-md-12">
                <span class="text-muted font-large">
                    <fmt:message code="appstore.app.app.view.page.app.from.developer.label"/>
                </span>
                <br/>
                <span>
                    <c:forEach items="${appsByDeveloper}" var="app">
                        <a href='view?appId=<c:out value="${app.id}"/>'>
                            <div class="col-md-2 app-detail-tile">
                                <div class="row app-detail-tile-inner">
                                    <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                         class="margin-bottom-5" width="100%"/>
                        <span class="app-detail-tile-text">
                            <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                            <br/>
                            <span class="appstore-app-owner">
                                <span class="appstore-app-owner-name">
                                    <c:out value="${app.developer}"/>
                                </span>
                                <span class="pull-right">
                                    <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                    <i class="icon-star rating-color"></i>
                                </span>
                            </span>

                            <br/>

                            <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                <div class="row app-tile-button">
                                    <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${app.name}"/>'
                                            app-id='<c:out value="${app.id}"/>'
                                            app-charging='<c:out value="${app.chargingLabel}"/>'
                                            app-type='
                                                <c:if test="${app.canDownload}">
                                                    download
                                                </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    subscribe
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    unSub
                                                </c:if>'>
                                        <span class="app-tile-text">
                                           <c:if test="${app.canDownload && !app.canSubscribe}">
                                               <i class="icon-download-alt"></i>
                                               DOWNLOAD
                                           </c:if>
                                            <c:if test="${app.canSubscribe}">
                                                <i class="icon-rss"></i>
                                                SUBSCRIBE
                                            </c:if>
                                            <c:if test="${app.canUnSubscribe}">
                                                <i class="icon-ban-circle"></i>
                                                UN-SUBSCRIBE
                                            </c:if>
                                        </span>
                                    </appAction>
                                </div>
                            </c:if>
                            <c:if test="${ !app.canSubscribe && !app.canDownload && !app.canUnSubscribe}">
                                <div class="row app-tile-button" style="background-color: #ffffff">
                                    &nbsp;
                                </div>
                            </c:if>

                        </span>
                                </div>
                            </div>
                        </a>
                    </c:forEach>
                </span>
            </div>
        </div>

    </div>

    <%--similar apps--%>
    <div class="row col-md-3 padding-left-right-0">
        <div class="row col-md-12 app-panel-title-area vdf-app-panel-header" style="margin-top: 0px;">
            <div class="col-md-8 padding-left-5 padding-right-0 vdf-app-panel-title app-panel-title">
                <fmt:message code="appstore.similar.apps.title"/>
            </div>
            <div class="col-md-4 popular-app-view-all">
                <a href="viewSimilarApps?appId=${appDetails.id}&appName=${appDetails.name}" class="btn btn-danger btn-md padding-1-3">
                    <fmt:message code="appstore.app.panel.view.all.text"/>
                </a>
            </div>
        </div>

        <c:forEach items="${relatedApps}" var="app">

            <a href='view?appId=<c:out value="${app.id}"/>'>
                <div class="col-md-6 app-detail-tile">
                    <div class="row app-detail-tile-inner">
                        <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>' width="100%" class="margin-bottom-5"/>
                        <span class="app-detail-tile-text">
                            <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                            <br/>
                            <span class="appstore-app-owner">
                                <span class="appstore-app-owner-name">
                                    <c:out value="${app.developer}"/>
                                </span>
                                <span class="pull-right">
                                    <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                    <i class="icon-star rating-color"></i>
                                </span>
                            </span>

                            <br/>

                            <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                <div class="row app-tile-button">
                                    <appAction
                                            href=''
                                            data-target='#appStoreModal'
                                            data-toggle='modal'
                                            id='appActionButton'
                                            app-name='<c:out value="${app.name}"/>'
                                            app-id='<c:out value="${app.id}"/>'
                                            app-charging='<c:out value="${app.chargingLabel}"/>'
                                            app-type='
                                                <c:if test="${app.canDownload}">
                                                    download
                                                </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    subscribe
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    unSub
                                                </c:if>'>
                                        <span class="app-tile-text">
                                           <c:if test="${app.canDownload && !app.canSubscribe}">
                                               <i class="icon-download-alt"></i>
                                               DOWNLOAD
                                           </c:if>
                                            <c:if test="${app.canSubscribe}">
                                                <i class="icon-rss"></i>
                                                SUBSCRIBE
                                            </c:if>
                                            <c:if test="${app.canUnSubscribe}">
                                                <i class="icon-ban-circle"></i>
                                                UN-SUBSCRIBE
                                            </c:if>
                                        </span>
                                    </appAction>
                                </div>
                            </c:if>
                            <c:if test="${ !app.canSubscribe && !app.canDownload && !app.canUnSubscribe}">
                                <div class="row app-tile-button" style="background-color: #ffffff">
                                    &nbsp;
                                </div>
                            </c:if>
                        </span>
                    </div>
                </div>
            </a>
        </c:forEach>
    </div>

</div>

<script>
    // Open the Modal
    function openModal() {
        document.getElementById('myModal').style.display = "block";
    }

    // Close the Modal
    function closeModal() {
        document.getElementById('myModal').style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("demo");
        var captionText = document.getElementById("caption");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
        captionText.innerHTML = dots[slideIndex-1].alt;
    }
</script>

</body>
</html>