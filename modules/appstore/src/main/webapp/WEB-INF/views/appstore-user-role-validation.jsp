<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="modal-header vdf-modal-header branding-background">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style" id="subscribeModalLabel">
            <i class="icon-remove"></i>
            <span id='subscribingAppName'><fmt:message code="appstore.access.denied"/> </span></h4>
    </div>
    <div class="modal-body" style="padding-bottom: 0px">
            <%--Add application Id here--%>
            <fmt:message code="appstore.invalid.consumer.message"/>
            <%----%>
    </div>
    <div class="modal-footer modal-footer-admin-disabled">
        <button type="submit" class="btn vdf-button branding-background-hover" data-dismiss="modal"><fmt:message code="appstore.ok.button.title"/> </button>
    </div>