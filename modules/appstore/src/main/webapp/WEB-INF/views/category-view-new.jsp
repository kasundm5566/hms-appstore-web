<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><c:out value="${panelTitle}"/> <fmt:message code="appstore.main.title"/></title>
</head>

<body>
<%--Page content begins--%>
<div class="row">
    <c:if test="${fn:length(banners) gt 0}">
        <%--category banner--%>
        <div id="myCarousel" class="carousel">
            <ol class="carousel-indicators">
                <c:set value="0" var="count"/>
                <c:forEach items="${banners}" var="b">
                    <li data-target="#myCarousel" data-slide-to='<c:out value="${count}"/>'
                        class='<c:if test="${count==0}">active</c:if>'></li>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </ol>
            <div class="carousel-inner">
                <c:set value="1" var="count"/>
                <c:forEach items="${banners}" var="banner">
                    <div class='item <c:if test="${count eq 1}">active</c:if> '>
                        <a href="${banner.link}" target="_blank">
                            <img src="${baseUrl}${banner.image_url}" width="100%"/>
                        </a>
                    </div>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </div>
        </div>
    </c:if>
</div>

<div class="row">

    <%--apps in category--%>
    <div class="row col-md-12 padding-left-right-0">
        <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
            <div class="col-md-12 padding-left-right-0 vdf-app-panel-title app-panel-title">${panelTitle}</div>
        </div>
        <%--alert if no applications found--%>
        <c:if test="${fn:length(appList) eq 0}">
            <h4 class="alert alert-warning"><fmt:message code="appstore.app.app.category.no.apps.notice"/></h4>
        </c:if>

        <c:if test="${fn:length(appList) ge 1}">
            <c:set var="count" value="0"/>
            <c:forEach items="${appList}" var="app">
                <a href='view?appId=<c:out value="${app.id}"/>'>
                    <div class="col-md-2 col-sm-4 col-xs-6 app-tile
                        <c:if test='${count%6 eq 0 }'>padding-left-0</c:if>
                        <c:if test='${ (count-5)%6 eq 0 }'>padding-right-0</c:if>
                                                                ">
                        <div class="appstore-app container">
                            <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                 class="appstore-app-icon"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${app.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>

                                <br/>

                                <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${app.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${app.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${app.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${app.canDownload && !app.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                            </span>
                        </div>
                    </div>
                </a>
                <c:set value="${count+1}" var="count"/>
            </c:forEach>
        </c:if>

    </div>

    <br/>

    <%--Page content ends--%>
    <div align="center" class="margin-top-bottom-5">
        <div class="pagination">

        </div>
    </div>

    <%--Most used apps in category--%>
    <div class="row col-md-12 padding-left-right-0">

        <c:if test="${fn:length(mostUsedApps) ge 1}">

        <div class="row col-md-12 app-panel-title-area vdf-app-panel-header">
            <div class="col-md-12 padding-left-right-0 vdf-app-panel-title app-panel-title">
                <c:choose>
                    <c:when test="${ panelTitle eq 'Most Used Apps' }">
                        <fmt:message code="appstore.home.page.newly.added.panel.title"/>
                    </c:when>
                    <c:otherwise>
                        <fmt:message code="appstore.mostly.used.panel.title"/>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>

        <c:set var="count" value="0"/>

        <c:forEach items="${mostUsedApps}" var="app">
        <a href='view?appId=<c:out value="${app.id}"/>'>
            <div class="col-md-2 col-sm-4 col-xs-6 app-tile
                 <c:if test='${count%6 eq 0 }'>padding-left-0</c:if>
                 <c:if test='${ (count-5)%6 eq 0 }'>padding-right-0</c:if>
                                                            ">
                <div class="appstore-app container">
                    <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                         class="appstore-app-icon"/>
                    <span class="appstore-app-name">
                        <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                        <br/>
                        <span class="appstore-app-owner">
                            <span class="appstore-app-owner-name">
                                <c:out value="${app.developer}"/>
                            </span>
                            <span class="pull-right">
                                <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                <i class="icon-star rating-color"></i>
                            </span>
                        </span>

                        <br/>

                        <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                            <div class="row app-tile-button">
                                <appAction
                                        href=''
                                        data-target='#appStoreModal'
                                        data-toggle='modal'
                                        id='appActionButton'
                                        app-name='<c:out value="${app.name}"/>'
                                        app-id='<c:out value="${app.id}"/>'
                                        app-charging='<c:out value="${app.chargingLabel}"/>'
                                        app-type='
                                            <c:if test="${app.canDownload}">
                                                download
                                            </c:if>
                                            <c:if test="${app.canSubscribe}">
                                                subscribe
                                            </c:if>
                                            <c:if test="${app.canUnSubscribe}">
                                                unSub
                                            </c:if>'>
                                    <span class="app-tile-text">
                                       <c:if test="${app.canDownload && !app.canSubscribe}">
                                           <i class="icon-download-alt"></i>
                                           DOWNLOAD
                                       </c:if>
                                        <c:if test="${app.canSubscribe}">
                                            <i class="icon-rss"></i>
                                            SUBSCRIBE
                                        </c:if>
                                        <c:if test="${app.canUnSubscribe}">
                                            <i class="icon-ban-circle"></i>
                                            UN-SUBSCRIBE
                                        </c:if>
                                    </span>
                                </appAction>
                            </div>
                        </c:if>
                    </span>
                </div>
            </div>
        </a>
    <c:set value="${count+1}" var="count"/>
    </c:forEach>

    </c:if>
</div>


</div>

<script type="text/javascript">

    var pageSize = parseInt('<c:out value="${appsPerPage}"/>');
    var categoryKey = '<c:out value="${param.category}"/>';
    categoryKey = encodeURIComponent(categoryKey.replace(/&amp;/g, "&"))
    <%--var searchKey = '<c:out value="${param.searchKey}"/>';--%>

    var options = {
        currentPage:<c:out value="${active+1}"/>,
        totalPages:<c:out value="${fn:length(pagination)}"/>,
        useBootstrapTooltip: true,
        numberOfPages:<c:out value="${numberOfPages}"/>,
        pageUrl: function (type, page, current) {
            return ("?category=" + categoryKey + "&skip=" + ((page - 1) * pageSize) + "&limit=" + pageSize);
        }
    };

    $(document).ready(function () {
        $(".pagination").bootstrapPaginator(options);
    });

</script>

</body>
</html>