<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <title><fmt:message code="appstore.publish.page.sub.title"/> <fmt:message code="appstore.main.title"/></title>
    </head>

    <body>
        <%--Add application Id here--%>
    <c:if test="${status=='S1000'}">
        <c:if test="${app != null}">
            <getPage id="publish"></getPage>
            <%--Page content begins--%>
            <div class="row app-view-main-panel">
            <div class="row app-view-main-panel-top">
                <div class="col-sm-10 col-md-10 col-xs-12 app-view-app-description-area">
                    <div class="app-view-app-details-area">
                        <div class="appstore-app-publish-title">
                            <i class="icon-check"></i> <fmt:message code="appstore.app.app.publish.form.title"/>
                        </div>
                    </div>
                </div>
            </div>
                <%--</div>--%>

            <div class="row app-upload-form-row">
            <div class="row profile-row vdf-gray-font">

            <form class="form-horizontal" role="form" action="update-app" method="POST" id="update-app">
            <div class="form-group">
                <label for="appName" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.name.label"/></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="appName" id="appName" value='<c:out value="${app.name}"/>' readonly/>
                </div>
            </div>
            <div class="form-group">
                <label for="pubName" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.publish.name.label"/></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" id="pubName" required="required" name="pubName" value='' maxlength="25">
                </div>
            </div>
            <div class="form-group">
                <label for="appCategoryList" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.category.label"/></label>
                <div class="col-sm-6">
                    <select class="form-control" id="appCategoryList" name="category">
                        <c:forEach items="${appCategories}" var="category">
                            <option><c:out value="${category}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="appLabel1" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.label1.label"/></label>
                <div class="col-sm-6">
                    <select class="form-control" id="appLabel1" name="label_1">
                        <c:forEach items="${appCategories}" var="category">
                            <option><c:out value="${category}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="appLabel2" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.label2.label"/></label>
                <div class="col-sm-6">
                    <select class="form-control" id="appLabel2" name="label_2">
                        <c:forEach items="${appCategories}" var="category">
                            <option><c:out value="${category}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="appLabel3" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.label3.label"/></label>
                <div class="col-sm-6">
                    <select class="form-control" id="appLabel3" name="label_3">
                        <c:forEach items="${appCategories}" var="category">
                            <option><c:out value="${category}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="appIcon" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.icon.label"/></label>
                <div class="col-sm-6" id='appIcon'>

                        <%--ICON UPLOAD--%>
                    <div class='image-select-class' id=''
                         onclick="return getFile('IconAppImage')">
                        <img src='' id='IconSelectedImgPreview'
                             class="selectedImage"  style="display: none;">

                        <div id='icon_defaultImage_1' class="image_upload_placeholder">
                            <i class="icon-picture icon-3x image-select-icon"></i>
                            <br/><fmt:message code="appstore.select.image"/>
                        </div>
                    </div>
                    <input type="file" style="display: none" class="hidden" id="IconAppImage"
                           onchange="preview(this,
                                           'IconSelectedImgPreview',
                                           'icon_defaultImage_1',
                                           'iconImageHash',
                                           'Icon',
                                           'icon_1',
                                           'iconError')"/>

                    <input type="text" name="appIcon"  id="iconImageHash" class="hidden" required/>
                    <div class="imageValidationNote"><b><fmt:message code="field.required"/></b><fmt:message code="publish.icon.validate"/> </div>
                    <span id="iconError" class="has-error"></span>
                        <%--ICON UPLOAD END--%>

                </div>
            </div>

            <c:if test="${app.canDownload}">
                <div class="form-group">
                    <label for="appScreenShots" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.screen.label"/></label>
                    <div class="row">

                        <div class="col-sm-5" id='appScreenShots'>
                                <%--SCREEN_SHOT UPLOAD--%>
                            <div class='image-select-class' id='' onclick="return getFile('screenAppImage')">
                                <img src='' id='ScreenSelectedImgPreview'
                                     class="selectedImage" style="display: none;">

                                <div id='screen_defaultImage_1' class="image_upload_placeholder">
                                    <i class="icon-picture icon-3x image-select-icon"></i>
                                    <br/><fmt:message code="appstore.select.image"/>
                                </div>
                            </div>
                            <input type="file" style="display: none" class="hidden" id="screenAppImage"
                                   onchange="preview(this,
                                           'ScreenSelectedImgPreview',
                                           'screen_defaultImage_1',
                                           'screenImageHash_1',
                                           'ScreenShot',
                                           'screenshot_1',
                                           'screenError')"/>

                            <input type="text" name="screenShot"  id="screenImageHash_1" class="hidden"/>
                                <%--SCREEN_SHOT UPLOAD END--%>


                                <%--SCREEN_SHOT UPLOAD--%>
                            <div class='image-select-class' id=''
                                 onclick="return getFile('screenAppImage_1')">
                                <img src='' id='ScreenSelectedImgPreview_1'
                                     class="selectedImage" style="display: none;">

                                <div id='screen_defaultImage_2' class="image_upload_placeholder">
                                    <i class="icon-picture icon-3x image-select-icon"></i>
                                    <br/><fmt:message code="appstore.select.image"/>
                                </div>
                            </div>
                            <input type="file" style="display: none" class="hidden" id="screenAppImage_1"
                                   onchange="preview(this,
                                   'ScreenSelectedImgPreview_1',
                                   'screen_defaultImage_2',
                                   'screenImageHash_2',
                                   'ScreenShot',
                                   'screenshot_2',
                                   'screenError')"/>

                            <input type="text" name="screenShot_1"  id="screenImageHash_2" class="hidden"/>
                                <%--SCREEN_SHOT UPLOAD END--%>

                                <%--SCREEN_SHOT UPLOAD--%>
                            <div class='image-select-class' id=''
                                 onclick="return getFile('screenAppImage_2')">
                                <img src='' id='ScreenSelectedImgPreview_2'
                                     class="selectedImage" style="display: none;">

                                <div id='screen_defaultImage_3' class="image_upload_placeholder">
                                    <i class="icon-picture icon-3x image-select-icon"></i>
                                    <br/><fmt:message code="appstore.select.image"/>
                                </div>
                            </div>
                            <input type="file" style="display: none" class="hidden" id="screenAppImage_2"
                                   onchange="preview(this,
                                   'ScreenSelectedImgPreview_2',
                                   'screen_defaultImage_3',
                                   'screenImageHash_3',
                                   'ScreenShot',
                                   'screenshot_3',
                                   'screenError')"/>

                            <input type="text" name="screenShot_2"  id="screenImageHash_3" class="hidden"/>
                                <%--SCREEN_SHOT UPLOAD END--%>

                        </div>
                        <div>
                            <div class="imageValidationNote"><fmt:message code="publish.screenshot.validate"/> </div>
                            <span id="screenError" class="has-error"></span>
                        </div>
                        <%--<div class="col-sm-6" id='appScreenShots'>--%>
                               <%----%>

                        <%--</div>--%>

                    </div>

                </div>
                <div class="form-group">
                    <label for="appMScreenShots" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.mobile.screen.label"/></label>
                    <div class="col-sm-5" id='appMScreenShots'>

                            <%--Mobile_SCREEN_SHOT UPLOAD--%>
                        <div class='image-select-class' id=''
                             onclick="return getFile('mobileAppImage_1')">
                            <img src='' id='mobileSelectedImgPreview_1'
                                 class="selectedImage" style="display: none;">

                            <div id='mobile_defaultImage_1' class="image_upload_placeholder">
                                <i class="icon-picture icon-3x image-select-icon"></i>
                                <br/><fmt:message code="appstore.select.image"/>
                            </div>
                        </div>
                        <input type="file" style="display: none" class="hidden" id="mobileAppImage_1"
                               onchange="preview(this,
                                            'mobileSelectedImgPreview_1',
                                            'mobile_defaultImage_1',
                                            'mobileImageHash_1',
                                            'MobileScreenShot',
                                            'mobile_screenshot_1',
                                            'mobileError')"/>

                        <input type="text" name="mobileScreenShot_1"  id="mobileImageHash_1" class="hidden"/>
                        <%--Mobile_SCREEN_SHOT UPLOAD END--%>

                        <%--Mobile_SCREEN_SHOT UPLOAD--%>
                    <div class='image-select-class' id=''
                         onclick="return getFile('mobileAppImage_2')">
                        <img src='' id='mobileSelectedImgPreview_2'
                             class="selectedImage" style="display: none;">

                        <div id='mobile_defaultImage_2' class="image_upload_placeholder">
                            <i class="icon-picture icon-3x image-select-icon"></i>
                            <br/><fmt:message code="appstore.select.image"/>
                        </div>
                    </div>
                    <input type="file" style="display: none" class="hidden" id="mobileAppImage_2"
                           onchange="preview(this,
                                'mobileSelectedImgPreview_2',
                                'mobile_defaultImage_2',
                                'mobileImageHash_2',
                                'MobileScreenShot',
                                'mobile_screenshot_2',
                                'mobileError')"/>

                    <input type="text" name="mobileScreenShot_2"  id="mobileImageHash_2" class="hidden"/>
                        <%--Mobile_SCREEN_SHOT UPLOAD END--%>

                        <%--Mobile_SCREEN_SHOT UPLOAD--%>
                    <div class='image-select-class' id=''
                         onclick="return getFile('mobileAppImage_3')">
                        <img src='' id='mobileSelectedImgPreview_3'
                             class="selectedImage" style="display: none;">

                        <div id='mobile_defaultImage_3' class="image_upload_placeholder">
                            <i class="icon-picture icon-3x image-select-icon"></i>
                            <br/><fmt:message code="appstore.select.image"/>
                        </div>
                    </div>
                    <input type="file" style="display: none" class="hidden" id="mobileAppImage_3"
                           onchange="preview(this,
                                'mobileSelectedImgPreview_3',
                                'mobile_defaultImage_3',
                                'mobileImageHash_3',
                                'MobileScreenShot',
                                'mobile_screenshot_3',
                                'mobileError')"/>

                    <input type="text" name="mobileScreenShot_3"  id="mobileImageHash_3" class="hidden"/>
                        <%--Mobile_SCREEN_SHOT UPLOAD END--%>


                    </div>
                    <div>
                        <div class="imageValidationNote"><fmt:message code="publish.mobilescreenshot.validate"/> </div>
                        <span id="mobileError" class="has-error"></span>
                    </div>

                </div>
            </c:if>
            <div class="form-group">
                <label for="appDescription" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.description.label"/></label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="appDescription" name="description"><c:out value="${app.description}"/></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="appShortDescription" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.short.description.label"/></label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="appShortDescription" name="shortDesc"><c:out value="${app.shortDesc}"/></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="appUsageInstructions" class="col-sm-3 control-label"><fmt:message code="appstore.app.app.publish.form.app.usage.instructions.label"/></label>
                <div class="col-sm-6">
                    <textarea class="form-control" id="appUsageInstructions" name="instructions"><c:forEach items="${appDetails.instructions}" var="entry"><c:out value="${entry.key}"/> : <c:out value="${entry.value}"/></c:forEach></textarea>
                </div>
            </div>
                <%--<div class="form-group">--%>
                <%--<label for="developerName" class="col-sm-3 control-label"><fmt:message key="appstore.app.app.publish.form.app.developer.name.label"/></label>--%>
                <%--<div class="col-sm-6">--%>
                <%--<input type="text" class="form-control" id="developerName">--%>
                <%--</div>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                <%--<label for="appServiceProList" class="col-sm-3 control-label"><fmt:message key="appstore.app.app.publish.form.app.service.provider.label"/></label>--%>
                <%--<div class="col-sm-6">--%>
                <%--<select class="form-control" id="appServiceProList">--%>
                <%--<option selected="selected"></option>--%>
                <%--<option>Dialog</option>--%>
                <%--<option>Etisalat</option>--%>
                <%--<option>Vodafone</option>--%>
                <%--</select>--%>
                <%--</div>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                <%--<label for="serviceProShortCode" class="col-sm-3 control-label"><fmt:message key="appstore.app.app.publish.form.app.short.code.label"/></label>--%>
                <%--<div class="col-sm-6">--%>
                <%--<select class="form-control" id="serviceProShortCode">--%>
                <%--<option selected="selected"></option>--%>
                <%--<option>77700</option>--%>
                <%--<option>72077</option>--%>
                <%--<option>05877</option>--%>
                <%--</select>--%>
                <%--</div>--%>
                <%--</div>--%>
                <%--<div class="form-group">--%>
                <%--<label for="appKeyWords" class="col-sm-3 control-label"><fmt:message key="appstore.app.app.publish.form.app.key.words.label"/></label>--%>
                <%--<div class="col-sm-6">--%>
                <%--<input type="text" class="form-control" id="appKeyWords">--%>
                <%--</div>--%>
                <%--</div>--%>

            <input type="hidden" name="appId" value="${app.id}"/>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit"  class="btn btn-default vdf-button"><fmt:message code="appstore.app.app.publish.form.app.publish.button.value"/></button>
                </div>
            </div>
            </form>
            </div>
            </div>
            </div>
            <%--Page content ends--%>
        </c:if>

        <c:if test="${app == null}">
            <p class="publishingSuccessNotice"><i class="icon-ok"></i> <fmt:message code="appstore.publishing.success"/></p>
        </c:if>
    </c:if>
    <c:if test="${status =='E5106'}">
        <p class="publishingDuplicateNotice"><i class="icon-warning-sign"></i> <fmt:message code="appstore.publishing.exists"/></p>
    </c:if>
    <c:if test="${status =='E5000'}">
        <p class="publishingErrorNotice"><i class="icon-remove"></i> <fmt:message code="appstore.publishing.error"/></p>
    </c:if>
    </body>
    </html>