<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:bundle basename="messages">

    <!DOCTYPE html>
    <html lang="en">
    <head>

        <title>
            <fmt:message key="iap.title"/>
        </title>

    </head>
    <body>
        <%--<body data-twttr-rendered="true" itemscope="" itemtype="http://schema.org/Product">--%>
    <script type="text/javascript">
        window.onload = function () {
            var counter = 5;
            var interval = setInterval(function () {
                counter--;
                $("#seconds").text('You will be redirected to application within  '
                        + counter + "  seconds...");
                if (counter == 0) {
                    postToUrl("<c:out value="${callback_url}"/>", "<c:out value="${iap_response_base64}"/>");
                    clearInterval(interval);
                }
            }, 1000);

        };

        function postToUrl(url, response) {
            var form = document.createElement("form");
            form.setAttribute("method", "post");
            form.setAttribute("action", url);

            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", "iap-response");
            hiddenField.setAttribute("value", response);
            form.appendChild(hiddenField);

            document.body.appendChild(form);
            form.submit();
        }

    </script>


    <div class="panel panel-default col-sm-8 col-sm-offset-2" style="margin-top: 5%;">

        <div class="panel-body">
            <h4 class="branding-font-style">
                <fmt:message key="iap.purchase.complete"/>
            </h4>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <fmt:message key="iap.purchase.summary"/>
                </div>
                <div class="panel-body" style="padding-left:0px;padding-right:0px;">
                    <table class="table table-borderless table-responsive">
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.application.name"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info_done.applicationName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.item.name"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info_done.itemName}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.item.description"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info_done.itemDescription}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <fmt:message key="iap.purchase.info.amount"/>
                            </td>
                            <td>
                                <c:out value="${purchase_info_done.currency}"/> <c:out
                                    value="${purchase_info_done.amount}"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="alert alert-success">
                <p>
                        <c:out value="${iap_response_displayable_text}"/>
                    <br/>
                    <br/>

                <div id="seconds">
                    You will be redirected to application within 5 seconds...
                </div>
                </p>
            </div>
        </div>


    </div>


    </body>
    </html>
</fmt:bundle>