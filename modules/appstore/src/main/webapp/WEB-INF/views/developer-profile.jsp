<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title><c:out value="${panelTitle}"/> <fmt:message code="appstore.main.title"/></title>
</head>

<body>
<div id="fb-root"></div>
<%--Page content begins--%>
<div class="row">
    <c:if test="${fn:length(banners) gt 0}">
        <%--category banner--%>
        <div id="myCarousel" class="carousel">
            <ol class="carousel-indicators">
                <c:set value="0" var="count"/>
                <c:forEach items="${banners}" var="b">
                    <li data-target="#myCarousel" data-slide-to='<c:out value="${count}"/>' class='<c:if test="${count==0}">active</c:if>'></li>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </ol>
            <div class="carousel-inner">
                <c:set value="1" var="count"/>
                <c:forEach items="${banners}" var="banner">
                    <div class='item <c:if test="${count eq 1}">active</c:if> '>
                        <a href="${banner.link}" target="_blank">
                            <img src="${baseUrl}${banner.image_url}" width="100%"/>
                        </a>
                    </div>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </div>
        </div>
    </c:if>
</div>

<div class="row col-md-12 app-panel-title-area vdf-app-panel-header" style="margin-bottom: -5px;">
    <div class="col-md-10 padding-left-right-0 vdf-app-panel-title app-panel-title">
        <fmt:message code="appstore.app.app.view.developer.profile"/>
    </div>
</div>

<div class="row profile-detail-tile-inner">

    <form class="form-horizontal">
        <div class="row padding-15">

            <div class="col-md-3" style="padding-left: 30px">
                <div class="col-md-12">
                    <c:if test="${profileImage != '' }">
                        <img src='${baseUrl}${developerImage}' class="profile-pic-circle" width="100%">
                    </c:if>
                    <c:if test="${profileImage == '' }">
                        <img src='./resources/images/appstore_default_profile.jpg' class="profile-pic-circle" width="100%">
                    </c:if>
                </div>
            </div>

            <div class="col-md-6" style="padding-top:25px;">

                <div class="form-group">
                    <label for="fullDeveloperName" class="col-md-4 control-label"> <fmt:message code="appstore.user.profile.developer.name"/> </label>
                    <div class="col-md-8">
                        <label id="fullDeveloperName" class="control-text">${developerName}</label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-4 control-label"> <fmt:message code="appstore.user.profile.developer.email"/> </label>
                    <div class="col-md-8">
                        <label class="control-text" id="email">${developerEmail}</label>
                    </div>
                </div>

            </div>
        </div>

    </form>
</div>

<div class="row col-md-12 app-panel-title-area vdf-app-panel-header" style="margin-bottom: -5px;">
    <div class="col-md-10 padding-left-right-0 vdf-app-panel-title app-panel-title">
        <fmt:message code="appstore.app.app.view.page.app.from.developer.label"/> ${developerFirstName} ${developerLastName}
    </div>
</div>

<div class="row">
        <%--alert if no applications found--%>
        <c:if test="${fn:length(appList) eq 0}">
            <h4 class="alert alert-warning"><fmt:message code="appstore.app.app.category.no.apps.notice"/></h4>
        </c:if>

        <c:if test="${fn:length(appList) ge 1}">
            <c:set var="count" value="0"/>
            <c:forEach items="${appList}" var="app">

                <a href='view?appId=<c:out value="${app.id}"/>'>
                    <div class="col-md-2 col-sm-4 col-xs-6 app-tile" <c:if test="${count%6 eq 0 }">style="padding-left: 0px;" </c:if>>
                        <div class="appstore-app container">
                            <img src='<fmt:message code="appstore.app.icon.base.url"/><c:out value="${app.appIcon}"/>'
                                 class="appstore-app-icon"/>
                            <span class="appstore-app-name">
                                <span class="appstore-ellipsis"><c:out value="${app.name}"/></span>
                                <br/>
                                <span class="appstore-app-owner">
                                    <span class="appstore-app-owner-name">
                                        <c:out value="${app.developer}"/>
                                    </span>
                                    <span class="pull-right">
                                        <format:formatNumber type="number"  groupingUsed="false" value="${app.rating}" />
                                        <i class="icon-star rating-color"></i>
                                    </span>
                                </span>

                                <br/>

                                <c:if test="${app.canSubscribe||app.canDownload||app.canUnSubscribe}">
                                    <div class="row app-tile-button">
                                        <appAction
                                                href=''
                                                data-target='#appStoreModal'
                                                data-toggle='modal'
                                                id='appActionButton'
                                                app-name='<c:out value="${app.name}"/>'
                                                app-id='<c:out value="${app.id}"/>'
                                                app-charging='<c:out value="${app.chargingLabel}"/>'
                                                app-type='
                                                    <c:if test="${app.canDownload}">
                                                        download
                                                    </c:if>
                                                    <c:if test="${app.canSubscribe}">
                                                        subscribe
                                                    </c:if>
                                                    <c:if test="${app.canUnSubscribe}">
                                                        unSub
                                                    </c:if>'>
                                            <span class="app-tile-text">
                                               <c:if test="${app.canDownload && !app.canSubscribe}">
                                                   <i class="icon-download-alt"></i>
                                                   DOWNLOAD
                                               </c:if>
                                                <c:if test="${app.canSubscribe}">
                                                    <i class="icon-rss"></i>
                                                    SUBSCRIBE
                                                </c:if>
                                                <c:if test="${app.canUnSubscribe}">
                                                    <i class="icon-ban-circle"></i>
                                                    UN-SUBSCRIBE
                                                </c:if>
                                            </span>
                                        </appAction>
                                    </div>
                                </c:if>
                            </span>
                        </div>
                    </div>
                </a>
                <c:set value="${count+1}" var="count"/>
            </c:forEach>
        </c:if>
</div>
<%--Page content ends--%>

</body>
</html>