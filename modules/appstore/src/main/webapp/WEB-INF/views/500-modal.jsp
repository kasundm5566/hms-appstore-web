<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <div class="modal-header vdf-modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title branding-font-style text-center" id="subscribeModalLabel">
            <i class="icon-exclamation-sign"></i>
            <fmt:message code="appsotre.modal.error.title"/>
        </h4>
    </div>
    <div class="modal-body">
        <h5 class="vdf-gray-font text-center">
            <c:out value="${message}"/>
        </h5>

        <br/>

        <div class="col-md-12 popup-btn">
            <button type="close" class="btn vdf-button branding-background-hover" data-dismiss="modal">
                <fmt:message code="appstore.ok.button.title"/>
            </button>
        </div>

    </div>