<%@ taglib prefix="fmt" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal-header vdf-modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title branding-font-style text-center" id="signInModalLabel"><i class="icon-user"></i>
        <fmt:message code="appstore.sign.in.modal.title"/>
    </h4>
</div>
<div class="modal-body">
    <form role="form">
        <div class="form-group">
            <div class="col-md-12">
                <p class="sign-in-connect-notice vdf-gray-font">
                    <fmt:message code="appstore.app.user.sign.in.notice"/>
                </p>
            </div>
        </div>
        <a class="btn btn-lg btn-block vdf-button vdf-sign-in-button branding-background-hover" type="submit"
               href='login_direct'>
            <fmt:message code="appstore.sign.in.button.value"/>
        </a>
    </form>
</div>