<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--Navigation start--%>
<div class="navbar navbar-default vdf-main-header navbar-static-top hidden-xs" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only"><fmt:message key="appstore.main.navigation.toggle"/></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

                <li class=<c:if test="${active_page!='user'}">'active'</c:if>>
                    <a href="/appstore">
                        <fmt:message key="appstore.main.navigation.all.apps"/>
                    </a>
                </li>
                <li class=<c:if test="${active_page=='user'}">'active'</c:if>>
                    <c:if test="${(authenticated)&&(!userRole)}">
                        <a href="user">
                            <fmt:message key="appstore.myapp"/>
                            <c:if test="${appUpdateCount > 0}">
                            <span class="num-of-updates branding-background" data-toggle="tooltip" data-placement="left"
                                  title="You have <c:if test="${authenticated}"><c:out value="${appUpdateCount}"/></c:if><c:if test="${!authenticated}">0</c:if> app updates">
                                <c:if test="${authenticated}">
                                    <c:out value="${appUpdateCount}"/>
                                </c:if>
                                <c:if test="${!authenticated}">0</c:if>
                            </span>
                            </c:if>
                        </a>
                    </c:if>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${authenticated}">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary-vdf"><c:out value="${userId}"/></button>
                            <button type="button" class="btn btn-primary-vdf dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <c:if test="${userRole}">
                                        <a href='<fmt:message key="appstore.admin.home.url"/>'>
                                            <i class="icon-cog"></i>
                                            <fmt:message key="appstore.admin"/>
                                        </a>
                                    </c:if>
                                    <c:if test="${showRegLink}">
                                        <a href='<fmt:message key="registration.home"/>'>
                                            <i class="icon-home"></i>
                                            <fmt:message key="appstore.registration"/>
                                        </a>
                                    </c:if>
                                </li>
                                <li>
                                    <a tabindex="-1" href='profile'>
                                        <i class="icon-user"></i> <fmt:message key="appstore.user.profile.label"/>
                                    </a>
                                </li>
                                <li>
                                    <a tabindex="-1" href='logout'>
                                        <i class="icon-off"></i> <fmt:message key="appstore.logout"/>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </c:if>
                <c:if test="${!authenticated}">
                    <li style="padding-top: 8px;">
                        <a href="login_direct"
                           id='signInButton'>
                            <button class="btn btn-primary-vdf"><fmt:message
                                    key='appstore.main.navigation.sign.in'/></button>
                        </a>
                    </li>
                </c:if>
            </ul>
            <form class="navbar-form navbar-right" method="get" action="search" onsubmit="return validateSearch()">
                <div class="form-group">
                    <input type="text"
                           placeholder="<fmt:message key="appstore.main.navigation.Search.button.place.holder"/>"
                           class="form-control vdf-form-control" name="searchKey" value='<c:out value="${searchKey}"/>'
                           required>
                </div>
                <button type="submit" class="btn btn-default hidden"><i class="icon-search"></i></button>
            </form>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>
<div class="container hidden-xs">
    <div class="row row-shortned">
        <div class="navbar-collapse collapse">
            <ul class="nav nav-justified branding-background">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><fmt:message
                            key="appstore.main.navigation.app.categories"/> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <c:forEach items="${appCategories}" var="category">
                            <c:if test="${!fn:contains(category,'Dialog' )}">
                                <li><a href='apps?category=<c:out value="${category}"/>'><c:out
                                        value="${category}"/> </a></li>
                            </c:if>
                        </c:forEach>
                    </ul>
                </li>
                <li><a href="popular-apps"><fmt:message key="appstore.main.navigation.popular.apps"/></a></li>
                <li><a href="featured-apps"><fmt:message key="appstore.main.navigation.featured.apps"/></a></li>
                <li><a href="subscription-apps"><fmt:message key="appstore.main.navigation.subscription.apps"/></a></li>
                <li><a href="downloadable-apps"><fmt:message key="appstore.main.navigation.downloadable.apps"/></a></li>
                <li><a href="mostly-used"><fmt:message key="appstore.main.navigation.mostly.used.apps"/></a></li>
                <li><a href="newly-added"><fmt:message key="appstore.main.navigation.newly.added.apps"/></a></li>
                <li><a href="free-apps"><fmt:message key="appstore.main.navigation.free.apps"/></a></li>
                <%--<li><a href="top-rated"><fmt:message key="appstore.main.navigation.top.rated.apps"/></a></li>--%>

            </ul>
        </div>
    </div>
</div>
<%--Navigation end--%>

<%--slide navigation--%>
<div class="hidden-lg hidden-md hidden-sm visible-xs">

    <nav class="navbar navbar-default navbar-default-min" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="sb-toggle-left navbar-toggle" data-toggle="collapse"
                        data-target="#xs-navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div style="float: left;text-align: right;width: 45%;" id="nav_wrapper_resp">
                <a class="navbar-brand" style="margin-top: 12px" href="/appstore"><fmt:message
                        key="appstore.main.navigation.all.apps"/></a></div>
                <c:if test="${!authenticated}">
                    <a style="top:0.3em;right: 1em; position: relative" class="pull-right"
                       href="login_direct">
                        <button class="btn btn-primary-vdf"><fmt:message
                                key='appstore.main.navigation.sign.in'/></button>
                    </a>
                </c:if>
                <c:if test="${authenticated}">
                    <a style="top:0.3em;right: 1em; position: relative" class="pull-right"
                       href="<fmt:message key='appstore.cas.logout'/>">
                        <button class="btn btn-primary-vdf">
                            <fmt:message key='appstore.logout'/></button>
                    </a>
                </c:if>
            </div>
            <div class="hidden collapse navbar-collapse" id="">
                <ul class="nav navbar-nav">
                    <li class=<c:if test="${active_page=='user'}">'active'</c:if>>
                        <c:if test="${(authenticated)&&(!userRole)}">
                            <a href="user">
                                <fmt:message key="appstore.myapp"/>
                                <span class="num-of-updates branding-background" data-toggle="tooltip" data-placement="left"
                                      title="You have <c:if test="${authenticated}"><c:out value="${appUpdateCount}"/></c:if><c:if test="${!authenticated}">0</c:if> app updates">
                                <c:if test="${authenticated}">
                                    <c:out value="${appUpdateCount}"/>
                                </c:if>
                                <c:if test="${!authenticated}">0</c:if>
                            </span>
                            </a>
                        </c:if>
                    </li>
                    <%--auth links--%>
                    <c:if test="${userRole}">
                        <li>
                            <a href='<fmt:message key="appstore.admin.home.url"/>'>
                                <fmt:message key="appstore.admin"/>
                            </a>
                        </li>
                    </c:if>
                    <c:if test="${showRegLink}">
                        <li>
                            <a href='<fmt:message key="registration.home"/>'>
                                <fmt:message key="appstore.registration"/>
                            </a>
                        </li>
                    </c:if>
                    <%--end auth links--%>

                    <li><a href="freeApps"><fmt:message key="appstore.main.navigation.free.apps"/></a></li>
                    <li><a href="top-rated"><fmt:message key="appstore.main.navigation.top.rated.apps"/></a></li>
                    <li><a href="newly-added"><fmt:message key="appstore.main.navigation.newly.added.apps"/></a></li>
                    <li><a href="mostly-used"><fmt:message key="appstore.main.navigation.mostly.used.apps"/></a></li>


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <fmt:message key="appstore.main.navigation.app.categories"/>
                            <b class="caret"></b>
                            <ul class="dropdown-menu">
                                <c:forEach items="${appCategories}" var="category">
                                    <c:if test="${!fn:contains(category,'Dialog' )}">
                                        <li><a href='apps?category=<c:out value="${category}"/>'><c:out
                                                value="${category}"/> </a></li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="sb-slidebar sb-left">
                <nav>
                    <ul class="sb-menu">
                        <c:if test="${authenticated}">
                            <li>
                                <button type="button" style="width: 100%; border-radius: 0px;" class="btn btn-default"><c:out value="${userId}"/></button>
                            </li>
                        </c:if>
                        <li class=<c:if test="${active_page=='user'}">'active'</c:if>>
                            <c:if test="${(authenticated)&&(!userRole)}">
                                <a href="user">
                                    <fmt:message key="appstore.myapp"/>
                                    <span class="num-of-updates branding-background" data-toggle="tooltip" data-placement="left"
                                          title="You have <c:if test="${authenticated}"><c:out value="${appUpdateCount}"/></c:if><c:if test="${!authenticated}">0</c:if> app updates">
                                <c:if test="${authenticated}">
                                    <c:out value="${appUpdateCount}"/>
                                </c:if>
                                <c:if test="${!authenticated}">0</c:if>
                            </span>
                                </a>
                            </c:if>
                        </li>
                        <c:if test="${userRole}">
                            <li>
                                <a href='<fmt:message key="appstore.admin.home.url"/>'>
                                    <fmt:message key="appstore.admin"/>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${showRegLink}">
                            <li>
                                <a href='<fmt:message key="registration.home"/>'>
                                    <fmt:message key="appstore.registration"/>
                                </a>
                            </li>
                        </c:if>
                        <li><a href="freeApps"><fmt:message key="appstore.main.navigation.free.apps"/></a></li>
                        <li><a href="top-rated"><fmt:message key="appstore.main.navigation.top.rated.apps"/></a></li>
                        <li><a href="newly-added"><fmt:message key="appstore.main.navigation.newly.added.apps"/></a>
                        </li>
                        <li><a href="mostly-used"><fmt:message key="appstore.main.navigation.mostly.used.apps"/></a>
                        </li>
                        <li>
                            <a href="#" class="sb-toggle-submenu"><fmt:message
                                    key="appstore.main.navigation.app.categories"/><span class="sb-caret"></span></a>
                            <ul class="sb-submenu" style="display: none;">
                                <c:forEach items="${appCategories}" var="category">
                                    <c:if test="${!fn:contains(category,'Dialog' )}">
                                        <li><a href='apps?category=<c:out value="${category}"/>'><c:out
                                                value="${category}"/> </a></li>
                                    </c:if>
                                </c:forEach>
                            </ul>
                        </li>

                    </ul>
                </nav>
            </div>
        </div>
    </nav>

    <%--search for small devices--%>
    <div class="container row">
        <div class="col-xs-12">
            <form id="custom-search-form" class="form-search form-horizontal" action="search" method="get">
                <div class="input-group">
                    <input type="text"
                           placeholder="<fmt:message key="appstore.main.navigation.Search.button.place.holder"/>"
                           class="form-control vdf-form-control" name="searchKey" value='<c:out value="${searchKey}"/>'
                           required>
                    <span class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="icon-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <br/>
</div>
<%--end slide navigation--%>