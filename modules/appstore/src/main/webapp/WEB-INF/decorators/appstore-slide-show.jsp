<%@ taglib prefix="fmtj" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="row">
    <c:if test="${fn:length(bannerUrls) gt 0}">
        <%--app banner--%>
        <div id="myCarousel" class="carousel">
            <ol class="carousel-indicators">
                <c:set value="0" var="count"/>
                <c:forEach items="${bannerUrls}" var="b">
                    <li data-target="#myCarousel" data-slide-to='<c:out value="${count}"/>' class='<c:if test="${count==0}">active</c:if>'></li>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </ol>
            <div class="carousel-inner">
                <c:set value="1" var="count"/>
                <c:forEach items="${bannerUrls}" var="url">
                    <div class='item <c:if test="${count eq 1}">active</c:if> '>
                        <a href="${bannerLinks[count-1]}" target="_blank">
                            <img src='https://vdfapps.hsenidmobile.com/appstore/<c:out value="${url}"/>' width="100%"/>
                        </a>
                    </div>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </div>
        </div>
    </c:if>
</div>