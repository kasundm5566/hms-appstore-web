<div class="navbar-default sidebar left-navigation" role="navigation">
    <ul class="nav" id="side-menu">
        <li id="home-link">
            <a href="/appstore"><i class="glyphicon glyphicon-home"></i> Home </a>
        </li>

        <c:if test="${(authenticated)&&(!userRole)}">
            <li id="myApps-link" class=<c:if test="${active_page=='user'}">'active'</c:if>>
                <a href="myApps">
                    <i class="glyphicon glyphicon-phone"></i>
                    <fmt:message key="appstore.myapp"/>
                    <c:if test="${appUpdateCount > 0}">
                                <span class="num-of-updates branding-background" data-toggle="tooltip"
                                      data-placement="left"
                                      title="You have <c:if test="${authenticated}"><c:out value="${appUpdateCount}"/></c:if><c:if test="${!authenticated}">0</c:if> app updates">
                                    <c:if test="${authenticated}">
                                        <c:out value="${appUpdateCount}"/>
                                    </c:if>
                                    <c:if test="${!authenticated}">0</c:if>
                                </span>
                    </c:if>
                </a>
            </li>
        </c:if>

        <hr class="margin-top-bottom-0"/>

        <c:if test="${authenticated}">
            <li id="profile-link">
                <a tabindex="-1" href='profile'>
                    <i class="icon-user"></i> <fmt:message key="appstore.user.account.label"/>
                </a>
            </li>
            <li>
                <c:if test="${showRegLink}">
                    <a href='<fmt:message key="registration.home"/>'>
                        <i class="glyphicon glyphicon-edit"></i>
                        <fmt:message key="appstore.registration"/>
                    </a>
                </c:if>
                <c:if test="${userRole}">
                    <a href='<fmt:message key="appstore.admin.home.url"/>'>
                        <i class="glyphicon glyphicon-list"></i>
                        <fmt:message key="appstore.admin"/>
                    </a>
                </c:if>
            </li>
        </c:if>

    </ul>
</div>
<br/>
<div>
    <c:if test="${fn:length(leftBanners) gt 0}">
        <%--left panel banners--%>
        <div id="carousel-example-generic" class="carousel">
            <div class="carousel-inner">
                <c:set value="1" var="count"/>
                <c:forEach items="${leftBanners}" var="banner">
                    <div class='item <c:if test="${count eq 1}">active</c:if> '>
                        <a href="${leftBannerLinks[count-1]}" target="_blank">
                            <img src="${baseUrl}${banner}" width="100%"/>
                        </a>
                    </div>
                    <c:set value="${count+1}" var="count"/>
                </c:forEach>
            </div>
        </div>
    </c:if>
</div>