<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:bundle basename="messages">
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <sitemesh:write property='head'></sitemesh:write>

        <link rel="shortcut icon" href="./resources/images/fevicon.png">

        <title>
            <sitemesh:write property='title'/>
        </title>
        <!-- Bootstrap core CSS -->
        <link href="./resources/css/bootstrap.min.css" rel="stylesheet">

            <%--font awesome css--%>
        <link rel="stylesheet" href="./resources/css/font-awesome.min.css">

        <!-- Custom styles for this template -->
        <link href="./resources/css/custom.css" rel="stylesheet">
        <link href="./resources/css/vodafone.css" rel="stylesheet">
        <link href="./resources/css/slidebars.min.css" rel="stylesheet"/>
        <link href="./resources/css/offcanvas.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="./resources/js/html5shiv.js"></script>
        <script src="./resources/js/respond.min.js"></script>
        <![endif]-->
    <%--colorbox css--%>
        <%--essential javascript--%>
        <script src="./resources/js/jquery.js"></script>
        <script src="./resources/js/bootstrap-paginator.js"></script>

        <link href="./resources/css/colorbox.css" type="text/css" rel="stylesheet"/>
        <!-- Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-52008234-1', {
                'cookieDomain': 'none'
            });
            ga('send', 'pageview');

        </script>
        <!-- End Google Analytics -->

    </head>

    <body>

    <div class="row">
            <%--Navigation start--%>
        <%@ include file="top-navigation-new.jsp" %>
            <%--Navigation end--%>
    </div>
    <div class="row vdf-background" style="min-height: 100%;">
        <div class="container">

            <%--<div id="fb-root"></div>--%>

                <%--Page content begins--%>
            <div id="containerElement">
                <div class="row">
                    <div class="col-md-2" id="leftNaviPanel">
                        <%@ include file="left-navigation.jsp" %>
                    </div>
                    <div class="col-md-10 col-sm-12 col-xs-12 padding-left-right-0" id="mainBodyCol">
                        <%@ include file="body-navigation.jsp"%>
                        <sitemesh:write property='body'/>
                    </div>

                        <%--left app panel begin--%>
                    <%--<appstoreLeftPanel id="appStoreLeftPanel">
                        <%@ include file="left-app-panel.jsp" %>
                    </appstoreLeftPanel>--%>
                        <%--left app panel end--%>

                </div>
            </div>

                <%--Page content ends--%>
            <%@ include file="modals.jsp" %>

        </div>
    </div>

        <%--footer begin--%>
    <div class="row">
        <%@ include file="footer.jsp" %>
    </div>
        <%--footer end--%>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->

    <script src="./resources/js/bootstrap.min.js"></script>
    <script src="./resources/js/offcanvas.js"></script>
    <script src="./resources/js/bootstrap-tooltip.js"></script>
    <script src="./resources/js/bootstrap-popover.js"></script>
    <script src="./resources/js/jquery.colorbox-min.js"></script>
    <script src="./resources/js/jquery.validate.js"></script>
    <script src="./resources/js/slidebars.min.js"></script>
    <script src="./resources/js/custom.js"></script>

    </div>
    </body>
    </html>
</fmt:bundle>