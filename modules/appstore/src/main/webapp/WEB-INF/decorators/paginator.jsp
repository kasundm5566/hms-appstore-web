<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">

    var pageSize = parseInt('<c:out value="${appsPerPage}"/>');
    var categoryKey = '<c:out value="${param.category}"/>';
    var searchStatus = '<c:out value="${searchFilterStatus}"/>';

    console.info("------------pagination----------------")
    var options = {
        currentPage:<c:out value="${activePage+1}"/>,
        totalPages:<c:out value="${fn:length(pagination)}"/>,
        useBootstrapTooltip: true,
        numberOfPages:5,

        pageUrl: function (type, page, current) {
            return ("?category=" + categoryKey + "&skip=" + ((page - 1) * pageSize) + "&limit=" + pageSize);
        }
    };
//    console.info(pageSize+"|"+categoryKey+"|"+options.currentPage+"|"+options.numberOfPages);
    $(function () {
        $("#pagination").bootstrapPaginator(options);
    });

</script>