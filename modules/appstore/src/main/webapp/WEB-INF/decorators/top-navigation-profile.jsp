<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--Navigation start--%>
<div class="navbar navbar-default vdf-main-header navbar-static-top hidden-xs" role="navigation">
    <div class="container">

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${authenticated}">
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary-vdf"><c:out value="${userId}"/></button>
                            <button type="button" class="btn btn-primary-vdf dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <c:if test="${userRole}">
                                        <a href='<fmt:message key="appstore.admin.home.url"/>'>
                                            <i class="icon-cog"></i>
                                            <fmt:message key="appstore.admin"/>
                                        </a>
                                    </c:if>
                                    <c:if test="${showRegLink}">
                                        <a href='<fmt:message key="registration.home"/>'>
                                            <i class="icon-home"></i>
                                            <fmt:message key="appstore.registration"/>
                                        </a>
                                    </c:if>
                                </li>
                                <li>
                                    <a tabindex="-1" href='profile'>
                                        <i class="icon-user"></i> <fmt:message key="appstore.user.profile.label"/>
                                    </a>
                                </li>
                                <li>
                                    <a tabindex="-1" href='logout'>
                                        <i class="icon-off"></i> Logout
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </c:if>
                <c:if test="${!authenticated}">
                    <li style="padding-top: 8px;">
                        <a href="login_direct"
                           id='signInButton'>
                            <button class="btn btn-primary-vdf"><fmt:message
                                    key='appstore.main.navigation.sign.in'/></button>
                        </a>
                    </li>
                </c:if>
            </ul>
            <form class="navbar-form" method="get" action="search" onsubmit="return validateSearch()">
                <div class="form-group">
                    <input type="text"
                           placeholder="<fmt:message key="appstore.main.navigation.Search.button.place.holder"/>"
                           class="form-control vdf-form-control" name="searchKey" class="top-nav-search" value='<c:out value="${searchKey}"/>'
                           required>
                </div>
                <button type="submit" class="btn btn-default btn-search"><i class="icon-search"></i></button>
            </form>
        </div>
        <!--/.nav-collapse -->
    </div>
</div>

<%--Navigation end--%>

