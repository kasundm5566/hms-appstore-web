<div class="body-navigation">
    <ul class="nav nav-justified">

        <li class="dropdown dropdown-large navi-links" id="category-link">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <div class="">
                    <fmt:message key="appstore.main.navigation.app.categories"/> <span class="caret"></span>
                </div>
            </a>

            <c:set value="${fn:length(appCategories)}" var="categoriesLength"/>

            <c:choose>
                <%-- Check whther there are admin created categories. --%>
                <c:when test="${categoriesLength gt 0}">
                    <c:choose>
                        <%--
                        It will show maximum of 4 columns(fixed) of categories in category drop-down.
                        So, the maximum number of category names can be shown in 5x4 grid is 20.
                        When the there are more than 20 categories, it will increment the rows rather than adding new columns.
                          --%>
                        <c:when test="${fn:length(appCategories) gt 20}">
                            <fmt:formatNumber
                                    value="${(categoriesLength/4) + ((categoriesLength/4) % 1 == 0 ? 0 : 0.5)}"
                                    var="numOfRows" pattern="#"/>
                        </c:when>
                        <c:otherwise>
                            <c:set var="numOfRows" value="${5}"/>
                        </c:otherwise>
                    </c:choose>
                    <%-- Number of columns required to diplay 5 categories per row. --%>
                    <fmt:formatNumber value="${(categoriesLength/5) + ((categoriesLength/5) % 1 == 0 ? 0 : 0.5)}"
                                      var="numOfCols" pattern="#"/>

                    <ul class="dropdown-menu dropdown-menu-large row categories-drop-down">
                        <c:forEach begin="1" end="${numOfCols}" varStatus="loop">
                            <%--
                            If the required column count is lesser than 4, it is required to modify the drop down width.
                            Also, the class of list item(col-sm-<value>) since it also need to change dynamically according to the columns count.
                             --%>
                            <c:choose>
                                <c:when test="${numOfCols lt 4}">
                                    <fmt:formatNumber value="${(12/numOfCols)}" var="cols" pattern="#"/>
                                    <c:set value="${'col-sm-'}${cols}" var="listItemClass"/>
                                    <li class=${listItemClass}>
                                    <style>
                                        .categories-drop-down {
                                            width: ${150*numOfCols}px !important;
                                        }
                                    </style>
                                </c:when>
                                <c:otherwise>
                                    <li class="col-sm-3">
                                    <style>
                                        .categories-drop-down {
                                            width: ${150*4}px !important;
                                        }
                                    </style>
                                </c:otherwise>
                            </c:choose>
                            <ul>
                                <%-- Fill the category drop-down list. --%>
                                <c:forEach begin="0" end="${numOfRows-1}" varStatus="loop2">
                                    <li>
                                        <c:url value="apps" var="url">
                                            <c:param name="category" value="${appCategories[numOfRows*(loop.index-1) + loop2.index]}"/>
                                        </c:url>
                                        <a style="word-wrap: break-word;"
                                           href='${url}'><c:out
                                                value="${appCategories[numOfRows*(loop.index-1) + loop2.index]}"/> </a>
                                    </li>
                                </c:forEach>
                            </ul>
                            </li>
                        </c:forEach>
                    </ul>
                </c:when>
                <c:otherwise>
                    <ul class="dropdown-menu dropdown-menu-large row categories-drop-down"
                        style="width: 150px !important;">
                        <li style="margin-left: 10px;"><fmt:message
                                key="appstore.main.navigation.app.categories.empty"/></li>
                    </ul>
                </c:otherwise>
            </c:choose>
        </li>
        <li class="navi-links" id="popular-apps">
            <a href="popular-apps"><fmt:message key="appstore.main.navigation.popular.apps"/></a>
        </li>
        <li class="navi-links" id="featured-apps">
            <a href="featured-apps"><fmt:message key="appstore.main.navigation.featured.apps"/></a>
        </li>
        <li class="navi-links" id="subscription-apps">
            <a href="subscription-apps"><fmt:message key="appstore.main.navigation.subscription.apps"/></a>
        </li>
        <li class="navi-links" id="downloadable-apps">
            <a href="downloadable-apps"><fmt:message key="appstore.main.navigation.downloadable.apps"/></a>
        </li>
        <li class="navi-links" id="mostly-used">
            <a href="mostly-used"><fmt:message key="appstore.mostly.used.panel.title"/></a>
        </li>
        <li class="navi-links" id="newly-added">
            <a href="newly-added"><fmt:message key="appstore.main.navigation.newly.added.apps"/></a>
        </li>
        <li class="navi-links" id="free-apps">
            <a href="free-apps"><fmt:message key="appstore.main.navigation.free.apps"/></a>
        </li>
        <%--<li><a href="top-rated"><fmt:message key="appstore.main.navigation.top.rated.apps"/></a></li>--%>

    </ul>
</div>

<script>
    $(document).ready(function () {
        var currentUrl = window.location.href;
        var last = currentUrl.lastIndexOf("/");
        var currentPath = currentUrl.substring(last);

        if (currentPath.indexOf("category") !== -1) {
            var activeElement = document.getElementById("category-link");
            activeElement.classList.add("active");
        } else if (currentPath === "/") {
            var activeElement = document.getElementById("home-link");
            activeElement.classList.add("active");
        } else if (currentPath === "/myApps") {
            var activeElement = document.getElementById("myApps-link");
            activeElement.classList.add("active");
        } else if (currentPath === "/profile") {
            var activeElement = document.getElementById("profile-link");
            activeElement.classList.add("active");
        } else if ((currentPath.indexOf("view?appId") !== -1) || (currentPath.indexOf("search") !== -1)) {
            //do nothing
        } else {
            var activeElement = document.getElementById(currentPath.substring(1));
            activeElement.classList.add("active");
        }
    });
</script>
