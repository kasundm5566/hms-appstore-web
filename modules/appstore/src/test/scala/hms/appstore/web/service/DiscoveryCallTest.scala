package hms.appstore.web.service

import com.escalatesoft.subcut.inject.Injectable
import hms.appstore.api.client.DiscoveryInternalService
import org.specs2.mutable.Specification
import scala.concurrent.Await

class DiscoveryCallTest extends Specification with Injectable with WebConfig{

  val bindingModule = ServiceModule

  private val discoveryInternalService=inject[DiscoveryInternalService]
  "app find should give appdetails" in {
    val resultFuture=discoveryInternalService.findApp("APP_000026")
    val result=Await.result(resultFuture,discoveryApiTimeOut).result
    println(result)

  }
}
