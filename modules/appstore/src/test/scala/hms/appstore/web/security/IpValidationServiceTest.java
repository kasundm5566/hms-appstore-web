package hms.appstore.web.security;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IpValidationServiceTest {

    private final IpValidationService validationService;
    private static final List<String> ipList = new ArrayList<>();

    public IpValidationServiceTest() {
        validationService = IpValidationService.getInstance();
        prepareRandIpList(1_000);
    }

    @Test
    public void testGetInstance() throws Exception {
        final Object serviceInstance = IpValidationService.getInstance();
        assertTrue(serviceInstance instanceof IpValidationService);
    }

    public static void prepareRandIpList(int size) {
        int counter = 0;
        Random random = new Random();
        while (counter < size) {
            ipList.add(genIp(random));
            counter += 1;
        }
    }

    private static String genIp(Random random) {
        return (random.nextInt(254) + "."
                + random.nextInt(254) + "."
                + random.nextInt(254) + "."
                + random.nextInt(254));
    }

    @Test
    public void testCommaSeperated() {
        final String ip = "27.123.162.12, 127.0.0.1";
        assertEquals(false, validationService.isValidHost(ip));
    }

    @Test
    public void testIsValidHost(){

        System.out.println("validating ips " + ipList.size());
        long start = System.currentTimeMillis();
        for (String str : ipList) {
            System.out.println(
                    String.format(
                            "validating %10s %10b",
                            str,
                            (validationService.isValidHost(str)))
            );
        }
        long end = System.currentTimeMillis();
        System.out.println("Running Time " + (end - start) + " ms");

    }
}
