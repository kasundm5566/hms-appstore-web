package hms.appstore.web.security

import org.specs2.mutable.Specification


class IpValidationServiceSpecs extends Specification {

  "ipvalidationservice should validate " in {
    val ipListFile = this.getClass.getClassLoader.getResource("ip-list.txt")
    val ipList = scala.io.Source.fromFile(ipListFile.toURI).getLines().toList
    val validationService = IpValidationService.getInstance()
    ipList.foreach(ip => {
      printf("validating %16s => %b\n", ip, validationService.isValidHost(ip))
    })
  }

}
